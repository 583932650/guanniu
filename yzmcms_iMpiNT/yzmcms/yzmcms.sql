/*
 Navicat Premium Data Transfer

 Source Server         : hua
 Source Server Type    : MySQL
 Source Server Version : 50726 (5.7.26)
 Source Host           : localhost:3306
 Source Schema         : yzmcms

 Target Server Type    : MySQL
 Target Server Version : 50726 (5.7.26)
 File Encoding         : 65001

 Date: 13/03/2023 20:09:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for yzm_admin
-- ----------------------------
DROP TABLE IF EXISTS `yzm_admin`;
CREATE TABLE `yzm_admin`  (
  `adminid` mediumint(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `adminname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `roleid` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `rolename` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `realname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `nickname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `email` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `logintime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `loginip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `addtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `errnum` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `addpeople` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`adminid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_admin
-- ----------------------------
INSERT INTO `yzm_admin` VALUES (1, 'yzmcms', '2bde58da94b24f41fd9194fb2138d2ab', 1, '超级管理员', '', '', '', 1678669752, '127.0.0.1', 1677496482, 0, '创始人');

-- ----------------------------
-- Table structure for yzm_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `yzm_admin_log`;
CREATE TABLE `yzm_admin_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `module` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `controller` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `querystring` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `adminid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `adminname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `logtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `logtime`(`logtime`) USING BTREE,
  INDEX `adminid`(`adminid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_admin_log
-- ----------------------------
INSERT INTO `yzm_admin_log` VALUES (1, 'admin', '', '清除错误日志', 1, 'yzmcms', '127.0.0.1', 1678088503);
INSERT INTO `yzm_admin_log` VALUES (2, 'admin', '', '清除错误日志', 1, 'yzmcms', '127.0.0.1', 1678497071);

-- ----------------------------
-- Table structure for yzm_admin_login_log
-- ----------------------------
DROP TABLE IF EXISTS `yzm_admin_login_log`;
CREATE TABLE `yzm_admin_login_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `adminname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `logintime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `loginip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `address` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `password` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `loginresult` tinyint(1) NOT NULL DEFAULT 0 COMMENT '登录结果1为登录成功0为登录失败',
  `cause` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `admin_index`(`adminname`, `loginresult`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_admin_login_log
-- ----------------------------
INSERT INTO `yzm_admin_login_log` VALUES (1, 'yzmcms', 1677496527, '127.0.0.1', '', '', 1, '登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES (2, 'yzmcms', 1677653339, '127.0.0.1', '', 'yzmxms', 0, '密码错误！');
INSERT INTO `yzm_admin_login_log` VALUES (3, 'yzmcms', 1677653357, '127.0.0.1', '', '', 1, '登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES (4, 'yzmcms', 1677828600, '127.0.0.1', '', '', 1, '登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES (5, 'yzmcms', 1678065170, '127.0.0.1', '', '', 1, '登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES (6, 'yzmcms', 1678089053, '127.0.0.1', '', '', 1, '登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES (7, 'yzmcms', 1678158416, '127.0.0.1', '', '', 1, '登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES (8, 'yzmcms', 1678241091, '127.0.0.1', '', '', 1, '登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES (9, 'yzmcms', 1678325627, '127.0.0.1', '', '', 1, '登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES (10, 'yzmcms', 1678412387, '127.0.0.1', '', '', 1, '登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES (11, 'yzmcms', 1678412421, '127.0.0.1', '', '', 1, '登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES (12, 'yzmcms', 1678497037, '127.0.0.1', '', '', 1, '登录成功！');
INSERT INTO `yzm_admin_login_log` VALUES (13, 'yzmcms', 1678669752, '127.0.0.1', '', '', 1, '登录成功！');

-- ----------------------------
-- Table structure for yzm_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `yzm_admin_role`;
CREATE TABLE `yzm_admin_role`  (
  `roleid` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `rolename` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `system` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `disabled` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`roleid`) USING BTREE,
  INDEX `disabled`(`disabled`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_admin_role
-- ----------------------------
INSERT INTO `yzm_admin_role` VALUES (1, '超级管理员', '超级管理员', 1, 0);
INSERT INTO `yzm_admin_role` VALUES (2, '总编', '总编', 1, 0);
INSERT INTO `yzm_admin_role` VALUES (3, '发布人员', '发布人员', 1, 0);

-- ----------------------------
-- Table structure for yzm_admin_role_priv
-- ----------------------------
DROP TABLE IF EXISTS `yzm_admin_role_priv`;
CREATE TABLE `yzm_admin_role_priv`  (
  `roleid` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `m` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `c` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `a` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `data` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  INDEX `roleid`(`roleid`, `m`, `c`, `a`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of yzm_admin_role_priv
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_adver
-- ----------------------------
DROP TABLE IF EXISTS `yzm_adver`;
CREATE TABLE `yzm_adver`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '1文字2代码3图片',
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `text` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `img` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `code` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `describe` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `start_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `end_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type`(`type`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_adver
-- ----------------------------
INSERT INTO `yzm_adver` VALUES (1, 1, '极简中式', 'http://test123.com/', '极简中式', '', '<a href=\"http://test123.com/\" target=\"_blank\" title=\"极简中式\">极简中式</a>', '极简中式', 1678426489, 1678377600, 1678982400);
INSERT INTO `yzm_adver` VALUES (2, 0, '', '', '', '', '<a href=\"\" target=\"_blank\" title=\"\"><img src=\"\"></a>', '', 1678430960, 0, 0);
INSERT INTO `yzm_adver` VALUES (3, 0, '', '', '', '', '<a href=\"\" target=\"_blank\" title=\"\"><img src=\"\"></a>', '', 1678431016, 0, 0);
INSERT INTO `yzm_adver` VALUES (4, 0, '', '', '', '', '<a href=\"\" target=\"_blank\" title=\"\"><img src=\"\"></a>', '', 1678431101, 0, 0);
INSERT INTO `yzm_adver` VALUES (5, 0, '', '', '', '', '<a href=\"\" target=\"_blank\" title=\"\"><img src=\"\"></a>', '', 1678431324, 0, 0);
INSERT INTO `yzm_adver` VALUES (6, 0, '', '', '', '', '<a href=\"\" target=\"_blank\" title=\"\"><img src=\"\"></a>', '极简中式', 1678435260, 0, 0);

-- ----------------------------
-- Table structure for yzm_all_content
-- ----------------------------
DROP TABLE IF EXISTS `yzm_all_content`;
CREATE TABLE `yzm_all_content`  (
  `allid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `modelid` tinyint(2) UNSIGNED NOT NULL DEFAULT 0,
  `catid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `username` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `thumb` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `issystem` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`allid`) USING BTREE,
  INDEX `userid_index`(`userid`, `issystem`, `status`) USING BTREE,
  INDEX `modelid_index`(`modelid`, `id`) USING BTREE,
  INDEX `status`(`siteid`, `status`) USING BTREE,
  INDEX `issystem`(`siteid`, `issystem`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_all_content
-- ----------------------------
INSERT INTO `yzm_all_content` VALUES (8, 0, 1, 50, 16, 1, 'yzmcms', '马到成功', 1678691028, 1678695229, 'http://test123.com/bojueshimumen/16.html', '/uploads/ueditor/image/20230307/1678175859106042.jpg', 1, 1);
INSERT INTO `yzm_all_content` VALUES (9, 0, 1, 50, 17, 1, 'yzmcms', '福禄双全', 1678691301, 1678695218, 'http://test123.com/bojueshimumen/17.html', '/uploads/ueditor/image/20230307/1678175868787799.jpg', 1, 1);
INSERT INTO `yzm_all_content` VALUES (3, 0, 1, 28, 3, 1, 'yzmcms', '关于冠牛', 1678161320, 1678343678, 'http://test123.com/gongsixinwen/3.html', 'http://test123.com/uploads/ueditor/image/20230307/1678161623276510.jpg', 1, 1);
INSERT INTO `yzm_all_content` VALUES (4, 0, 1, 28, 4, 1, 'yzmcms', '伯爵实木门', 1678174856, 1678347665, 'http://test123.com/gongsixinwen/4.html', 'http://www.szguanniu.com/upimg/2019090315074741.jpg', 1, 1);
INSERT INTO `yzm_all_content` VALUES (5, 0, 1, 28, 5, 1, 'yzmcms', '冠牛快讯近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。', 1678341758, 1678343662, 'http://test123.com/gongsixinwen/5.html', '/uploads/ueditor/image/20230309/thumb_500_300_1678342055107323.jpg', 1, 1);
INSERT INTO `yzm_all_content` VALUES (6, 0, 1, 31, 14, 1, 'yzmcms', '商学院介绍', 1678350473, 1678350511, 'http://test123.com/shangxueyuan/14.html', 'http://test123.com/uploads/ueditor/image/20230309/1678350477874263.jpg', 1, 1);
INSERT INTO `yzm_all_content` VALUES (10, 0, 3, 49, 1, 1, 'yzmcms', '测试下载文件1', 1678709019, 1678709133, 'http://test123.com/xiazaizhongxin/1.html', '', 1, 1);
INSERT INTO `yzm_all_content` VALUES (11, 0, 3, 49, 2, 1, 'yzmcms', '测试下载文件2', 1678709300, 1678709331, 'http://test123.com/xiazaizhongxin/2.html', '', 1, 1);

-- ----------------------------
-- Table structure for yzm_article
-- ----------------------------
DROP TABLE IF EXISTS `yzm_article`;
CREATE TABLE `yzm_article`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `nickname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `title` varchar(180) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `color` char(9) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `keywords` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `click` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `copyfrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `thumb` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `flag` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '1置顶,2头条,3特荐,4推荐,5热点,6幻灯,7跳转',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `issystem` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `listorder` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `groupids_view` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '阅读权限',
  `readpoint` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '阅读收费',
  `paytype` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '收费类型',
  `is_push` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否百度推送',
  `pro_img` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '产品轮播图',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `status`(`status`, `listorder`) USING BTREE,
  INDEX `catid`(`catid`, `status`) USING BTREE,
  INDEX `userid`(`userid`, `status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_article
-- ----------------------------
INSERT INTO `yzm_article` VALUES (16, 50, 1, 'yzmcms', '管理员', '马到成功', '', 1678691028, 1678695229, '马到成功', '马到成功-罗马假日', 132, '<p><span style=\"font-family: 微软雅黑; background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(102, 102, 102);\">采用中古对称式的平衡构图，儒雅大方。无论图形大小，它总意在体现成双成对的效果，是中国传统意义上的审美。它诠释出福禄双双而至的幸福之家。“福禄”本是福星与禄星的结合，福星主平安祥和，禄星主功名利禄。福如东海，飞黄腾达，成就完美人生。<br/></span><br style=\"color: rgb(119, 119, 119); font-family: 微软雅黑; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255);\"/><br style=\"color: rgb(119, 119, 119); font-family: 微软雅黑; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255);\"/><span style=\"font-family: 微软雅黑; background-color: rgb(255, 255, 255); font-size: 24px; color: rgb(102, 102, 102);\"></span><span style=\"font-family: 微软雅黑; background-color: rgb(255, 255, 255); font-size: 24px; color: rgb(102, 102, 102);\">和气生财</span><span style=\"color: rgb(119, 119, 119); font-family: 微软雅黑; background-color: rgb(255, 255, 255); font-size: 24px;\"><span style=\"color: rgb(102, 102, 102);\"></span><br/><span style=\"font-size: 14px; color: rgb(102, 102, 102);\"></span><span style=\"font-size: 14px; color: rgb(102, 102, 102);\"></span></span><br style=\"color: rgb(119, 119, 119); font-family: 微软雅黑; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255);\"/><span style=\"font-family: 微软雅黑; background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(102, 102, 102);\"></span><span style=\"font-family: 微软雅黑; background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(102, 102, 102);\">采用纹路清晰的红橡材质，以平面造型搭配以简单的雕花和繁密的线条衬托而制成。色调与纹理相互交融，线面元素谐调搭配，给人一种整体性的视觉美感效果，表达着和气生财的美好愿望和自律。适宜各种风格的住宅门面与室内选配。</span></p>', '原创', '/uploads/ueditor/image/20230307/1678175859106042.jpg', 'http://test123.com/bojueshimumen/16.html', '', 1, 1, 10, 0, 0, 1, 0, '{\"0\":{\"url\":\"\\/uploads\\/ueditor\\/image\\/20230307\\/1678175867162400.jpg\",\"alt\":\"马到成功-罗马假日-1\"},\"1\":{\"url\":\"\\/uploads\\/ueditor\\/image\\/20230307\\/1678175864408375.jpg\",\"alt\":\"马到成功-罗马假日-2\"}}');
INSERT INTO `yzm_article` VALUES (3, 28, 1, 'yzmcms', '管理员', '关于冠牛', '', 1678161320, 1678343678, '关于,冠牛', 'ABOUT CROWN COW（ENGLISH）关于冠牛&lt;span font-size:18px;color:#e56600;&quot;=&quot;&quot; style=&quot;color: rgb(229, 102, 0);font-family: SimSun&quot;&gt;企业简介冠牛，始创于1998年，2...', 115, '<h3 style=\";padding: 0px;font-size: 26px;color: rgb(204, 204, 204);line-height: 1.3em;font-weight: normal;font-family: font\">ABOUT CROWN COW（ENGLISH）</h3><h3 style=\";padding: 0px;font-size: 24px;color: rgb(85, 85, 85);line-height: 1.75em;font-weight: normal\"><span style=\"display: inline-block;padding: 0px 40px;position: relative\">关于冠牛</span></h3><p><img src=\"/uploads/ueditor/image/20230307/1678161623276510.jpg\" alt=\"\" style=\"border: none;vertical-align: top;max-width: 100%\"/></p><h2 style=\";padding: 0px\"></h2><p style=\"margin: 16px 0;padding: 0px 0px 30px;text-indent: 32px\"><span style=\"line-height: 2.5;color: rgb(102, 102, 102)\">冠牛，始创于</span><span style=\"line-height: 2.5;color: rgb(102, 102, 102)\">1998</span><span style=\"line-height: 2.5;color: rgb(102, 102, 102)\">年，</span><span style=\"line-height: 2.5;color: rgb(102, 102, 102)\">20余</span><span style=\"line-height: 2.5;color: rgb(102, 102, 102)\">年来始终以实木作为设计与生产的核心。“实木”作为冠牛的标签，一直贯穿于企业的发展历程。从经典的木门单品延伸到现在的实木全屋定制领域，冠牛始终坚持实木品质信念和实木专研精神。这种匠心的坚守与对实木的专注，也让冠牛在</span><span style=\"line-height: 2.5;color: rgb(102, 102, 102)\">20</span><span style=\"line-height: 2.5;color: rgb(102, 102, 102)\">余年的发展历程中，沉淀出了独树一帜的实木产品品质与影响力。</span></p><ul style=\"padding: 0px\" class=\" list-paddingleft-2\"></ul><p><img src=\"/uploads/ueditor/image/20230307/1678161624152531.jpg\" alt=\"\" style=\"border: none;vertical-align: top;max-width: 100%\"/></p><h2 style=\";padding: 0px\"><span style=\"color: rgb(229, 102, 0);font-family: SimSun\">企业理念：</span><span style=\"color: rgb(229, 102, 0);font-family: SimSun\">止于至善&nbsp;</span><span style=\"color: rgb(229, 102, 0);font-family: SimSun\">治木之道</span></h2><p><span style=\"color: rgb(229, 102, 0)\"><br/></span></p><p style=\"margin-top: 0px;margin-bottom: 0px;padding: 0px 0px 30px\"><span style=\"font-family: SimSun;line-height: 2.5;font-size: 12px;color: rgb(102, 102, 102)\">【理念解读】“止于至善”来源于《礼记·大学》一篇的首句：“大学之道，在明明德，在亲民，在止于至善。”大学之道讲的大人生的经营和管理，经营的目标是至善，也即是一种完美的境界。“止于至善”昭示的是一种永不止息、创新超越的“进取”心态，是一种对完善、完美的境界孜孜不倦追求的崇高精神。冠牛的“止于至善”是对制造木门而言的，因为有了“止于至善”的追求，也就形成了独具个性的治木精神，即“治木之道”。“止于至善，治木之道”是冠牛人坚定不移的信念追求。</span></p><ul style=\"padding: 0px\" class=\" list-paddingleft-2\"></ul><p><img src=\"/uploads/ueditor/image/20230307/1678161626210489.jpg\" alt=\"\" style=\"border: none;vertical-align: top;max-width: 100%\"/></p><h2 style=\";padding: 0px\"><span style=\"color: rgb(229, 102, 0);font-family: SimSun;line-height: 2\">企业精神：创新 开拓 务实 诚信</span></h2><h2 style=\";padding: 0px\"><strong><span style=\"color: rgb(102, 102, 102);font-size: 10px;line-height: 2\">鼓励创新的精神；注重开拓的精神；追求务实的精神；传达诚信的精神。</span></strong><strong><span style=\"background-color: rgb(102, 102, 102)\"></span></strong></h2><p><span style=\"color: rgb(102, 102, 102);font-size: 14px\"><strong><span style=\"line-height: 2;font-size: 10px\">创新：</span></strong><span style=\"line-height: 2;font-size: 10px\">创业立世，需要革故鼎新。创新，是经营者必备的业务素质。成功源于远见卓识的头脑和吐故纳新的胸怀。创新同时也昭示着：“境由心造，事在人为”的自由境界。</span><br/><strong><span style=\"line-height: 2;font-size: 10px\">开拓：</span></strong><span style=\"line-height: 2;font-size: 10px\">开创未来，拓展疆土，是创业者的宇宙意识。安逸的没有风浪的生活不是有志者的追求，只有永不满足，不断地开拓进取，才得以成就永恒的事业。</span><br/><strong><span style=\"line-height: 2;font-size: 10px\">务实：</span></strong><span style=\"line-height: 2;font-size: 10px\">人生伟业的建立，不在能知，乃在能行。踏踏实实的工作态度，是一种着眼于微末，积沙成塔的本领，也是一种卓越的品质。任何成功的人生都超越不了这一步。</span><br/><strong><span style=\"line-height: 2;font-size: 10px\">诚信：</span></strong><span style=\"line-height: 2;font-size: 10px\">开诚立信，以诚待人，个人有诚，方能处世立身，企业有诚，方能展蔓延，生生不息。诚信之于人，乃立身之本；诚信之于业，乃立业之道。</span><br/><br/></span><span style=\"color: rgb(229, 102, 0);font-family: SimSun\"><br/></span></p><ul style=\"padding: 0px\" class=\" list-paddingleft-2\"></ul><p><img src=\"/uploads/ueditor/image/20230307/1678161629734676.jpg\" alt=\"\" style=\"border: none;vertical-align: top;max-width: 100%\"/></p><h2 style=\";padding: 0px\"><span style=\"color: rgb(229, 102, 0);font-family: SimSun\"><br/></span></h2><h2 style=\";padding: 0px\"><span style=\"color: rgb(229, 102, 0);font-family: SimSun\">经营理念</span></h2><p><span style=\"color: rgb(229, 102, 0);font-family: SimSun\"><br/></span></p><p style=\"margin-top: 0px;margin-bottom: 0px;padding: 0px 0px 30px;text-align: left\"><span style=\"color: rgb(51, 51, 51);font-family: SimSun;font-size: 12px;line-height: 2.5\"><span style=\"color: rgb(102, 102, 102)\">传承与发扬工匠精神；为优秀员工搭建成长和致富平台，</span><br/></span><span style=\"line-height: 2.5;color: rgb(102, 102, 102);font-family: SimSun;font-size: 12px\">实现“安居乐业”的梦想；</span><span style=\"line-height: 2.5;color: rgb(102, 102, 102);font-family: SimSun;font-size: 12px\">为消费者提供优质的绿色家</span><span style=\"color: rgb(102, 102, 102);line-height: 2.5;font-family: SimSun;font-size: 12px\">居产品和星级服务。</span></p><p><span style=\"font-family: SimSun;line-height: 2.5\"></span></p><p style=\"margin-top: 0px;margin-bottom: 0px;padding: 0px 0px 30px\"><br/></p><ul style=\"padding: 0px\" class=\" list-paddingleft-2\"></ul><p><img src=\"/uploads/ueditor/image/20230307/1678161633756260.jpg\" alt=\"\" style=\"border: none;vertical-align: top;max-width: 100%\"/></p><h2 style=\";padding: 0px\"><span style=\"color: rgb(229, 102, 0)\"><br/></span></h2><h2 style=\";padding: 0px\"><span style=\"color: rgb(229, 102, 0);font-family: SimSun\">企业愿景</span></h2><p><span style=\"color: rgb(229, 102, 0)\"><br/></span></p><p style=\"margin-top: 0px;margin-bottom: 0px;padding: 0px 0px 30px\"><span style=\"color: rgb(102, 102, 102);font-family: SimSun;font-size: 12px\">成为中国绿色整体家居行业领跑者</span></p><p style=\"margin-top: 0px;margin-bottom: 0px;padding: 0px 0px 30px\"><br/></p><p><br/></p><ul style=\"padding: 0px\" class=\" list-paddingleft-2\"><li><p><img src=\"/uploads/ueditor/image/20230307/1678161634943346.jpg\" alt=\"\" style=\"border: none;vertical-align: top;max-width: 100%\"/></p><p style=\"margin-top: 0px;margin-bottom: 0px;padding: 0px;position: absolute;left: 0px;bottom: 0px;background: rgba(0, 0, 0, 0.6);width: 379.188px;text-align: center;font-size: 16px;color: rgb(255, 255, 255);line-height: 2.8em\">冠牛·东莞生产基地</p></li><li><p>&nbsp;</p></li><li><p><img src=\"/uploads/ueditor/image/20230307/1678161634178781.jpg\" alt=\"\" style=\"border: none;vertical-align: top;max-width: 100%\"/></p><p style=\"margin-top: 0px;margin-bottom: 0px;padding: 0px;position: absolute;left: 0px;bottom: 0px;background: rgba(0, 0, 0, 0.6);width: 379.188px;text-align: center;font-size: 16px;color: rgb(255, 255, 255);line-height: 2.8em\">冠牛·宿迁生产基地</p></li><li><p>&nbsp;</p></li><li><p><img src=\"/uploads/ueditor/image/20230307/1678161642647690.jpg\" alt=\"\" style=\"border: none;vertical-align: top;max-width: 100%\"/></p><p style=\"margin-top: 0px;margin-bottom: 0px;padding: 0px;position: absolute;left: 0px;bottom: 0px;background: rgba(0, 0, 0, 0.6);width: 379.188px;text-align: center;font-size: 16px;color: rgb(255, 255, 255);line-height: 2.8em\">冠牛·宿迁生产基地</p></li></ul><pre class=\"brush:html;toolbar:false\"><br/></pre><p><br/></p>', '原创', 'http://test123.com/uploads/ueditor/image/20230307/1678161623276510.jpg', 'http://test123.com/gongsixinwen/3.html', '', 1, 1, 10, 0, 0, 1, 0, '');
INSERT INTO `yzm_article` VALUES (6, 28, 1, 'yzmcms', '管理员', '冠牛快讯近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。', '', 1678341758, 1678343662, '冠牛,技术,召开,顺利,沈阳,辽宁', '冠牛快讯：近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。       冠牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从...', 60, '<p class=\"15\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">冠牛快讯：近日</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">2021</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。<br/><img src=\"/uploads/ueditor/image/20230309/1678342055107323.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></span></p><p><span style=\"font-family: SimHei; color: rgb(51, 51, 51); line-height: 2;\"></span></p><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div><span style=\"font-size: 14px;\"><img src=\"/uploads/ueditor/image/20230309/1678342059319269.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/>&nbsp; &nbsp;<br/>&nbsp; &nbsp; 冠</span><span style=\"font-size: 14px;\"></span><span style=\"font-size: 14px;\">牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从各自为导向的优势，来对定制木作高端发展，提供一些可借鉴的道路。</span></div><span style=\"font-size: 14px;\"><br/></span></div></div><p><span style=\"font-family: 微软雅黑; font-size: 12px;\"></span></p><p><br/></p><div style=\"font-family: 微软雅黑; font-size: 12px; white-space: normal; text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102737_85504.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342059174802.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102940_72784.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342060449292.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><span style=\"color: rgb(51, 51, 51); line-height: 2; font-family: SimHei;\">冠牛副总裁庞晓良女士<br/></span><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103152_78286.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342061157770.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103251_85923.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>高端辩论会环节<br/><div style=\"text-align: left;\"><span style=\"font-family: SimHei; font-size: 14px; color: rgb(51, 51, 51); line-height: 2;\">&nbsp; &nbsp;另外，在大会中，冠牛参与了“致敬生态”的环保宣言，承诺致力于推动定制家居绿色可持续发展。最后，在大会的颁奖环节中，冠牛凭借23年积累的技术经验，以及推动行业发展做出的技术贡献，荣获大会颁发“技术卓越贡献奖”。<br/><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104152_93341.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342062573807.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></div><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104629_66106.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></div><br/><img src=\"/uploads/ueditor/image/20230309/1678342064414018.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div style=\"text-align: center;\"><br/>荣获大会颁发“技术卓越贡献奖”</div></div><span style=\"font-size: 12px;\"><span style=\"font-size: 14px;\"></span><br/><img src=\"/uploads/ueditor/image/20230309/1678342065105872.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>致敬生态环保宣言环节</span></div></span></div></div><p><br/></p>', '原创', '/uploads/ueditor/image/20230309/thumb_500_300_1678342055107323.jpg', 'http://test123.com/gongsixinwen/5.html', '', 1, 1, 10, 0, 0, 1, 0, '');
INSERT INTO `yzm_article` VALUES (4, 28, 1, 'yzmcms', '管理员', '伯爵实木门', '', 1678174856, 1678347665, '冠牛,木门', '马到成功运用纹理丰富的高档材质为基调，以写意的手法构画出大器天成的名门境界。它以简洁明快的风格布局平面元素，形成具有层次感的造型作品，并以精湛的工艺赋予它独特的文...', 117, '<p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-size: 14px; line-height: 2;\">2020第八届中国木门技术大会上，冠牛木门·整体家居再次斩获由中国木门技术联盟颁发的2020年度木门行业工艺技术金智奖，这是继2019年第七届中国木门技术大会以“实木门芯材防变形结构”获得工艺技术金智奖后，冠牛再次蝉联该奖项。<br/></span><br/><span style=\"font-size: 14px; line-height: 2;\"></span><img src=\"http://www.szguanniu.com/edit_file/image/20201125/20201125164928_67403.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><br/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-size: 14px; line-height: 2;\">金智奖作为表彰木门产业链先进创新技术的“奥斯卡”，是每年中国木门技术大会的最高奖项。冠牛连续两年获此殊荣，体现了大会对冠牛在实木工艺上深入创新、不断突破的认可。</span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-size: 14px;\"><br/></span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><img src=\"http://www.szguanniu.com/edit_file/image/20201125/20201125170742_51522.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal; text-align: center;\"><strong><br/></strong></p><div style=\"font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><strong><strong><span style=\"font-size: 16px; line-height: 2;\">新一代实木极简工艺技术获得金智奖垂青</span></strong></strong></div><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-size: 14px; line-height: 2;\">本次冠牛获奖技术是“实木极简木门工艺技术”，此项技术包含两个方面，一个是解决实木平板门防变形问题；另一个是解决门、墙、柜空间整体设计性问题。</span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-size: 14px; line-height: 2;\">目前市场流行的极简风格木门，大部分以平板木门形式展示，通常以桥洞力学板结构为主。而冠牛专利《平板实木门防变形结构》，不仅提高了用天然实木作为填充芯材的环保性问题，也解决了实木易变形的难题，该结构主要是对填充材——天然实木，进行两边错位的应力释放孔，以此良好平衡地释放木材的应力，使实木填充材达到相对稳定性，解决实木门的变形问题。这项独特芯材结构也完美地诠释了冠牛“善治佳木，良芯筑家”的品牌理念。</span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><br/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><img src=\"http://www.szguanniu.com/edit_file/image/20201125/20201125170857_44045.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-size: 14px; line-height: 2;\"><br/>就目前的家装设计趋势而言，木门不仅仅具有作为隔音、隔断的功能性作用，更成为家居空间整体设计的一部分，特别应与其他家装配套相辅相成，让家居空间整体视觉更和谐。木门与冠牛衣柜、墙板类产品做配套设计，木门与柜门同材装饰，为用户打造更加和谐统一的生活空间。</span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><br/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><img src=\"http://www.szguanniu.com/edit_file/image/20201125/20201125170942_53057.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><strong><span style=\"font-size: 16px;\"><br/><span style=\"line-height: 2;\">木门产业链高端对话，畅谈冠牛智造历程</span></span></strong></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-size: 14px;\"><span style=\"line-height: 2;\">在第八届中国木门技术大会的木门产业链高端对话环节，冠牛木门·整体家居公司副总裁庞晓良女士受邀登台，一同论道高端木作在智能制造上的投入与发展。</span><br/></span><span style=\"font-size: 14px; line-height: 2;\"><br/><img src=\"http://www.szguanniu.com/edit_file/image/20201125/20201125171042_26867.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><br/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-size: 14px; line-height: 2;\">庞晓良女士表示，冠牛作为智能制造的践行者，一直在思考如何将实木产品与智能制造进行融合。早在2014年冠牛就已经开始引进智能自动化设备，对于定制类家居，这是非常罕见的。虽然从传统手工制作过渡到数控设备加工是一个不容易的过程，但是一旦跨越了从手工到智能的鸿沟，智造的红利便得以展现，目前冠牛已经在工程订单上开始发力，这得益于前端的市场拓展，更重要的是后方产能的支撑。<br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20201125/20201125171106_51294.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><br/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><br/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><strong><span style=\"background-color: rgba(1, 0, 0, 0); font-size: 16px; line-height: 2;\">冠牛实木·极简新品，克服了生产端两大难题</span></strong></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"line-height: 2; font-size: 14px;\">要让实木产品适应软件与设备的标准化生产，实现生产提速，还有两个难度需要克服。第一是技术层面的难点，如何让实木平板化，成为极简风格的设计元素。极简风格的呈现需要更加简约设计，板式家居可以比较好的呈现，但是对于实木，因其自然材料特性，还需对木材进行处理后，才能实现这种极简风格。针对这个问题，冠牛专门研发出等量应力释放孔的专利技术，去实现实木平板化。<br/><img src=\"http://www.szguanniu.com/edit_file/image/20201125/20201125171220_80952.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-size: 14px;\"><span style=\"line-height: 2;\">第二是生产层面，如何缩短实木产品的生产周期，适应现在消费者对家居定制的快速交付要求。所以对于冠牛实木极简的产品，冠牛采取“双线并行，双管齐下”的生产模式。实木·极简系列完全脱离了原有经典实木的产品线，独立出新的生产线，即冠牛宿迁实木智造生产基地。从设计到下单再到生产，清一色引进全套软硬件。例如引进了智能设计系统加自动报价系统，代替传统的CAD图形设计，把设计到拆单由从原来20多天缩短至3天左右，大幅提高沟通设计效率。换言之，就是让实木产品的生产周期更符合现在消费者期待的时间，让消费者最短时间内享受高品质的实木产品。<br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20201125/20201125171630_74289.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></span><br/></span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><br/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal; text-align: center;\"><strong><span style=\"font-size: 14px;\">冠牛江苏宿迁智造生产基地</span></strong></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><br/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><strong><span style=\"font-size: 16px; line-height: 2;\">“善治佳木，良芯筑家”</span></strong><strong><span style=\"font-size: 16px; line-height: 2;\">冠牛实现从产品到智造的系统性升级</span></strong></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"line-height: 2; font-size: 14px;\">从“从容掌门”到“良芯筑家”，冠牛品牌理念的升级，背后体现的是冠牛从实木门单品向实木全屋高端定制进行延伸，着力为用户提供整体空间解决方案。这背后少不了针对用户需求量身打造出新产品如“冠牛实木·极简系列”，更少不了强大的智能制造实力在背后支撑产能与交付。</span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-size: 14px; line-height: 2;\">“善治佳木，良芯筑家”的品牌理念正在指引着冠牛在产品端、生产端完成战略部署，两句有力的口号如同两颗坚实有力的树根，源源不断向上供应营养与方向，使冠牛企业像百年参天大树一样壮大生长。</span></p><p><br/></p>', '原创', 'http://www.szguanniu.com/upimg/2019090315074741.jpg', 'http://test123.com/gongsixinwen/4.html', '', 1, 1, 10, 0, 0, 1, 0, '');
INSERT INTO `yzm_article` VALUES (5, 28, 1, 'yzmcms', '管理员', '冠牛快讯近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。', '', 1678341758, 1678343662, '冠牛,技术,召开,顺利,沈阳,辽宁', '冠牛快讯：近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。       冠牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从...', 74, '<p class=\"15\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">冠牛快讯：近日</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">2021</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。<br/><img src=\"/uploads/ueditor/image/20230309/1678342055107323.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></span></p><p><span style=\"font-family: SimHei; color: rgb(51, 51, 51); line-height: 2;\"></span></p><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div><span style=\"font-size: 14px;\"><img src=\"/uploads/ueditor/image/20230309/1678342059319269.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/>&nbsp; &nbsp;<br/>&nbsp; &nbsp; 冠</span><span style=\"font-size: 14px;\"></span><span style=\"font-size: 14px;\">牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从各自为导向的优势，来对定制木作高端发展，提供一些可借鉴的道路。</span></div><span style=\"font-size: 14px;\"><br/></span></div></div><p><span style=\"font-family: 微软雅黑; font-size: 12px;\"></span></p><p><br/></p><div style=\"font-family: 微软雅黑; font-size: 12px; white-space: normal; text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102737_85504.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342059174802.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102940_72784.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342060449292.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><span style=\"color: rgb(51, 51, 51); line-height: 2; font-family: SimHei;\">冠牛副总裁庞晓良女士<br/></span><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103152_78286.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342061157770.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103251_85923.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>高端辩论会环节<br/><div style=\"text-align: left;\"><span style=\"font-family: SimHei; font-size: 14px; color: rgb(51, 51, 51); line-height: 2;\">&nbsp; &nbsp;另外，在大会中，冠牛参与了“致敬生态”的环保宣言，承诺致力于推动定制家居绿色可持续发展。最后，在大会的颁奖环节中，冠牛凭借23年积累的技术经验，以及推动行业发展做出的技术贡献，荣获大会颁发“技术卓越贡献奖”。<br/><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104152_93341.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342062573807.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></div><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104629_66106.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></div><br/><img src=\"/uploads/ueditor/image/20230309/1678342064414018.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div style=\"text-align: center;\"><br/>荣获大会颁发“技术卓越贡献奖”</div></div><span style=\"font-size: 12px;\"><span style=\"font-size: 14px;\"></span><br/><img src=\"/uploads/ueditor/image/20230309/1678342065105872.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>致敬生态环保宣言环节</span></div></span></div></div><p><br/></p>', '原创', '/uploads/ueditor/image/20230309/thumb_500_300_1678342055107323.jpg', 'http://test123.com/gongsixinwen/5.html', '', 1, 1, 10, 0, 0, 1, 0, '');
INSERT INTO `yzm_article` VALUES (14, 31, 1, 'yzmcms', '管理员', '商学院介绍', '', 1678350473, 1678350511, '商学院,介绍', '                &ldquo;知识改变命运，学习成就未来&rdquo;，冠牛商学院的成立为冠牛人之间的相互学习、交流、共同进步和提高提供了专业平台，搭建起冠牛营销中心内部与产生部门沟通...', 69, '<p><span style=\"font-family: 微软雅黑; font-size: 12px; line-height: 2.5;\"><div style=\"text-align: center;\">&nbsp; &nbsp; &nbsp; &nbsp;<img src=\"/uploads/ueditor/image/20230309/1678350477874263.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></div></span><span style=\"font-family: 微软雅黑; font-size: 12px;\"></span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"line-height: 2.5;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style=\"color: rgb(102, 102, 102);\">&nbsp;<span style=\"font-family: SimSun; color: rgb(51, 51, 51);\">“知识改变命运，学习成就未来”，冠牛商学院的成立为冠牛人之间的相互学习、交流、共同进步和提高提供了专业平台，搭建起冠牛营销中心内部与产生部门沟通交流的桥梁，为彼此之间的工作协作创造了良好的氛围，鞭策着冠牛人向着心中的目标努力前行。</span></span></span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal; color: rgb(38, 38, 38);\" p=\"\"><span style=\"line-height: 2.5;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2009年，冠牛成立了“先锐班”。这是冠牛实习生培训模式的第一次尝试的一个范本。它为前来冠牛实习的高校毕业生提供了锻炼的机会，对他们进行了入职培训，促使其尽快进入社会，找到适合自己发展的平台。09届、10届先锐班成功地培训出一批具有专业素质和职业技能的学生，为公司的后备人才储备奠定了坚实基础。冠牛“先锐班”是商学院的前身，经过两届的举办，为日后成立冠牛商学院提供了方向。</span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal; color: rgb(38, 38, 38);\" p=\"\"><span style=\"line-height: 2.5;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2010年7月，冠牛商学院正式成立。作为冠牛公司的企业内部大学，它的成立不仅系统规范了企业内部员工和代理商员工的培训工作，建立起比较规范的培训机制，而且为传播冠牛企业文化与价值观，在市场上树立良好的公司形象、品牌形象以及信誉度有很大的促进作用。商学院旨在提升全员的职业素养，加强冠牛营销团队在公司新形势下的精气神，形成员工的主动学习氛围，逐步提升整个园队的表达、沟通和演讲能力，为以市场为主导的营销中心提供持续的激励和信心保证。</span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal; color: rgb(38, 38, 38);\" p=\"\"><span style=\"line-height: 2.5;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"line-height: 2.5;\">商学院将成为公司自制人才的地方，成为公司发展战略中的一种竞争手段。面对未来公司对人才培养的要求，商学院正不断完善，继续努力为公司人才培养做出贡献。</span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal; text-align: center; color: rgb(38, 38, 38);\" span=\"\" p=\"\"><br/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal; color: rgb(38, 38, 38);\" span=\"\" p=\"\"><span style=\"line-height: 2.5;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br/>作为冠牛商学院的前身，先锐班虽只举办了两届，但其影响却是深远的。它是冠牛实习生培训新模式的第一次尝试，成功地培训出一批具有专业素质和职业技能的学生，为公司的后备人才储备奠定了坚实基础。而对于刚出社会的职场新鲜人来说，是一次特别的经历，从中获益良多，在冠牛的成长将会让他们更自信地踏入社会，为社会创造财富。</span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal; color: rgb(38, 38, 38);\" div=\"\" span=\"\" img=\"\" src=\"http://www.szguanniu.com/xiekun/gnmy/edit_file/image/20190111/20190111100924_18734.jpg\" alt=\"\" p=\"\"><span style=\"line-height: 2.5;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span style=\"line-height: 2.5;\">冠牛商学院于2010年7月正式成立。经过两届先锐班的试水，商学院的成立意味着冠牛在人才储备的道路上积累了更多的经验，为更好地实现冠牛人自主学习，提升职业素养提供了保障。<br/></span></p><div style=\"font-family: 微软雅黑; font-size: 12px; white-space: normal; text-align: center;\"><img src=\"/uploads/ueditor/image/20230309/1678350485932809.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></div><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><br/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal; color: rgb(38, 38, 38);\" p=\"\"><span style=\"line-height: 2.5;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;从第一期到现在的第九期成功举行，都预示着作为冠牛内部大学的商学院在传播正能量的道路上又跨出了坚定的一步。还记得第一期课程主题是“如何做一个合格的区域经理”，冠牛营销中心市场部区域经理职业规划与成长演讲会激情上演，市场部六大区域精英轮番上阵，从各自对区域经理角色的独特理解出发，结合各区域市场情况，分别发表精彩演讲阐释自己的“施政理念”和行动纲领。此次演讲会是为响应冠牛集团“谋发展口促飞跃”的主题年终冲刺动员大会而举行，旨在公司年终冲刺的关键时期，以职业规划与成长为切入点，推动员工之间的互动学刁，鼓励员工找准自身定位，主动自觉寻求职业发展，最终实现与公司的共赢发展。可以说，第一期的成功举办为今后商学院开课创造了一个好的开始，更是点燃了员工们的学习热情。<br/><br/></span></p><div style=\"font-family: 微软雅黑; font-size: 12px; white-space: normal; text-align: center;\"><img src=\"http://www.szguanniu.com/xiekun/gnmy/edit_file/image/20190111/20190111101011_64154.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></div><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><br/></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal; color: rgb(38, 38, 38);\" p=\"\"><span style=\"line-height: 2.5;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;商学院第二期延续第一期的活跃氛围，以“整装筹划与店面管理”为主题，包括“公司是船我在船上”为主题的企业文化讲座和整装产品知识及VI、SI知识两部分，引导冠牛营销公司全体人员端正正确的工作态度，创造良好的工作氛围，并以知识武装自己，在今后的市场营销工作中以更专业的素质发挥更大的作用。冠牛和所有的冠牛人，就像“船与船员”，全体船员需要各司其职、同心协力，船才能乘风破浪，顺利抵达目的地。</span></p><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal; color: rgb(38, 38, 38);\" p=\"\"><span style=\"line-height: 24px;\"><span style=\"line-height: 2.5;\"></span><span style=\"line-height: 2.5;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span><span style=\"line-height: 2.5;\">商学院是冠牛正能量的代表，为冠牛人提供了学习的机会，为冠牛公司内部员工的交流和沟通提供了场所。往后的几期学爿，依然精彩不断，如第七期的“沟通技巧与感恩心态”的主题培训，专业知识技巧的指导和做人态度、人生方向的引导，全方位为冠牛人提供各种知识培训，是商学院努力的方向和奋斗的目标。相信，在冠牛人才战略调整的大前提下，冠牛商学院将会越办越好，朝着行业内最优秀企业学院的目标前进！</span></p><p><br/></p>', '原创', 'http://test123.com/uploads/ueditor/image/20230309/1678350477874263.jpg', 'http://test123.com/shangxueyuan/14.html', '', 1, 1, 10, 0, 0, 1, 0, '');
INSERT INTO `yzm_article` VALUES (7, 28, 1, 'yzmcms', '管理员', '冠牛快讯近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。', '', 1678341758, 1678343662, '冠牛,技术,召开,顺利,沈阳,辽宁', '冠牛快讯：近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。       冠牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从...', 60, '<p class=\"15\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">冠牛快讯：近日</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">2021</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。<br/><img src=\"/uploads/ueditor/image/20230309/1678342055107323.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></span></p><p><span style=\"font-family: SimHei; color: rgb(51, 51, 51); line-height: 2;\"></span></p><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div><span style=\"font-size: 14px;\"><img src=\"/uploads/ueditor/image/20230309/1678342059319269.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/>&nbsp; &nbsp;<br/>&nbsp; &nbsp; 冠</span><span style=\"font-size: 14px;\"></span><span style=\"font-size: 14px;\">牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从各自为导向的优势，来对定制木作高端发展，提供一些可借鉴的道路。</span></div><span style=\"font-size: 14px;\"><br/></span></div></div><p><span style=\"font-family: 微软雅黑; font-size: 12px;\"></span></p><p><br/></p><div style=\"font-family: 微软雅黑; font-size: 12px; white-space: normal; text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102737_85504.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342059174802.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102940_72784.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342060449292.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><span style=\"color: rgb(51, 51, 51); line-height: 2; font-family: SimHei;\">冠牛副总裁庞晓良女士<br/></span><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103152_78286.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342061157770.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103251_85923.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>高端辩论会环节<br/><div style=\"text-align: left;\"><span style=\"font-family: SimHei; font-size: 14px; color: rgb(51, 51, 51); line-height: 2;\">&nbsp; &nbsp;另外，在大会中，冠牛参与了“致敬生态”的环保宣言，承诺致力于推动定制家居绿色可持续发展。最后，在大会的颁奖环节中，冠牛凭借23年积累的技术经验，以及推动行业发展做出的技术贡献，荣获大会颁发“技术卓越贡献奖”。<br/><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104152_93341.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342062573807.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></div><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104629_66106.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></div><br/><img src=\"/uploads/ueditor/image/20230309/1678342064414018.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div style=\"text-align: center;\"><br/>荣获大会颁发“技术卓越贡献奖”</div></div><span style=\"font-size: 12px;\"><span style=\"font-size: 14px;\"></span><br/><img src=\"/uploads/ueditor/image/20230309/1678342065105872.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>致敬生态环保宣言环节</span></div></span></div></div><p><br/></p>', '原创', '/uploads/ueditor/image/20230309/thumb_500_300_1678342055107323.jpg', 'http://test123.com/gongsixinwen/5.html', '', 1, 1, 10, 0, 0, 1, 0, '');
INSERT INTO `yzm_article` VALUES (8, 28, 1, 'yzmcms', '管理员', '冠牛快讯近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。', '', 1678341758, 1678343662, '冠牛,技术,召开,顺利,沈阳,辽宁', '冠牛快讯：近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。       冠牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从...', 60, '<p class=\"15\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">冠牛快讯：近日</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">2021</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。<br/><img src=\"/uploads/ueditor/image/20230309/1678342055107323.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></span></p><p><span style=\"font-family: SimHei; color: rgb(51, 51, 51); line-height: 2;\"></span></p><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div><span style=\"font-size: 14px;\"><img src=\"/uploads/ueditor/image/20230309/1678342059319269.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/>&nbsp; &nbsp;<br/>&nbsp; &nbsp; 冠</span><span style=\"font-size: 14px;\"></span><span style=\"font-size: 14px;\">牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从各自为导向的优势，来对定制木作高端发展，提供一些可借鉴的道路。</span></div><span style=\"font-size: 14px;\"><br/></span></div></div><p><span style=\"font-family: 微软雅黑; font-size: 12px;\"></span></p><p><br/></p><div style=\"font-family: 微软雅黑; font-size: 12px; white-space: normal; text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102737_85504.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342059174802.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102940_72784.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342060449292.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><span style=\"color: rgb(51, 51, 51); line-height: 2; font-family: SimHei;\">冠牛副总裁庞晓良女士<br/></span><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103152_78286.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342061157770.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103251_85923.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>高端辩论会环节<br/><div style=\"text-align: left;\"><span style=\"font-family: SimHei; font-size: 14px; color: rgb(51, 51, 51); line-height: 2;\">&nbsp; &nbsp;另外，在大会中，冠牛参与了“致敬生态”的环保宣言，承诺致力于推动定制家居绿色可持续发展。最后，在大会的颁奖环节中，冠牛凭借23年积累的技术经验，以及推动行业发展做出的技术贡献，荣获大会颁发“技术卓越贡献奖”。<br/><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104152_93341.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342062573807.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></div><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104629_66106.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></div><br/><img src=\"/uploads/ueditor/image/20230309/1678342064414018.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div style=\"text-align: center;\"><br/>荣获大会颁发“技术卓越贡献奖”</div></div><span style=\"font-size: 12px;\"><span style=\"font-size: 14px;\"></span><br/><img src=\"/uploads/ueditor/image/20230309/1678342065105872.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>致敬生态环保宣言环节</span></div></span></div></div><p><br/></p>', '原创', '/uploads/ueditor/image/20230309/thumb_500_300_1678342055107323.jpg', 'http://test123.com/gongsixinwen/5.html', '', 1, 1, 10, 0, 0, 1, 0, '');
INSERT INTO `yzm_article` VALUES (9, 28, 1, 'yzmcms', '管理员', '冠牛快讯近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。', '', 1678341758, 1678343662, '冠牛,技术,召开,顺利,沈阳,辽宁', '冠牛快讯：近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。       冠牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从...', 60, '<p class=\"15\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">冠牛快讯：近日</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">2021</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。<br/><img src=\"/uploads/ueditor/image/20230309/1678342055107323.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></span></p><p><span style=\"font-family: SimHei; color: rgb(51, 51, 51); line-height: 2;\"></span></p><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div><span style=\"font-size: 14px;\"><img src=\"/uploads/ueditor/image/20230309/1678342059319269.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/>&nbsp; &nbsp;<br/>&nbsp; &nbsp; 冠</span><span style=\"font-size: 14px;\"></span><span style=\"font-size: 14px;\">牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从各自为导向的优势，来对定制木作高端发展，提供一些可借鉴的道路。</span></div><span style=\"font-size: 14px;\"><br/></span></div></div><p><span style=\"font-family: 微软雅黑; font-size: 12px;\"></span></p><p><br/></p><div style=\"font-family: 微软雅黑; font-size: 12px; white-space: normal; text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102737_85504.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342059174802.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102940_72784.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342060449292.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><span style=\"color: rgb(51, 51, 51); line-height: 2; font-family: SimHei;\">冠牛副总裁庞晓良女士<br/></span><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103152_78286.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342061157770.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103251_85923.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>高端辩论会环节<br/><div style=\"text-align: left;\"><span style=\"font-family: SimHei; font-size: 14px; color: rgb(51, 51, 51); line-height: 2;\">&nbsp; &nbsp;另外，在大会中，冠牛参与了“致敬生态”的环保宣言，承诺致力于推动定制家居绿色可持续发展。最后，在大会的颁奖环节中，冠牛凭借23年积累的技术经验，以及推动行业发展做出的技术贡献，荣获大会颁发“技术卓越贡献奖”。<br/><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104152_93341.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342062573807.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></div><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104629_66106.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></div><br/><img src=\"/uploads/ueditor/image/20230309/1678342064414018.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div style=\"text-align: center;\"><br/>荣获大会颁发“技术卓越贡献奖”</div></div><span style=\"font-size: 12px;\"><span style=\"font-size: 14px;\"></span><br/><img src=\"/uploads/ueditor/image/20230309/1678342065105872.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>致敬生态环保宣言环节</span></div></span></div></div><p><br/></p>', '原创', '/uploads/ueditor/image/20230309/thumb_500_300_1678342055107323.jpg', 'http://test123.com/gongsixinwen/5.html', '', 1, 1, 10, 0, 0, 1, 0, '');
INSERT INTO `yzm_article` VALUES (10, 28, 1, 'yzmcms', '管理员', '冠牛快讯近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。', '', 1678341758, 1678343662, '冠牛,技术,召开,顺利,沈阳,辽宁', '冠牛快讯：近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。       冠牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从...', 60, '<p class=\"15\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">冠牛快讯：近日</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">2021</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。<br/><img src=\"/uploads/ueditor/image/20230309/1678342055107323.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></span></p><p><span style=\"font-family: SimHei; color: rgb(51, 51, 51); line-height: 2;\"></span></p><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div><span style=\"font-size: 14px;\"><img src=\"/uploads/ueditor/image/20230309/1678342059319269.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/>&nbsp; &nbsp;<br/>&nbsp; &nbsp; 冠</span><span style=\"font-size: 14px;\"></span><span style=\"font-size: 14px;\">牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从各自为导向的优势，来对定制木作高端发展，提供一些可借鉴的道路。</span></div><span style=\"font-size: 14px;\"><br/></span></div></div><p><span style=\"font-family: 微软雅黑; font-size: 12px;\"></span></p><p><br/></p><div style=\"font-family: 微软雅黑; font-size: 12px; white-space: normal; text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102737_85504.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342059174802.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102940_72784.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342060449292.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><span style=\"color: rgb(51, 51, 51); line-height: 2; font-family: SimHei;\">冠牛副总裁庞晓良女士<br/></span><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103152_78286.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342061157770.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103251_85923.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>高端辩论会环节<br/><div style=\"text-align: left;\"><span style=\"font-family: SimHei; font-size: 14px; color: rgb(51, 51, 51); line-height: 2;\">&nbsp; &nbsp;另外，在大会中，冠牛参与了“致敬生态”的环保宣言，承诺致力于推动定制家居绿色可持续发展。最后，在大会的颁奖环节中，冠牛凭借23年积累的技术经验，以及推动行业发展做出的技术贡献，荣获大会颁发“技术卓越贡献奖”。<br/><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104152_93341.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342062573807.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></div><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104629_66106.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></div><br/><img src=\"/uploads/ueditor/image/20230309/1678342064414018.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div style=\"text-align: center;\"><br/>荣获大会颁发“技术卓越贡献奖”</div></div><span style=\"font-size: 12px;\"><span style=\"font-size: 14px;\"></span><br/><img src=\"/uploads/ueditor/image/20230309/1678342065105872.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>致敬生态环保宣言环节</span></div></span></div></div><p><br/></p>', '原创', '/uploads/ueditor/image/20230309/thumb_500_300_1678342055107323.jpg', 'http://test123.com/gongsixinwen/5.html', '', 1, 1, 10, 0, 0, 1, 0, '');
INSERT INTO `yzm_article` VALUES (11, 28, 1, 'yzmcms', '管理员', '冠牛快讯近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。', '', 1678341758, 1678343662, '冠牛,技术,召开,顺利,沈阳,辽宁', '冠牛快讯：近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。       冠牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从...', 60, '<p class=\"15\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">冠牛快讯：近日</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">2021</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。<br/><img src=\"/uploads/ueditor/image/20230309/1678342055107323.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></span></p><p><span style=\"font-family: SimHei; color: rgb(51, 51, 51); line-height: 2;\"></span></p><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div><span style=\"font-size: 14px;\"><img src=\"/uploads/ueditor/image/20230309/1678342059319269.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/>&nbsp; &nbsp;<br/>&nbsp; &nbsp; 冠</span><span style=\"font-size: 14px;\"></span><span style=\"font-size: 14px;\">牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从各自为导向的优势，来对定制木作高端发展，提供一些可借鉴的道路。</span></div><span style=\"font-size: 14px;\"><br/></span></div></div><p><span style=\"font-family: 微软雅黑; font-size: 12px;\"></span></p><p><br/></p><div style=\"font-family: 微软雅黑; font-size: 12px; white-space: normal; text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102737_85504.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342059174802.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102940_72784.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342060449292.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><span style=\"color: rgb(51, 51, 51); line-height: 2; font-family: SimHei;\">冠牛副总裁庞晓良女士<br/></span><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103152_78286.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342061157770.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103251_85923.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>高端辩论会环节<br/><div style=\"text-align: left;\"><span style=\"font-family: SimHei; font-size: 14px; color: rgb(51, 51, 51); line-height: 2;\">&nbsp; &nbsp;另外，在大会中，冠牛参与了“致敬生态”的环保宣言，承诺致力于推动定制家居绿色可持续发展。最后，在大会的颁奖环节中，冠牛凭借23年积累的技术经验，以及推动行业发展做出的技术贡献，荣获大会颁发“技术卓越贡献奖”。<br/><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104152_93341.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342062573807.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></div><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104629_66106.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></div><br/><img src=\"/uploads/ueditor/image/20230309/1678342064414018.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div style=\"text-align: center;\"><br/>荣获大会颁发“技术卓越贡献奖”</div></div><span style=\"font-size: 12px;\"><span style=\"font-size: 14px;\"></span><br/><img src=\"/uploads/ueditor/image/20230309/1678342065105872.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>致敬生态环保宣言环节</span></div></span></div></div><p><br/></p>', '原创', '/uploads/ueditor/image/20230309/thumb_500_300_1678342055107323.jpg', 'http://test123.com/gongsixinwen/5.html', '', 1, 1, 10, 0, 0, 1, 0, '');
INSERT INTO `yzm_article` VALUES (12, 28, 1, 'yzmcms', '管理员', '冠牛快讯近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。', '', 1678341758, 1678343662, '冠牛,技术,召开,顺利,沈阳,辽宁', '冠牛快讯：近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。       冠牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从...', 60, '<p class=\"15\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">冠牛快讯：近日</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">2021</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。<br/><img src=\"/uploads/ueditor/image/20230309/1678342055107323.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></span></p><p><span style=\"font-family: SimHei; color: rgb(51, 51, 51); line-height: 2;\"></span></p><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div><span style=\"font-size: 14px;\"><img src=\"/uploads/ueditor/image/20230309/1678342059319269.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/>&nbsp; &nbsp;<br/>&nbsp; &nbsp; 冠</span><span style=\"font-size: 14px;\"></span><span style=\"font-size: 14px;\">牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从各自为导向的优势，来对定制木作高端发展，提供一些可借鉴的道路。</span></div><span style=\"font-size: 14px;\"><br/></span></div></div><p><span style=\"font-family: 微软雅黑; font-size: 12px;\"></span></p><p><br/></p><div style=\"font-family: 微软雅黑; font-size: 12px; white-space: normal; text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102737_85504.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342059174802.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102940_72784.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342060449292.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><span style=\"color: rgb(51, 51, 51); line-height: 2; font-family: SimHei;\">冠牛副总裁庞晓良女士<br/></span><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103152_78286.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342061157770.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103251_85923.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>高端辩论会环节<br/><div style=\"text-align: left;\"><span style=\"font-family: SimHei; font-size: 14px; color: rgb(51, 51, 51); line-height: 2;\">&nbsp; &nbsp;另外，在大会中，冠牛参与了“致敬生态”的环保宣言，承诺致力于推动定制家居绿色可持续发展。最后，在大会的颁奖环节中，冠牛凭借23年积累的技术经验，以及推动行业发展做出的技术贡献，荣获大会颁发“技术卓越贡献奖”。<br/><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104152_93341.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342062573807.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></div><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104629_66106.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></div><br/><img src=\"/uploads/ueditor/image/20230309/1678342064414018.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div style=\"text-align: center;\"><br/>荣获大会颁发“技术卓越贡献奖”</div></div><span style=\"font-size: 12px;\"><span style=\"font-size: 14px;\"></span><br/><img src=\"/uploads/ueditor/image/20230309/1678342065105872.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>致敬生态环保宣言环节</span></div></span></div></div><p><br/></p>', '原创', '/uploads/ueditor/image/20230309/thumb_500_300_1678342055107323.jpg', 'http://test123.com/gongsixinwen/5.html', '', 1, 1, 10, 0, 0, 1, 0, '');
INSERT INTO `yzm_article` VALUES (13, 28, 1, 'yzmcms', '管理员', '冠牛快讯近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。', '', 1678341758, 1678343662, '冠牛,技术,召开,顺利,沈阳,辽宁', '冠牛快讯：近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。       冠牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从...', 60, '<p class=\"15\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; font-family: 微软雅黑; font-size: 12px; white-space: normal;\"><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">冠牛快讯：近日</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">2021</span><span style=\"font-family: SimHei; font-size: 16px; color: rgb(51, 51, 51); line-height: 2;\">年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。<br/><img src=\"/uploads/ueditor/image/20230309/1678342055107323.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></span></p><p><span style=\"font-family: SimHei; color: rgb(51, 51, 51); line-height: 2;\"></span></p><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div><span style=\"font-size: 14px;\"><img src=\"/uploads/ueditor/image/20230309/1678342059319269.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/>&nbsp; &nbsp;<br/>&nbsp; &nbsp; 冠</span><span style=\"font-size: 14px;\"></span><span style=\"font-size: 14px;\">牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从各自为导向的优势，来对定制木作高端发展，提供一些可借鉴的道路。</span></div><span style=\"font-size: 14px;\"><br/></span></div></div><p><span style=\"font-family: 微软雅黑; font-size: 12px;\"></span></p><p><br/></p><div style=\"font-family: 微软雅黑; font-size: 12px; white-space: normal; text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102737_85504.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342059174802.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217102940_72784.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342060449292.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><span style=\"color: rgb(51, 51, 51); line-height: 2; font-family: SimHei;\">冠牛副总裁庞晓良女士<br/></span><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103152_78286.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342061157770.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217103251_85923.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>高端辩论会环节<br/><div style=\"text-align: left;\"><span style=\"font-family: SimHei; font-size: 14px; color: rgb(51, 51, 51); line-height: 2;\">&nbsp; &nbsp;另外，在大会中，冠牛参与了“致敬生态”的环保宣言，承诺致力于推动定制家居绿色可持续发展。最后，在大会的颁奖环节中，冠牛凭借23年积累的技术经验，以及推动行业发展做出的技术贡献，荣获大会颁发“技术卓越贡献奖”。<br/><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104152_93341.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><img src=\"/uploads/ueditor/image/20230309/1678342062573807.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/></div><div style=\"text-align: center;\"><img src=\"http://www.szguanniu.com/edit_file/image/20211217/20211217104629_66106.png\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/></div><br/><img src=\"/uploads/ueditor/image/20230309/1678342064414018.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><div style=\"text-align: center;\"><div style=\"text-align: left;\"><div style=\"text-align: center;\"><br/>荣获大会颁发“技术卓越贡献奖”</div></div><span style=\"font-size: 12px;\"><span style=\"font-size: 14px;\"></span><br/><img src=\"/uploads/ueditor/image/20230309/1678342065105872.jpg\" alt=\"\" style=\"border: none; vertical-align: top; max-width: 100%;\"/><br/><br/>致敬生态环保宣言环节</span></div></span></div></div><p><br/></p>', '原创', '/uploads/ueditor/image/20230309/thumb_500_300_1678342055107323.jpg', 'http://test123.com/gongsixinwen/5.html', '', 1, 1, 10, 0, 0, 1, 0, '');
INSERT INTO `yzm_article` VALUES (17, 50, 1, 'yzmcms', '管理员', '福禄双全', '', 1678691301, 1678695218, '福禄双全', '福禄双全-和气生财', 133, '<p><span style=\"font-family: 微软雅黑; background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(102, 102, 102);\">采用中古对称式的平衡构图，儒雅大方。无论图形大小，它总意在体现成双成对的效果，是中国传统意义上的审美。它诠释出福禄双双而至的幸福之家。“福禄”本是福星与禄星的结合，福星主平安祥和，禄星主功名利禄。福如东海，飞黄腾达，成就完美人生。<br/></span><br style=\"color: rgb(119, 119, 119); font-family: 微软雅黑; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255);\"/><br style=\"color: rgb(119, 119, 119); font-family: 微软雅黑; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255);\"/><span style=\"font-family: 微软雅黑; background-color: rgb(255, 255, 255); font-size: 24px; color: rgb(102, 102, 102);\"></span><span style=\"font-family: 微软雅黑; background-color: rgb(255, 255, 255); font-size: 24px; color: rgb(102, 102, 102);\">和气生财</span><span style=\"color: rgb(119, 119, 119); font-family: 微软雅黑; background-color: rgb(255, 255, 255); font-size: 24px;\"><span style=\"color: rgb(102, 102, 102);\"></span><br/><span style=\"font-size: 14px; color: rgb(102, 102, 102);\"></span><span style=\"font-size: 14px; color: rgb(102, 102, 102);\"></span></span><br style=\"color: rgb(119, 119, 119); font-family: 微软雅黑; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255);\"/><span style=\"font-family: 微软雅黑; background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(102, 102, 102);\"></span><span style=\"font-family: 微软雅黑; background-color: rgb(255, 255, 255); font-size: 14px; color: rgb(102, 102, 102);\">采用纹路清晰的红橡材质，以平面造型搭配以简单的雕花和繁密的线条衬托而制成。色调与纹理相互交融，线面元素谐调搭配，给人一种整体性的视觉美感效果，表达着和气生财的美好愿望和自律。适宜各种风格的住宅门面与室内选配。</span></p>', '原创', '/uploads/ueditor/image/20230307/1678175868787799.jpg', 'http://test123.com/bojueshimumen/17.html', '', 1, 1, 10, 0, 0, 1, 0, '{\"0\":{\"url\":\"\\/uploads\\/ueditor\\/image\\/20230307\\/1678175871164509.jpg\",\"alt\":\"福禄双全-1\"},\"1\":{\"url\":\"\\/uploads\\/ueditor\\/image\\/20230307\\/1678175868787799.jpg\",\"alt\":\"福禄双全-2\"}}');

-- ----------------------------
-- Table structure for yzm_attachment
-- ----------------------------
DROP TABLE IF EXISTS `yzm_attachment`;
CREATE TABLE `yzm_attachment`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `module` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `contentid` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `originname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `filename` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `filepath` char(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `filesize` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `fileext` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `isimage` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `downloads` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `uploadtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `uploadip` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `siteid`(`siteid`) USING BTREE,
  INDEX `userid_index`(`userid`) USING BTREE,
  INDEX `contentid`(`contentid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 230 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_attachment
-- ----------------------------
INSERT INTO `yzm_attachment` VALUES (1, 0, 'admin', '', '2018071713442636.png', '230303034906603.png', '/uploads/202303/03/', 10398, 'png', 1, 0, 1, 'yzmcms', 1677829746, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (2, 0, 'banner', '', '2020092415064110.jpg', '230306032227685.jpg', '/uploads/202303/06/', 223179, 'jpg', 1, 0, 1, 'yzmcms', 1678087347, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (3, 0, 'banner', '', '2022061709291683.jpg', '230306032402100.jpg', '/uploads/202303/06/', 622625, 'jpg', 1, 0, 1, 'yzmcms', 1678087442, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (4, 0, 'banner', '', '2018090316330673.jpg', '230306032444907.jpg', '/uploads/202303/06/', 320451, 'jpg', 1, 0, 1, 'yzmcms', 1678087484, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (5, 0, 'banner', '', '2018101316100795.jpg', '230306032516261.jpg', '/uploads/202303/06/', 482599, 'jpg', 1, 0, 1, 'yzmcms', 1678087516, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (6, 0, 'admin', '', '2018071714480836.jpg', '230306041021826.jpg', '/uploads/202303/06/', 9391, 'jpg', 1, 0, 1, 'yzmcms', 1678090221, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (7, 0, 'banner', '', '2020041116134078.jpg', '230306043630157.jpg', '/uploads/202303/06/', 454250, 'jpg', 1, 0, 1, 'yzmcms', 1678091790, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (8, 0, 'banner', '', '2020060510505058.jpg', '230306044002139.jpg', '/uploads/202303/06/', 389219, 'jpg', 1, 0, 1, 'yzmcms', 1678092002, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (9, 0, 'banner', '', '2020060511043125.jpg', '230306044109564.jpg', '/uploads/202303/06/', 401239, 'jpg', 1, 0, 1, 'yzmcms', 1678092069, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (10, 0, 'index', '', '远程抓取-2019011009055415.jpg', '1678161398163342.jpg', '/uploads/ueditor/image/20230307/', 40774, 'jpg', 1, 0, 1, 'yzmcms', 1678161395, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (11, 0, 'index', '', '远程抓取-2018080821031322.jpg', '1678161404551261.jpg', '/uploads/ueditor/image/20230307/', 234312, 'jpg', 1, 0, 1, 'yzmcms', 1678161395, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (12, 0, 'index', '', '远程抓取-2018080820550012.jpg', '1678161408162187.jpg', '/uploads/ueditor/image/20230307/', 213548, 'jpg', 1, 0, 1, 'yzmcms', 1678161395, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (13, 0, 'index', '', '远程抓取-2018080820533972.jpg', '1678161415118739.jpg', '/uploads/ueditor/image/20230307/', 467582, 'jpg', 1, 0, 1, 'yzmcms', 1678161395, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (14, 0, 'index', '', '远程抓取-2018080820521285.jpg', '1678161421121076.jpg', '/uploads/ueditor/image/20230307/', 424429, 'jpg', 1, 0, 1, 'yzmcms', 1678161395, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (15, 0, 'index', '', '远程抓取-2018080820303615.jpg', '1678161424150084.jpg', '/uploads/ueditor/image/20230307/', 124806, 'jpg', 1, 0, 1, 'yzmcms', 1678161395, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (16, 0, 'index', '', '远程抓取-2021051816393083.jpg', '1678161426179856.jpg', '/uploads/ueditor/image/20230307/', 131306, 'jpg', 1, 0, 1, 'yzmcms', 1678161395, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (17, 0, 'index', '', '远程抓取-2021051816332465.jpg', '1678161436990685.jpg', '/uploads/ueditor/image/20230307/', 1001486, 'jpg', 1, 0, 1, 'yzmcms', 1678161395, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (18, 0, 'index', '', '远程抓取-2019011009055415.jpg', '1678161623276510.jpg', '/uploads/ueditor/image/20230307/', 40774, 'jpg', 1, 0, 1, 'yzmcms', 1678161623, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (19, 0, 'index', '', '远程抓取-2018080821031322.jpg', '1678161624152531.jpg', '/uploads/ueditor/image/20230307/', 234312, 'jpg', 1, 0, 1, 'yzmcms', 1678161623, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (20, 0, 'index', '', '远程抓取-2018080820550012.jpg', '1678161626210489.jpg', '/uploads/ueditor/image/20230307/', 213548, 'jpg', 1, 0, 1, 'yzmcms', 1678161623, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (21, 0, 'index', '', '远程抓取-2018080820533972.jpg', '1678161629734676.jpg', '/uploads/ueditor/image/20230307/', 467582, 'jpg', 1, 0, 1, 'yzmcms', 1678161623, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (22, 0, 'index', '', '远程抓取-2018080820521285.jpg', '1678161633756260.jpg', '/uploads/ueditor/image/20230307/', 424429, 'jpg', 1, 0, 1, 'yzmcms', 1678161623, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (23, 0, 'index', '', '远程抓取-2018080820303615.jpg', '1678161634943346.jpg', '/uploads/ueditor/image/20230307/', 124806, 'jpg', 1, 0, 1, 'yzmcms', 1678161623, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (24, 0, 'index', '', '远程抓取-2021051816393083.jpg', '1678161634178781.jpg', '/uploads/ueditor/image/20230307/', 131306, 'jpg', 1, 0, 1, 'yzmcms', 1678161623, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (25, 0, 'index', '', '远程抓取-2021051816332465.jpg', '1678161642647690.jpg', '/uploads/ueditor/image/20230307/', 1001486, 'jpg', 1, 0, 1, 'yzmcms', 1678161623, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (26, 0, 'admin', '', '2018071713442636.png', '230307021731198.png', '/uploads/202303/07/', 10398, 'png', 1, 0, 1, 'yzmcms', 1678169851, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (27, 0, 'index', '', '远程抓取-2019090315074741.jpg', '1678175267200739.jpg', '/uploads/ueditor/image/20230307/', 240944, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (28, 0, 'index', '', '远程抓取-2019090314550492.jpg', '1678175269862741.jpg', '/uploads/ueditor/image/20230307/', 267344, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (29, 0, 'index', '', '远程抓取-2019090314175642.jpg', '1678175271504392.jpg', '/uploads/ueditor/image/20230307/', 273360, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (30, 0, 'index', '', '远程抓取-2019090314153381.jpg', '1678175274129674.jpg', '/uploads/ueditor/image/20230307/', 320091, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (31, 0, 'index', '', '远程抓取-2019090314135379.jpg', '1678175276698550.jpg', '/uploads/ueditor/image/20230307/', 240853, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (32, 0, 'index', '', '远程抓取-2019090314034313.jpg', '1678175284166797.jpg', '/uploads/ueditor/image/20230307/', 287310, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (33, 0, 'index', '', '远程抓取-2019090314013077.jpg', '1678175287911815.jpg', '/uploads/ueditor/image/20230307/', 280694, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (34, 0, 'index', '', '远程抓取-2019090311572081.jpg', '1678175289176826.jpg', '/uploads/ueditor/image/20230307/', 280436, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (35, 0, 'index', '', '远程抓取-2019090311520324.jpg', '1678175292213724.jpg', '/uploads/ueditor/image/20230307/', 207299, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (36, 0, 'index', '', '远程抓取-2019090311455779.jpg', '1678175295869201.jpg', '/uploads/ueditor/image/20230307/', 274715, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (37, 0, 'index', '', '远程抓取-2019090311441198.jpg', '1678175297252404.jpg', '/uploads/ueditor/image/20230307/', 308758, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (38, 0, 'index', '', '远程抓取-2019090311392552.jpg', '1678175299557740.jpg', '/uploads/ueditor/image/20230307/', 252786, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (39, 0, 'index', '', '远程抓取-2019090311324450.jpg', '1678175301160689.jpg', '/uploads/ueditor/image/20230307/', 261639, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (40, 0, 'index', '', '远程抓取-2019090311311444.jpg', '1678175303197789.jpg', '/uploads/ueditor/image/20230307/', 240704, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (41, 0, 'index', '', '远程抓取-2019090311160888.jpg', '1678175306273844.jpg', '/uploads/ueditor/image/20230307/', 308978, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (42, 0, 'index', '', '远程抓取-2019090310584514.jpg', '1678175308136343.jpg', '/uploads/ueditor/image/20230307/', 243765, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (43, 0, 'index', '', '远程抓取-2019090309203111.jpg', '1678175313158611.jpg', '/uploads/ueditor/image/20230307/', 313374, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (44, 0, 'index', '', '远程抓取-2019090308395347.jpg', '1678175316120022.jpg', '/uploads/ueditor/image/20230307/', 287401, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (45, 0, 'index', '', '远程抓取-2019090308361216.jpg', '1678175320772006.jpg', '/uploads/ueditor/image/20230307/', 310622, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (46, 0, 'index', '', '远程抓取-2019090308163248.jpg', '1678175323155135.jpg', '/uploads/ueditor/image/20230307/', 266199, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (47, 0, 'index', '', '远程抓取-2019090218165156.jpg', '1678175326165669.jpg', '/uploads/ueditor/image/20230307/', 271694, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (48, 0, 'index', '', '远程抓取-2019090218091479.jpg', '1678175329141507.jpg', '/uploads/ueditor/image/20230307/', 293851, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (49, 0, 'index', '', '远程抓取-2019090311055798.jpg', '1678175331115686.jpg', '/uploads/ueditor/image/20230307/', 255015, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (50, 0, 'index', '', '远程抓取-2019090217195510.jpg', '1678175333161413.jpg', '/uploads/ueditor/image/20230307/', 309790, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (51, 0, 'index', '', '远程抓取-2019083118184423.jpg', '1678175336894901.jpg', '/uploads/ueditor/image/20230307/', 348576, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (52, 0, 'index', '', '远程抓取-2019090311113862.jpg', '1678175339112020.jpg', '/uploads/ueditor/image/20230307/', 307475, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (53, 0, 'index', '', '远程抓取-2019090311045388.jpg', '1678175341295926.jpg', '/uploads/ueditor/image/20230307/', 220338, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (54, 0, 'index', '', '远程抓取-2019090315242183.jpg', '1678175343149839.jpg', '/uploads/ueditor/image/20230307/', 282394, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (55, 0, 'index', '', '远程抓取-2019090315223543.jpg', '1678175346141886.jpg', '/uploads/ueditor/image/20230307/', 346852, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (56, 0, 'index', '', '远程抓取-2019090315350445.jpg', '1678175347107461.jpg', '/uploads/ueditor/image/20230307/', 221011, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (57, 0, 'index', '', '远程抓取-2019090314434784.jpg', '1678175349378819.jpg', '/uploads/ueditor/image/20230307/', 242245, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (58, 0, 'index', '', '远程抓取-2019090314322388.jpg', '1678175351520803.jpg', '/uploads/ueditor/image/20230307/', 190086, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (59, 0, 'index', '', '远程抓取-2019090308333027.jpg', '1678175352153784.jpg', '/uploads/ueditor/image/20230307/', 189827, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (60, 0, 'index', '', '远程抓取-2019090308191936.jpg', '1678175355113382.jpg', '/uploads/ueditor/image/20230307/', 326359, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (61, 0, 'index', '', '远程抓取-2019090218030428.jpg', '1678175357155779.jpg', '/uploads/ueditor/image/20230307/', 261650, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (62, 0, 'index', '', '远程抓取-2019083118133491.jpg', '1678175359188640.jpg', '/uploads/ueditor/image/20230307/', 328549, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (63, 0, 'index', '', '远程抓取-2019090315262844.jpg', '1678175361387968.jpg', '/uploads/ueditor/image/20230307/', 263013, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (64, 0, 'index', '', '远程抓取-2019030514592977.jpg', '1678175364146642.jpg', '/uploads/ueditor/image/20230307/', 310584, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (65, 0, 'index', '', '远程抓取-20190309094635_15250.jpg', '1678175364134561.jpg', '/uploads/ueditor/image/20230307/', 37566, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (66, 0, 'index', '', '远程抓取-2019030514584577.jpg', '1678175367149424.jpg', '/uploads/ueditor/image/20230307/', 431488, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (67, 0, 'index', '', '远程抓取-20190309094817_59555.jpg', '1678175368114580.jpg', '/uploads/ueditor/image/20230307/', 43915, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (68, 0, 'index', '', '远程抓取-2019030515001855.jpg', '1678175371295750.jpg', '/uploads/ueditor/image/20230307/', 428177, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (69, 0, 'index', '', '远程抓取-20190309094916_40928.jpg', '1678175372132851.jpg', '/uploads/ueditor/image/20230307/', 34886, 'jpg', 1, 0, 1, 'yzmcms', 1678175267, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (70, 0, 'index', '', '远程抓取-2019090315074741.jpg', '1678175373188754.jpg', '/uploads/ueditor/image/20230307/', 240944, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (71, 0, 'index', '', '远程抓取-2019090314550492.jpg', '1678175375540350.jpg', '/uploads/ueditor/image/20230307/', 267344, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (72, 0, 'index', '', '远程抓取-2019090314175642.jpg', '1678175378229976.jpg', '/uploads/ueditor/image/20230307/', 273360, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (73, 0, 'index', '', '远程抓取-2019090314153381.jpg', '1678175380171149.jpg', '/uploads/ueditor/image/20230307/', 320091, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (74, 0, 'index', '', '远程抓取-2019090314135379.jpg', '1678175382156969.jpg', '/uploads/ueditor/image/20230307/', 240853, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (75, 0, 'index', '', '远程抓取-2019090314034313.jpg', '1678175387113037.jpg', '/uploads/ueditor/image/20230307/', 287310, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (76, 0, 'index', '', '远程抓取-2019090314013077.jpg', '1678175391180593.jpg', '/uploads/ueditor/image/20230307/', 280694, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (77, 0, 'index', '', '远程抓取-2019090311572081.jpg', '1678175397759967.jpg', '/uploads/ueditor/image/20230307/', 280436, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (78, 0, 'index', '', '远程抓取-2019090311520324.jpg', '1678175402123593.jpg', '/uploads/ueditor/image/20230307/', 207299, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (79, 0, 'index', '', '远程抓取-2019090311455779.jpg', '1678175404199298.jpg', '/uploads/ueditor/image/20230307/', 274715, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (80, 0, 'index', '', '远程抓取-2019090311441198.jpg', '1678175407207132.jpg', '/uploads/ueditor/image/20230307/', 308758, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (81, 0, 'index', '', '远程抓取-2019090311392552.jpg', '1678175409744810.jpg', '/uploads/ueditor/image/20230307/', 252786, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (82, 0, 'index', '', '远程抓取-2019090311324450.jpg', '1678175411811041.jpg', '/uploads/ueditor/image/20230307/', 261639, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (83, 0, 'index', '', '远程抓取-2019090311311444.jpg', '1678175413156416.jpg', '/uploads/ueditor/image/20230307/', 240704, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (84, 0, 'index', '', '远程抓取-2019090311160888.jpg', '1678175416115938.jpg', '/uploads/ueditor/image/20230307/', 308978, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (85, 0, 'index', '', '远程抓取-2019090310584514.jpg', '1678175418162598.jpg', '/uploads/ueditor/image/20230307/', 243765, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (86, 0, 'index', '', '远程抓取-2019090309203111.jpg', '1678175421115932.jpg', '/uploads/ueditor/image/20230307/', 313374, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (87, 0, 'index', '', '远程抓取-2019090308395347.jpg', '1678175423212300.jpg', '/uploads/ueditor/image/20230307/', 287401, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (88, 0, 'index', '', '远程抓取-2019090308361216.jpg', '1678175426496578.jpg', '/uploads/ueditor/image/20230307/', 310622, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (89, 0, 'index', '', '远程抓取-2019090308163248.jpg', '1678175428147638.jpg', '/uploads/ueditor/image/20230307/', 266199, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (90, 0, 'index', '', '远程抓取-2019090218165156.jpg', '1678175430142198.jpg', '/uploads/ueditor/image/20230307/', 271694, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (91, 0, 'index', '', '远程抓取-2019090218091479.jpg', '1678175432164662.jpg', '/uploads/ueditor/image/20230307/', 293851, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (92, 0, 'index', '', '远程抓取-2019090311055798.jpg', '1678175434115559.jpg', '/uploads/ueditor/image/20230307/', 255015, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (93, 0, 'index', '', '远程抓取-2019090217195510.jpg', '1678175436206308.jpg', '/uploads/ueditor/image/20230307/', 309790, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (94, 0, 'index', '', '远程抓取-2019083118184423.jpg', '1678175439685702.jpg', '/uploads/ueditor/image/20230307/', 348576, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (95, 0, 'index', '', '远程抓取-2019090311113862.jpg', '1678175442871603.jpg', '/uploads/ueditor/image/20230307/', 307475, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (96, 0, 'index', '', '远程抓取-2019090311045388.jpg', '1678175444110622.jpg', '/uploads/ueditor/image/20230307/', 220338, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (97, 0, 'index', '', '远程抓取-2019090315242183.jpg', '1678175446109113.jpg', '/uploads/ueditor/image/20230307/', 282394, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (98, 0, 'index', '', '远程抓取-2019090315223543.jpg', '1678175450111147.jpg', '/uploads/ueditor/image/20230307/', 346852, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (99, 0, 'index', '', '远程抓取-2019090315350445.jpg', '1678175451139203.jpg', '/uploads/ueditor/image/20230307/', 221011, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (100, 0, 'index', '', '远程抓取-2019090314434784.jpg', '1678175454335211.jpg', '/uploads/ueditor/image/20230307/', 242245, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (101, 0, 'index', '', '远程抓取-2019090314322388.jpg', '1678175457181328.jpg', '/uploads/ueditor/image/20230307/', 190086, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (102, 0, 'index', '', '远程抓取-2019090308333027.jpg', '1678175459100468.jpg', '/uploads/ueditor/image/20230307/', 189827, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (103, 0, 'index', '', '远程抓取-2019090308191936.jpg', '1678175463179147.jpg', '/uploads/ueditor/image/20230307/', 326359, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (104, 0, 'index', '', '远程抓取-2019090218030428.jpg', '1678175470387307.jpg', '/uploads/ueditor/image/20230307/', 261650, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (105, 0, 'index', '', '远程抓取-2019083118133491.jpg', '1678175472289522.jpg', '/uploads/ueditor/image/20230307/', 328549, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (106, 0, 'index', '', '远程抓取-2019090315262844.jpg', '1678175477810014.jpg', '/uploads/ueditor/image/20230307/', 263013, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (107, 0, 'index', '', '远程抓取-2019030514592977.jpg', '1678175482269989.jpg', '/uploads/ueditor/image/20230307/', 310584, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (108, 0, 'index', '', '远程抓取-20190309094635_15250.jpg', '1678175482266722.jpg', '/uploads/ueditor/image/20230307/', 37566, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (109, 0, 'index', '', '远程抓取-2019030514584577.jpg', '1678175490191483.jpg', '/uploads/ueditor/image/20230307/', 431488, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (110, 0, 'index', '', '远程抓取-20190309094817_59555.jpg', '1678175491192846.jpg', '/uploads/ueditor/image/20230307/', 43915, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (111, 0, 'index', '', '远程抓取-2019030515001855.jpg', '1678175495196078.jpg', '/uploads/ueditor/image/20230307/', 428177, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (112, 0, 'index', '', '远程抓取-20190309094916_40928.jpg', '1678175495176072.jpg', '/uploads/ueditor/image/20230307/', 34886, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (113, 0, 'index', '', '远程抓取-2019030515005174.jpg', '1678175499582674.jpg', '/uploads/ueditor/image/20230307/', 302853, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (114, 0, 'index', '', '远程抓取-20190309094953_25632.jpg', '1678175500154386.jpg', '/uploads/ueditor/image/20230307/', 36866, 'jpg', 1, 0, 1, 'yzmcms', 1678175337, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (115, 0, 'index', '', '远程抓取-2019090315074741.jpg', '1678175502157846.jpg', '/uploads/ueditor/image/20230307/', 240944, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (116, 0, 'index', '', '远程抓取-2019090314550492.jpg', '1678175504903429.jpg', '/uploads/ueditor/image/20230307/', 267344, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (117, 0, 'index', '', '远程抓取-2019090314175642.jpg', '1678175506430326.jpg', '/uploads/ueditor/image/20230307/', 273360, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (118, 0, 'index', '', '远程抓取-2019090314153381.jpg', '1678175508175564.jpg', '/uploads/ueditor/image/20230307/', 320091, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (119, 0, 'index', '', '远程抓取-2019090314135379.jpg', '1678175510936649.jpg', '/uploads/ueditor/image/20230307/', 240853, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (120, 0, 'index', '', '远程抓取-2019090314034313.jpg', '1678175512185799.jpg', '/uploads/ueditor/image/20230307/', 287310, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (121, 0, 'index', '', '远程抓取-2019090314013077.jpg', '1678175514137374.jpg', '/uploads/ueditor/image/20230307/', 280694, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (122, 0, 'index', '', '远程抓取-2019090311572081.jpg', '1678175516183517.jpg', '/uploads/ueditor/image/20230307/', 280436, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (123, 0, 'index', '', '远程抓取-2019090311520324.jpg', '1678175518112083.jpg', '/uploads/ueditor/image/20230307/', 207299, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (124, 0, 'index', '', '远程抓取-2019090311455779.jpg', '1678175520522385.jpg', '/uploads/ueditor/image/20230307/', 274715, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (125, 0, 'index', '', '远程抓取-2019090311441198.jpg', '1678175522907559.jpg', '/uploads/ueditor/image/20230307/', 308758, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (126, 0, 'index', '', '远程抓取-2019090311392552.jpg', '1678175524125619.jpg', '/uploads/ueditor/image/20230307/', 252786, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (127, 0, 'index', '', '远程抓取-2019090311324450.jpg', '1678175526179338.jpg', '/uploads/ueditor/image/20230307/', 261639, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (128, 0, 'index', '', '远程抓取-2019090311311444.jpg', '1678175528126168.jpg', '/uploads/ueditor/image/20230307/', 240704, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (129, 0, 'index', '', '远程抓取-2019090311160888.jpg', '1678175530246921.jpg', '/uploads/ueditor/image/20230307/', 308978, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (130, 0, 'index', '', '远程抓取-2019090310584514.jpg', '1678175532211036.jpg', '/uploads/ueditor/image/20230307/', 243765, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (131, 0, 'index', '', '远程抓取-2019090309203111.jpg', '1678175535258182.jpg', '/uploads/ueditor/image/20230307/', 313374, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (132, 0, 'index', '', '远程抓取-2019090308395347.jpg', '1678175537137086.jpg', '/uploads/ueditor/image/20230307/', 287401, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (133, 0, 'index', '', '远程抓取-2019090308361216.jpg', '1678175539119339.jpg', '/uploads/ueditor/image/20230307/', 310622, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (134, 0, 'index', '', '远程抓取-2019090308163248.jpg', '1678175541505092.jpg', '/uploads/ueditor/image/20230307/', 266199, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (135, 0, 'index', '', '远程抓取-2019090218165156.jpg', '1678175543781249.jpg', '/uploads/ueditor/image/20230307/', 271694, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (136, 0, 'index', '', '远程抓取-2019090218091479.jpg', '1678175546149193.jpg', '/uploads/ueditor/image/20230307/', 293851, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (137, 0, 'index', '', '远程抓取-2019090311055798.jpg', '1678175547324973.jpg', '/uploads/ueditor/image/20230307/', 255015, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (138, 0, 'index', '', '远程抓取-2019090217195510.jpg', '1678175550184839.jpg', '/uploads/ueditor/image/20230307/', 309790, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (139, 0, 'index', '', '远程抓取-2019083118184423.jpg', '1678175552164744.jpg', '/uploads/ueditor/image/20230307/', 348576, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (140, 0, 'index', '', '远程抓取-2019090311113862.jpg', '1678175559103922.jpg', '/uploads/ueditor/image/20230307/', 307475, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (141, 0, 'index', '', '远程抓取-2019090311045388.jpg', '1678175562150379.jpg', '/uploads/ueditor/image/20230307/', 220338, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (142, 0, 'index', '', '远程抓取-2019090315242183.jpg', '1678175570978554.jpg', '/uploads/ueditor/image/20230307/', 282394, 'jpg', 1, 0, 1, 'yzmcms', 1678175393, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (143, 0, 'index', '', '远程抓取-2019090315074741.jpg', '1678175604210806.jpg', '/uploads/ueditor/image/20230307/', 240944, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (144, 0, 'index', '', '远程抓取-2019090314550492.jpg', '1678175606159837.jpg', '/uploads/ueditor/image/20230307/', 267344, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (145, 0, 'index', '', '远程抓取-2019090314175642.jpg', '1678175608205594.jpg', '/uploads/ueditor/image/20230307/', 273360, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (146, 0, 'index', '', '远程抓取-2019090314153381.jpg', '1678175610178952.jpg', '/uploads/ueditor/image/20230307/', 320091, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (147, 0, 'index', '', '远程抓取-2019090314135379.jpg', '1678175612214459.jpg', '/uploads/ueditor/image/20230307/', 240853, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (148, 0, 'index', '', '远程抓取-2019090314034313.jpg', '1678175614389462.jpg', '/uploads/ueditor/image/20230307/', 287310, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (149, 0, 'index', '', '远程抓取-2019090314013077.jpg', '1678175616366152.jpg', '/uploads/ueditor/image/20230307/', 280694, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (150, 0, 'index', '', '远程抓取-2019090311572081.jpg', '1678175618451035.jpg', '/uploads/ueditor/image/20230307/', 280436, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (151, 0, 'index', '', '远程抓取-2019090311520324.jpg', '1678175620168416.jpg', '/uploads/ueditor/image/20230307/', 207299, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (152, 0, 'index', '', '远程抓取-2019090311455779.jpg', '1678175622102609.jpg', '/uploads/ueditor/image/20230307/', 274715, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (153, 0, 'index', '', '远程抓取-2019090311441198.jpg', '1678175624142177.jpg', '/uploads/ueditor/image/20230307/', 308758, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (154, 0, 'index', '', '远程抓取-2019090311392552.jpg', '1678175634127555.jpg', '/uploads/ueditor/image/20230307/', 252786, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (155, 0, 'index', '', '远程抓取-2019090311324450.jpg', '1678175636204405.jpg', '/uploads/ueditor/image/20230307/', 261639, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (156, 0, 'index', '', '远程抓取-2019090311311444.jpg', '1678175638689714.jpg', '/uploads/ueditor/image/20230307/', 240704, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (157, 0, 'index', '', '远程抓取-2019090311160888.jpg', '1678175641210506.jpg', '/uploads/ueditor/image/20230307/', 308978, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (158, 0, 'index', '', '远程抓取-2019090310584514.jpg', '1678175643112853.jpg', '/uploads/ueditor/image/20230307/', 243765, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (159, 0, 'index', '', '远程抓取-2019090309203111.jpg', '1678175645193913.jpg', '/uploads/ueditor/image/20230307/', 313374, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (160, 0, 'index', '', '远程抓取-2019090308395347.jpg', '1678175647124376.jpg', '/uploads/ueditor/image/20230307/', 287401, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (161, 0, 'index', '', '远程抓取-2019090308361216.jpg', '1678175650192183.jpg', '/uploads/ueditor/image/20230307/', 310622, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (162, 0, 'index', '', '远程抓取-2019090308163248.jpg', '1678175652204592.jpg', '/uploads/ueditor/image/20230307/', 266199, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (163, 0, 'index', '', '远程抓取-2019090218165156.jpg', '1678175655371589.jpg', '/uploads/ueditor/image/20230307/', 271694, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (164, 0, 'index', '', '远程抓取-2019090218091479.jpg', '1678175657371417.jpg', '/uploads/ueditor/image/20230307/', 293851, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (165, 0, 'index', '', '远程抓取-2019090311055798.jpg', '1678175659438852.jpg', '/uploads/ueditor/image/20230307/', 255015, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (166, 0, 'index', '', '远程抓取-2019090217195510.jpg', '1678175662808698.jpg', '/uploads/ueditor/image/20230307/', 309790, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (167, 0, 'index', '', '远程抓取-2019083118184423.jpg', '1678175666820955.jpg', '/uploads/ueditor/image/20230307/', 348576, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (168, 0, 'index', '', '远程抓取-2019090311113862.jpg', '1678175669677457.jpg', '/uploads/ueditor/image/20230307/', 307475, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (169, 0, 'index', '', '远程抓取-2019090311045388.jpg', '1678175671105811.jpg', '/uploads/ueditor/image/20230307/', 220338, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (170, 0, 'index', '', '远程抓取-2019090315242183.jpg', '1678175674128982.jpg', '/uploads/ueditor/image/20230307/', 282394, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (171, 0, 'index', '', '远程抓取-2019090315223543.jpg', '1678175678560285.jpg', '/uploads/ueditor/image/20230307/', 346852, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (172, 0, 'index', '', '远程抓取-2019090315350445.jpg', '1678175681205294.jpg', '/uploads/ueditor/image/20230307/', 221011, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (173, 0, 'index', '', '远程抓取-2019090314434784.jpg', '1678175683206630.jpg', '/uploads/ueditor/image/20230307/', 242245, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (174, 0, 'index', '', '远程抓取-2019090314322388.jpg', '1678175685156448.jpg', '/uploads/ueditor/image/20230307/', 190086, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (175, 0, 'index', '', '远程抓取-2019090308333027.jpg', '1678175687212341.jpg', '/uploads/ueditor/image/20230307/', 189827, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (176, 0, 'index', '', '远程抓取-2019090308191936.jpg', '1678175689114032.jpg', '/uploads/ueditor/image/20230307/', 326359, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (177, 0, 'index', '', '远程抓取-2019090218030428.jpg', '1678175692212988.jpg', '/uploads/ueditor/image/20230307/', 261650, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (178, 0, 'index', '', '远程抓取-2019083118133491.jpg', '1678175696197313.jpg', '/uploads/ueditor/image/20230307/', 328549, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (179, 0, 'index', '', '远程抓取-2019090315262844.jpg', '1678175698604948.jpg', '/uploads/ueditor/image/20230307/', 263013, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (180, 0, 'index', '', '远程抓取-2019030514592977.jpg', '1678175701149647.jpg', '/uploads/ueditor/image/20230307/', 310584, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (181, 0, 'index', '', '远程抓取-20190309094635_15250.jpg', '1678175702198468.jpg', '/uploads/ueditor/image/20230307/', 37566, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (182, 0, 'index', '', '远程抓取-2019030514584577.jpg', '1678175706109603.jpg', '/uploads/ueditor/image/20230307/', 431488, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (183, 0, 'index', '', '远程抓取-20190309094817_59555.jpg', '1678175706163526.jpg', '/uploads/ueditor/image/20230307/', 43915, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (184, 0, 'index', '', '远程抓取-2019030515001855.jpg', '1678175710191492.jpg', '/uploads/ueditor/image/20230307/', 428177, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (185, 0, 'index', '', '远程抓取-20190309094916_40928.jpg', '1678175710916006.jpg', '/uploads/ueditor/image/20230307/', 34886, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (186, 0, 'index', '', '远程抓取-2019030515005174.jpg', '1678175713181462.jpg', '/uploads/ueditor/image/20230307/', 302853, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (187, 0, 'index', '', '远程抓取-20190309094953_25632.jpg', '1678175713846859.jpg', '/uploads/ueditor/image/20230307/', 36866, 'jpg', 1, 0, 1, 'yzmcms', 1678175603, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (188, 0, 'index', '', '远程抓取-2019090315074741.jpg', '1678175849364057.jpg', '/uploads/ueditor/image/20230307/', 240944, 'jpg', 1, 0, 1, 'yzmcms', 1678175848, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (189, 0, 'index', '', '远程抓取-2019090314550492.jpg', '1678175852169211.jpg', '/uploads/ueditor/image/20230307/', 267344, 'jpg', 1, 0, 1, 'yzmcms', 1678175848, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (190, 0, 'index', '', '远程抓取-2019090314175642.jpg', '1678175854198124.jpg', '/uploads/ueditor/image/20230307/', 273360, 'jpg', 1, 0, 1, 'yzmcms', 1678175848, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (191, 0, 'index', '', '远程抓取-2019090314153381.jpg', '1678175857320941.jpg', '/uploads/ueditor/image/20230307/', 320091, 'jpg', 1, 0, 1, 'yzmcms', 1678175848, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (192, 0, 'index', '', '远程抓取-2019090314135379.jpg', '1678175859106042.jpg', '/uploads/ueditor/image/20230307/', 240853, 'jpg', 1, 0, 1, 'yzmcms', 1678175848, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (193, 0, 'index', '', '远程抓取-2019090314034313.jpg', '1678175862212293.jpg', '/uploads/ueditor/image/20230307/', 287310, 'jpg', 1, 0, 1, 'yzmcms', 1678175848, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (194, 0, 'index', '', '远程抓取-2019090314013077.jpg', '1678175864408375.jpg', '/uploads/ueditor/image/20230307/', 280694, 'jpg', 1, 0, 1, 'yzmcms', 1678175848, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (195, 0, 'index', '', '远程抓取-2019090311572081.jpg', '1678175867162400.jpg', '/uploads/ueditor/image/20230307/', 280436, 'jpg', 1, 0, 1, 'yzmcms', 1678175848, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (196, 0, 'index', '', '远程抓取-2019090311520324.jpg', '1678175868787799.jpg', '/uploads/ueditor/image/20230307/', 207299, 'jpg', 1, 0, 1, 'yzmcms', 1678175848, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (197, 0, 'index', '', '远程抓取-2019090311455779.jpg', '1678175871164509.jpg', '/uploads/ueditor/image/20230307/', 274715, 'jpg', 1, 0, 1, 'yzmcms', 1678175848, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (198, 0, 'index', '', '远程抓取-20220907160157_33246.jpg', '1678342055107323.jpg', '/uploads/ueditor/image/20230309/', 150262, 'jpg', 1, 0, 1, 'yzmcms', 1678342050, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (199, 0, 'index', '', '远程抓取-20220907160223_78855.jpg', '1678342059319269.jpg', '/uploads/ueditor/image/20230309/', 145280, 'jpg', 1, 0, 1, 'yzmcms', 1678342050, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (200, 0, 'index', '', '远程抓取-20220907160453_96555.jpg', '1678342059174802.jpg', '/uploads/ueditor/image/20230309/', 119933, 'jpg', 1, 0, 1, 'yzmcms', 1678342050, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (201, 0, 'index', '', '远程抓取-20220907160538_43654.jpg', '1678342060449292.jpg', '/uploads/ueditor/image/20230309/', 86166, 'jpg', 1, 0, 1, 'yzmcms', 1678342050, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (202, 0, 'index', '', '远程抓取-20220907162155_77921.jpg', '1678342061157770.jpg', '/uploads/ueditor/image/20230309/', 122558, 'jpg', 1, 0, 1, 'yzmcms', 1678342050, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (203, 0, 'index', '', '远程抓取-20220907162230_34821.jpg', '1678342062573807.jpg', '/uploads/ueditor/image/20230309/', 141468, 'jpg', 1, 0, 1, 'yzmcms', 1678342050, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (204, 0, 'index', '', '远程抓取-20220907162246_78458.jpg', '1678342064414018.jpg', '/uploads/ueditor/image/20230309/', 151235, 'jpg', 1, 0, 1, 'yzmcms', 1678342050, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (205, 0, 'index', '', '远程抓取-20220907162349_55894.jpg', '1678342065105872.jpg', '/uploads/ueditor/image/20230309/', 95092, 'jpg', 1, 0, 1, 'yzmcms', 1678342050, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (206, 0, 'index', '', '远程抓取-20201125164928_67403.jpg', '1678347647163650.jpg', '/uploads/ueditor/image/20230309/', 124549, 'jpg', 1, 0, 1, 'yzmcms', 1678347643, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (207, 0, 'index', '', '远程抓取-20201125170742_51522.jpg', '1678347648161403.jpg', '/uploads/ueditor/image/20230309/', 79812, 'jpg', 1, 0, 1, 'yzmcms', 1678347643, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (208, 0, 'index', '', '远程抓取-20201125170857_44045.png', '1678347660174817.png', '/uploads/ueditor/image/20230309/', 530317, 'png', 1, 0, 1, 'yzmcms', 1678347643, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (209, 0, 'index', '', '远程抓取-20201125170942_53057.jpg', '1678347661143332.jpg', '/uploads/ueditor/image/20230309/', 96444, 'jpg', 1, 0, 1, 'yzmcms', 1678347643, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (210, 0, 'index', '', '远程抓取-20201125171042_26867.jpg', '1678347662108872.jpg', '/uploads/ueditor/image/20230309/', 124852, 'jpg', 1, 0, 1, 'yzmcms', 1678347643, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (211, 0, 'index', '', '远程抓取-20201125171106_51294.jpg', '1678347667210138.jpg', '/uploads/ueditor/image/20230309/', 657427, 'jpg', 1, 0, 1, 'yzmcms', 1678347643, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (212, 0, 'index', '', '远程抓取-20201125171220_80952.jpg', '1678347669761341.jpg', '/uploads/ueditor/image/20230309/', 169453, 'jpg', 1, 0, 1, 'yzmcms', 1678347643, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (213, 0, 'index', '', '远程抓取-20201125171630_74289.jpg', '1678347670200176.jpg', '/uploads/ueditor/image/20230309/', 96542, 'jpg', 1, 0, 1, 'yzmcms', 1678347643, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (214, 0, 'index', '', '远程抓取-20190415162554_43150.jpg', '1678350477874263.jpg', '/uploads/ueditor/image/20230309/', 38979, 'jpg', 1, 0, 1, 'yzmcms', 1678350476, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (215, 0, 'index', '', '远程抓取-20190415162639_54575.png', '1678350485932809.png', '/uploads/ueditor/image/20230309/', 654691, 'png', 1, 0, 1, 'yzmcms', 1678350476, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (216, 0, 'admin', '', '2018071714480836.jpg', '230309055351202.jpg', '/uploads/202303/09/', 9391, 'jpg', 1, 0, 1, 'yzmcms', 1678355631, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (217, 0, 'admin', '', '2018071714484518.jpg', '230309055528477.jpg', '/uploads/202303/09/', 9152, 'jpg', 1, 0, 1, 'yzmcms', 1678355728, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (218, 0, 'admin', '', 'indexvideo.mp4', '230311033019167.mp4', '/uploads/202303/11/', 10249606, 'mp4', 0, 0, 1, 'yzmcms', 1678519819, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (219, 0, 'admin', '', '2019011317014380.mp4', '230311033512943.mp4', '/uploads/202303/11/', 2131375, 'mp4', 0, 0, 1, 'yzmcms', 1678520112, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (220, 0, 'admin', '', '20220907160157_33246.jpg', '230313024538557.jpg', '/uploads/202303/13/', 150262, 'jpg', 1, 0, 1, 'yzmcms', 1678689938, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (221, 0, 'admin', '', '20220907160157_33246.jpg', '230313025432235.jpg', '/uploads/202303/13/', 150262, 'jpg', 1, 0, 1, 'yzmcms', 1678690472, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (222, 0, 'admin', '', '20220907160157_33246.jpg', '230313025720949.jpg', '/uploads/202303/13/', 150262, 'jpg', 1, 0, 1, 'yzmcms', 1678690640, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (223, 0, 'admin', '', '20220907160223_78855.jpg', '230313025727289.jpg', '/uploads/202303/13/', 145280, 'jpg', 1, 0, 1, 'yzmcms', 1678690647, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (224, 0, 'admin', '', '20220907160157_33246.jpg', '230313025758978.jpg', '/uploads/202303/13/', 150262, 'jpg', 1, 0, 1, 'yzmcms', 1678690678, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (225, 0, 'admin', '', '20220907160223_78855.jpg', '230313025758974.jpg', '/uploads/202303/13/', 145280, 'jpg', 1, 0, 1, 'yzmcms', 1678690678, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (226, 0, 'admin', '', '20220907160538_43654.jpg', '230313025758581.jpg', '/uploads/202303/13/', 86166, 'jpg', 1, 0, 1, 'yzmcms', 1678690678, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (227, 0, 'admin', '', '20220907162155_77921.jpg', '230313025758349.jpg', '/uploads/202303/13/', 122558, 'jpg', 1, 0, 1, 'yzmcms', 1678690678, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (228, 0, 'admin', '', '2019011317014380.mp4', '230313080405383.mp4', '/uploads/202303/13/', 2131375, 'mp4', 0, 0, 1, 'yzmcms', 1678709045, '127.0.0.1');
INSERT INTO `yzm_attachment` VALUES (229, 0, 'admin', '', '分销协议模板.docx', '230313080837798.docx', '/uploads/202303/13/', 14068, 'docx', 0, 0, 1, 'yzmcms', 1678709317, '127.0.0.1');

-- ----------------------------
-- Table structure for yzm_banner
-- ----------------------------
DROP TABLE IF EXISTS `yzm_banner`;
CREATE TABLE `yzm_banner`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `image` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `introduce` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '简介',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `listorder` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `typeid` tinyint(2) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '1显示0隐藏',
  `url1` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `status`(`status`) USING BTREE,
  INDEX `typeid`(`typeid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_banner
-- ----------------------------
INSERT INTO `yzm_banner` VALUES (1, '1', '/uploads/202303/06/230306032227685.jpg', 'http://test123.com/', '', 1678087389, 1, 1, 1, '');
INSERT INTO `yzm_banner` VALUES (2, '2', '/uploads/202303/06/230306032402100.jpg', 'http://test123.com/', '', 1678087449, 2, 1, 1, '');
INSERT INTO `yzm_banner` VALUES (3, '3', '/uploads/202303/06/230306032444907.jpg', 'http://test123.com/', '', 1678087501, 3, 1, 1, '');
INSERT INTO `yzm_banner` VALUES (4, '4', '/uploads/202303/06/230306032516261.jpg', 'http://test123.com/', '', 1678087522, 4, 1, 1, '');
INSERT INTO `yzm_banner` VALUES (5, '极简中式', '/uploads/202303/06/230306043630157.jpg', 'http://test123.com/', '首页产品轮播，产品1分类有两个链接，链接地址为查看详情地址，链接地址1为商城购买地址', 1678091933, 2, 2, 1, 'http://test123.com/');
INSERT INTO `yzm_banner` VALUES (6, '极简意式', '/uploads/202303/06/230306044002139.jpg', 'http://test123.com/', '', 1678092040, 1, 2, 1, 'http://test123.com/');
INSERT INTO `yzm_banner` VALUES (7, '实木·极简实景案例', '/uploads/202303/06/230306044109564.jpg', 'http://test123.com/', '', 1678092115, 3, 2, 1, 'http://test123.com/');

-- ----------------------------
-- Table structure for yzm_banner_type
-- ----------------------------
DROP TABLE IF EXISTS `yzm_banner_type`;
CREATE TABLE `yzm_banner_type`  (
  `tid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`tid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of yzm_banner_type
-- ----------------------------
INSERT INTO `yzm_banner_type` VALUES (1, '首页轮播');
INSERT INTO `yzm_banner_type` VALUES (2, '首页产品1');

-- ----------------------------
-- Table structure for yzm_category
-- ----------------------------
DROP TABLE IF EXISTS `yzm_category`;
CREATE TABLE `yzm_category`  (
  `catid` smallint(5) NOT NULL AUTO_INCREMENT COMMENT '栏目ID',
  `siteid` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `catname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '栏目名称',
  `modelid` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '模型id',
  `parentid` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '父级id',
  `arrparentid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '父级路径',
  `arrchildid` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '子栏目id集合',
  `catdir` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '栏目目录',
  `catimg` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '栏目图片',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '栏目类型:0普通栏目1单页2外部链接',
  `listorder` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '栏目排序',
  `target` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '打开方式',
  `member_publish` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否会员投稿',
  `display` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '在导航显示',
  `pclink` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '电脑版地址',
  `domain` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '绑定域名',
  `entitle` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '英文标题',
  `subtitle` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '副标题',
  `mobname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '手机版名称',
  `category_template` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '频道页模板',
  `list_template` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '列表页模板',
  `show_template` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '内容页模板',
  `seo_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SEO标题',
  `seo_keywords` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SEO关键字',
  `seo_description` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SEO描述',
  PRIMARY KEY (`catid`) USING BTREE,
  INDEX `siteid`(`siteid`) USING BTREE,
  INDEX `modelid`(`modelid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 57 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_category
-- ----------------------------
INSERT INTO `yzm_category` VALUES (51, 0, '简时代实木门', 1, 21, '0,8,21', '51', 'jianshidaishimumen', '', 0, 0, '_self', 0, 1, 'http://test123.com/jianshidaishimumen/', '', 'Crown cattle door', '实芯实木，表里如一。每一扇冠牛实木门，都用料十足、分量沉实。我们坚持表里如一、内外一致——门扇里外所有部件都采用同一种天', '简时代实木门', 'category_article_product', 'list_article_pro1', 'show_article_pro', '', '', '');
INSERT INTO `yzm_category` VALUES (52, 0, 'e时代实木门', 1, 21, '0,8,21', '52', 'eshidaishimumen', '', 0, 0, '_self', 0, 1, 'http://test123.com/eshidaishimumen/', '', '', '', 'e时代实木门', 'category_article_product', 'list_article_pro1', 'show_article_pro', '', '', '');
INSERT INTO `yzm_category` VALUES (50, 0, '伯爵实木门', 1, 21, '0,8,21', '50', 'bojueshimumen', '', 0, 0, '_self', 0, 1, 'http://test123.com/bojueshimumen/', '', 'Crown cattle door', '实芯实木，表里如一。每一扇冠牛实木门，都用料十足、分量沉实。我们坚持表里如一、内外一致——门扇里外所有部件都采用同一种天', '伯爵实木门', 'category_article_product', 'list_article_pro1', 'show_article_pro', '', '', '');
INSERT INTO `yzm_category` VALUES (49, 0, '下载中心', 3, 0, '0', '49', 'xiazaizhongxin', '', 0, 0, '_self', 0, 0, 'http://test123.com/xiazaizhongxin/', '', '', '', '下载中心', 'category_download', 'list_download', 'show_download', '', '', '');
INSERT INTO `yzm_category` VALUES (7, 0, '我们的冠牛', 1, 0, '0', '7,14,15,16,18,19,20', 'about', '', 0, 0, '_self', 0, 1, 'http://test123.com/about/', '', '', '', '我们的冠牛', 'category_article', 'list_article', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (8, 0, '我们的产品', 1, 0, '0', '8,21,22,23,50,51,52,53,54,55,56', 'product', '', 0, 0, '_self', 0, 1, 'http://test123.com/product/', '', '', '', '我们的产品', 'category_article_product', 'list_article', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (9, 0, '资讯中心', 1, 0, '0', '9,28,29,30', 'news', '', 0, 0, '_self', 0, 1, 'http://test123.com/news/', '', '', '', '资讯中心', 'category_article_news', 'list_article', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (10, 0, '冠牛商学院', 1, 0, '0', '10,31,32,33,34', 'school', '', 0, 0, '_self', 0, 1, 'http://test123.com/school/', '', '', '', '冠牛商学院', 'category_article_school', 'list_article_news1', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (11, 0, '加盟冠牛', 1, 0, '0', '11,35,36,37,38,39', 'jiamengguanniu', '', 0, 0, '_self', 0, 1, 'http://test123.com/jiamengguanniu/', '', '', '', '加盟冠牛', 'category_article_join', 'list_article', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (12, 0, '工程接洽', 1, 0, '0', '12,40,41,42,43', 'gongchengjieqia', '', 0, 0, '_self', 0, 1, 'http://test123.com/gongchengjieqia/', '', '', '', '工程接洽', 'category_article_show', 'list_article', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (13, 0, '我们的服务', 1, 0, '0', '13,44,45,46', 'womendefuwu', '', 0, 0, '_self', 0, 1, 'http://test123.com/womendefuwu/', '', '', '', '我们的服务', 'category_article_service', 'list_article', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (14, 0, '关于冠牛', 1, 7, '0,7', '14', 'guanyuguanniu', '', 0, 0, '_self', 0, 1, 'http://test123.com/guanyuguanniu/', '', '', '', '关于冠牛', 'category_article', 'list_article', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (15, 0, '冠牛品牌', 1, 7, '0,7', '15', 'brand', '', 0, 0, '_self', 0, 1, 'http://test123.com/brand/', '', '', '', '冠牛品牌', 'category_article', 'list_article_brand', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (16, 0, '冠牛荣誉', 1, 7, '0,7', '16', 'guanniurongyu', '', 0, 0, '_self', 0, 1, 'http://test123.com/guanniurongyu/', '', '', '', '冠牛荣誉', 'category_article', 'list_article_rongyu', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (19, 0, '发展历程', 1, 7, '0,7', '19', 'fazhanlicheng', '', 0, 0, '_self', 0, 1, 'http://test123.com/fazhanlicheng/', '', '', '', '发展历程', 'category_article', 'list_article_licheng', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (18, 0, '4.0智造', 1, 7, '0,7', '18', 'zhizao', '', 0, 0, '_self', 0, 1, 'http://test123.com/zhizao/', '', '', '', '4.0智造', 'category_article', 'list_article_zhizao', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (20, 0, '董事长致辞', 1, 7, '0,7', '20', 'dongshichangzhici', '', 0, 0, '_self', 0, 1, 'http://test123.com/dongshichangzhici/', '', '', '', '董事长致辞', 'category_article', 'list_article_zhici', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (21, 0, '冠牛木门', 1, 8, '0,8', '21,50,51,52,53', 'guanniumumen', '', 0, 0, '_self', 0, 1, 'http://test123.com/guanniumumen/', '', 'CROWN CATTLE DOOR', '实芯实木，表里如一。每一扇冠牛实木门，都用料十足、分量沉实。我们坚持表里如一、内外一致——门扇里外所有部件都采用同一种天', '冠牛木门', 'category_article_product', 'list_article_pro1', 'show_article_pro', '', '', '');
INSERT INTO `yzm_category` VALUES (22, 0, '整体家居', 1, 8, '0,8', '22,54,55,56', 'zhengtijiaji', '', 0, 0, '_self', 0, 1, 'http://test123.com/zhengtijiaji/', '', 'INTEGRAL HOUSEHOLD', '', '整体家居', 'category_article_product2', 'list_article_pro2', 'show_article_pro', '', '', '');
INSERT INTO `yzm_category` VALUES (23, 0, '实景案例', 1, 8, '0,8', '23', 'shijianli', '', 0, 0, '_self', 0, 1, 'http://test123.com/shijianli/', '', 'REAL CASE', '', '实景案例', 'category_article', 'list_article_pro3', 'show_article_pro', '', '', '');
INSERT INTO `yzm_category` VALUES (28, 0, '公司新闻', 1, 9, '0,9', '28', 'gongsixinwen', '', 0, 0, '_self', 0, 1, 'http://test123.com/gongsixinwen/', '', '', '', '公司新闻', 'category_article', 'list_article_news1', 'show_article_news', '', '', '');
INSERT INTO `yzm_category` VALUES (30, 0, '行业资讯', 1, 9, '0,9', '30', 'xingyezixun', '', 0, 0, '_self', 0, 1, 'http://test123.com/xingyezixun/', '', '', '', '行业资讯', 'category_article', 'list_article_news3', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (29, 0, '品牌活动', 1, 9, '0,9', '29', 'pinpaihuodong', '', 0, 0, '_self', 0, 1, 'http://test123.com/pinpaihuodong/', '', '', '', '品牌活动', 'category_article', 'list_article_news2', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (31, 0, '商学院', 1, 10, '0,10', '31', 'shangxueyuan', '', 0, 0, '_self', 0, 1, 'http://test123.com/shangxueyuan/', '', '', '', '商学院', 'category_article_school', 'list_article_news1', 'show_article_school', '', '', '');
INSERT INTO `yzm_category` VALUES (32, 0, '优秀员工', 1, 10, '0,10', '32', 'youxiuyuangong', '', 0, 0, '_self', 0, 1, 'http://test123.com/youxiuyuangong/', '', '', '', '优秀员工', 'category_article_school', 'list_article_school1', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (33, 0, '知名讲师', 1, 10, '0,10', '33', 'zhimingjiangshi', '', 0, 0, '_self', 0, 1, 'http://test123.com/zhimingjiangshi/', '', '', '', '知名讲师', 'category_article_school', 'list_article_school2', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (34, 0, '学员风采', 1, 10, '0,10', '34', 'xueyuanfengcai', '', 0, 0, '_self', 0, 1, 'http://test123.com/xueyuanfengcai/', '', '', '', '学员风采', 'category_article_school', 'list_article_school3', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (35, 0, '品牌优势', 1, 11, '0,11', '35', 'pinpaiyoushi', '', 0, 0, '_self', 0, 1, 'http://test123.com/pinpaiyoushi/', '', '', '', '品牌优势', 'category_article_join', 'list_article_join1', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (36, 0, '行业优势', 1, 11, '0,11', '36', 'xingyeyoushi', '', 0, 0, '_self', 0, 1, 'http://test123.com/xingyeyoushi/', '', '', '', '行业优势', 'category_article_join', 'list_article_join2', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (37, 0, '加盟支持', 1, 11, '0,11', '37', 'jiamengzhichi', '', 0, 0, '_self', 0, 1, 'http://test123.com/jiamengzhichi/', '', '', '', '加盟支持', 'category_article_join', 'list_article_join3', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (38, 0, '加盟流程', 1, 11, '0,11', '38', 'jiamengliucheng', '', 0, 0, '_self', 0, 1, 'http://test123.com/jiamengliucheng/', '', '', '', '加盟流程', 'category_article_join', 'list_article_join4', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (39, 0, '我要加盟', 1, 11, '0,11', '39', 'woyaojiameng', '', 0, 0, '_self', 0, 1, 'http://test123.com/woyaojiameng/', '', '', '', '我要加盟', 'category_article_join', 'list_article_join5', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (40, 0, '企业工程项目', 1, 12, '0,12', '40', 'qiyegongchengxiangmu', '', 0, 0, '_self', 0, 1, 'http://test123.com/qiyegongchengxiangmu/', '', '', '', '企业工程项目', 'category_article', 'list_article_show1', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (41, 0, '地产项目', 1, 12, '0,12', '41', 'dichanxiangmu', '', 0, 0, '_self', 0, 1, 'http://test123.com/dichanxiangmu/', '', '', '', '地产项目', 'category_article', 'list_article_show2', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (42, 0, '别墅项目', 1, 12, '0,12', '42', 'bieshuxiangmu', '', 0, 0, '_self', 0, 1, 'http://test123.com/bieshuxiangmu/', '', '', '', '别墅项目', 'category_article', 'list_article_show3', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (43, 0, '工程项目联系', 1, 12, '0,12', '43', 'gongchengxiangmulianxi', '', 0, 0, '_self', 0, 1, 'http://test123.com/gongchengxiangmulianxi/', '', '', '', '工程项目联系', 'category_article', 'list_article_show4', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (44, 0, '自助反馈', 1, 13, '0,13', '44', 'zizhufankui', '', 0, 0, '_self', 0, 1, 'http://test123.com/zizhufankui/', '', '', '', '自助反馈', 'category_article', 'list_article_service2', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (45, 0, '联系我们', 1, 13, '0,13', '45', 'lianxiwomen', '', 0, 0, '_self', 0, 1, 'http://test123.com/lianxiwomen/', '', '', '', '联系我们', 'category_article', 'list_article_service1', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (46, 0, '全国经销商门店查询', 1, 13, '0,13', '46', 'search', '', 0, 0, '_self', 0, 1, 'http://test123.com/search/', '', '', '', '全国经销商门店查询', 'category_article', 'list_article_service3', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (47, 0, '产品详情', 1, 0, '0', '47', 'chanpinxiangqing', '', 0, 0, '_self', 0, 0, 'http://test123.com/chanpinxiangqing/', '', '', '', '产品详情', 'category_article_pro_content', 'list_article_pro_content', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (48, 0, '新闻详情', 1, 0, '0', '48', 'xinwenxiangqing', '', 0, 0, '_self', 0, 0, 'http://test123.com/xinwenxiangqing/', '', '', '', '新闻详情', 'category_article', 'list_article_news_content', 'show_article', '', '', '');
INSERT INTO `yzm_category` VALUES (53, 0, '工艺优势', 1, 21, '0,8,21', '53', 'gongyiyoushi', '', 0, 0, '_self', 0, 1, 'http://test123.com/gongyiyoushi/', '', '', '', '工艺优势', 'category_article_product', 'list_article_pro1', 'show_article_pro', '', '', '');
INSERT INTO `yzm_category` VALUES (54, 0, '实木·经典——伯爵', 1, 22, '0,8,22', '54', 'shimujingdianbojue', '', 0, 0, '_self', 0, 1, 'http://test123.com/shimujingdianbojue/', '', '', '', '实木·经典——伯爵', 'category_article', 'list_article_pro2', 'show_article_pro', '', '', '');
INSERT INTO `yzm_category` VALUES (55, 0, '实木·简约——简时代', 1, 22, '0,8,22', '55', 'shimujianyuejianshidai', '', 0, 0, '_self', 0, 1, 'http://test123.com/shimujianyuejianshidai/', '', '', '', '实木·简约——简时代', 'category_article', 'list_article_pro2', 'show_article_pro', '', '', '');
INSERT INTO `yzm_category` VALUES (56, 0, '实木·极简', 1, 22, '0,8,22', '56', 'shimujijian', '', 0, 0, '_self', 0, 1, 'http://test123.com/shimujijian/', '', '', '', '实木·极简', 'category_article', 'list_article_pro2', 'show_article_pro', '', '', '');

-- ----------------------------
-- Table structure for yzm_collection_content
-- ----------------------------
DROP TABLE IF EXISTS `yzm_collection_content`;
CREATE TABLE `yzm_collection_content`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nodeid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0:未采集,1:已采集,2:已导入',
  `url` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `title` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `data` mediumtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `nodeid`(`nodeid`) USING BTREE,
  INDEX `status`(`status`) USING BTREE,
  INDEX `url`(`url`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_collection_content
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_collection_node
-- ----------------------------
DROP TABLE IF EXISTS `yzm_collection_node`;
CREATE TABLE `yzm_collection_node`  (
  `nodeid` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '采集节点ID',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '节点名称',
  `lastdate` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '最后采集时间',
  `sourcecharset` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '采集点字符集',
  `sourcetype` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '网址类型:1序列网址,2单页',
  `urlpage` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '采集地址',
  `pagesize_start` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '页码开始',
  `pagesize_end` mediumint(8) UNSIGNED NOT NULL DEFAULT 0 COMMENT '页码结束',
  `par_num` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '每次增加数',
  `url_contain` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '网址中必须包含',
  `url_except` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '网址中不能包含',
  `url_start` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '网址开始',
  `url_end` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '网址结束',
  `title_rule` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '标题采集规则',
  `title_html_rule` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题过滤规则',
  `time_rule` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '时间采集规则',
  `time_html_rule` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '时间过滤规则',
  `content_rule` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '内容采集规则',
  `content_html_rule` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容过滤规则',
  `down_attachment` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否下载图片',
  `watermark` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图片加水印',
  `coll_order` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '导入顺序',
  PRIMARY KEY (`nodeid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_collection_node
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_comment
-- ----------------------------
DROP TABLE IF EXISTS `yzm_comment`;
CREATE TABLE `yzm_comment`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `commentid` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `userpic` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '评论状态{0:未审核,1:通过审核}',
  `reply` mediumint(8) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为回复',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `siteid`(`siteid`) USING BTREE,
  INDEX `userid`(`userid`) USING BTREE,
  INDEX `commentid`(`commentid`, `status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_comment
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_comment_data
-- ----------------------------
DROP TABLE IF EXISTS `yzm_comment_data`;
CREATE TABLE `yzm_comment_data`  (
  `commentid` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `siteid` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `title` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `total` int(8) UNSIGNED NOT NULL DEFAULT 0,
  `catid` smallint(4) UNSIGNED NOT NULL DEFAULT 0,
  `modelid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`commentid`) USING BTREE,
  INDEX `siteid`(`siteid`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_comment_data
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_config
-- ----------------------------
DROP TABLE IF EXISTS `yzm_config`;
CREATE TABLE `yzm_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '配置名称',
  `type` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '配置类型',
  `title` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '配置说明',
  `value` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '配置值',
  `fieldtype` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '字段类型',
  `setting` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '字段设置',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `type`(`type`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 78 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_config
-- ----------------------------
INSERT INTO `yzm_config` VALUES (1, 'site_name', 0, '站点名称', '冠牛木门', '', '', 1);
INSERT INTO `yzm_config` VALUES (2, 'site_url', 0, '站点根网址', 'http://test123.com/', '', '', 1);
INSERT INTO `yzm_config` VALUES (3, 'site_keyword', 0, '站点关键字', '冠牛木门', '', '', 1);
INSERT INTO `yzm_config` VALUES (4, 'site_description', 0, '站点描述', '冠牛公司20余年专注实木家居定制服务领域，在为消费者提供高品质产品及服务的同时，争取为社会提供更多就业机会、为投资者创造更大商业价值。', '', '', 1);
INSERT INTO `yzm_config` VALUES (5, 'site_copyright', 0, '版权信息', 'Powered By 冠牛木门官方网站 © 2014-2099', '', '', 1);
INSERT INTO `yzm_config` VALUES (6, 'site_filing', 0, '站点备案号', '粤ICP备19026923号', '', '', 1);
INSERT INTO `yzm_config` VALUES (7, 'site_code', 0, '统计代码', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (8, 'site_theme', 0, '站点模板主题', 'gj_company', '', '', 1);
INSERT INTO `yzm_config` VALUES (9, 'site_logo', 0, '站点logo', '/uploads/202303/07/230307021731198.png', '', '', 1);
INSERT INTO `yzm_config` VALUES (10, 'url_mode', 0, '前台URL模式', '1', '', '', 1);
INSERT INTO `yzm_config` VALUES (11, 'is_words', 0, '是否开启前端留言功能', '0', '', '', 1);
INSERT INTO `yzm_config` VALUES (12, 'upload_maxsize', 0, '文件上传最大限制', '20480', '', '', 1);
INSERT INTO `yzm_config` VALUES (13, 'upload_types', 0, '允许上传附件类型', 'zip|rar|ppt|doc|xls|mp4|jpg|png|docx|pdf', '', '', 1);
INSERT INTO `yzm_config` VALUES (14, 'upload_image_types', 0, '允许上传图片类型', 'png|jpg|jpeg|gif', '', '', 1);
INSERT INTO `yzm_config` VALUES (15, 'watermark_enable', 0, '是否开启图片水印', '0', '', '', 1);
INSERT INTO `yzm_config` VALUES (16, 'watermark_name', 0, '水印图片名称', 'mark.png', '', '', 1);
INSERT INTO `yzm_config` VALUES (17, 'watermark_position', 0, '水印的位置', '9', '', '', 1);
INSERT INTO `yzm_config` VALUES (18, 'mail_server', 1, 'SMTP服务器', 'ssl://smtp.qq.com', '', '', 1);
INSERT INTO `yzm_config` VALUES (19, 'mail_port', 1, 'SMTP服务器端口', '465', '', '', 1);
INSERT INTO `yzm_config` VALUES (20, 'mail_from', 1, 'SMTP服务器的用户邮箱', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (21, 'mail_auth', 1, 'AUTH LOGIN验证', '1', '', '', 1);
INSERT INTO `yzm_config` VALUES (22, 'mail_user', 1, 'SMTP服务器的用户帐号', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (23, 'mail_pass', 1, 'SMTP服务器的用户密码', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (24, 'mail_inbox', 1, '收件邮箱地址', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (25, 'admin_log', 2, '启用后台管理操作日志', '0', '', '', 1);
INSERT INTO `yzm_config` VALUES (26, 'admin_prohibit_ip', 2, '禁止登录后台的IP', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (27, 'prohibit_words', 2, '屏蔽词', '她妈|它妈|他妈|你妈|去死|贱人', '', '', 1);
INSERT INTO `yzm_config` VALUES (28, 'comment_check', 2, '是否开启评论审核', '0', '', '', 1);
INSERT INTO `yzm_config` VALUES (29, 'comment_tourist', 2, '是否允许游客评论', '0', '', '', 1);
INSERT INTO `yzm_config` VALUES (30, 'is_link', 2, '允许用户申请友情链接', '0', '', '', 1);
INSERT INTO `yzm_config` VALUES (31, 'member_register', 3, '是否开启会员注册', '0', '', '', 1);
INSERT INTO `yzm_config` VALUES (32, 'member_email', 3, '新会员注册是否需要邮件验证', '0', '', '', 1);
INSERT INTO `yzm_config` VALUES (33, 'member_check', 3, '新会员注册是否需要管理员审核', '0', '', '', 1);
INSERT INTO `yzm_config` VALUES (34, 'member_point', 3, '新会员默认积分', '0', '', '', 1);
INSERT INTO `yzm_config` VALUES (35, 'member_yzm', 3, '是否开启会员登录验证码', '1', '', '', 1);
INSERT INTO `yzm_config` VALUES (36, 'rmb_point_rate', 3, '1元人民币购买积分数量', '10', '', '', 1);
INSERT INTO `yzm_config` VALUES (37, 'login_point', 3, '每日登录奖励积分', '1', '', '', 1);
INSERT INTO `yzm_config` VALUES (38, 'comment_point', 3, '发布评论奖励积分', '1', '', '', 1);
INSERT INTO `yzm_config` VALUES (39, 'publish_point', 3, '投稿奖励积分', '3', '', '', 1);
INSERT INTO `yzm_config` VALUES (40, 'qq_app_id', 3, 'QQ App ID', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (41, 'qq_app_key', 3, 'QQ App key', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (42, 'weibo_key', 4, '微博登录App Key', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (43, 'weibo_secret', 4, '微博登录App Secret', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (44, 'wx_appid', 4, '微信开发者ID', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (45, 'wx_secret', 4, '微信开发者密码', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (46, 'wx_token', 4, '微信Token签名', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (47, 'wx_encodingaeskey', 4, '微信EncodingAESKey', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (48, 'wx_relation_model', 4, '微信关联模型', 'article', '', '', 1);
INSERT INTO `yzm_config` VALUES (49, 'baidu_push_token', 0, '百度推送token', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (50, 'thumb_width', 2, '缩略图默认宽度', '500', '', '', 1);
INSERT INTO `yzm_config` VALUES (51, 'thumb_height', 2, '缩略图默认高度', '300', '', '', 1);
INSERT INTO `yzm_config` VALUES (52, 'site_seo_division', 0, '站点标题分隔符', '_', '', '', 1);
INSERT INTO `yzm_config` VALUES (53, 'keyword_link', 2, '是否启用关键字替换', '0', '', '', 1);
INSERT INTO `yzm_config` VALUES (54, 'keyword_replacenum', 2, '关键字替换次数', '1', '', '', 1);
INSERT INTO `yzm_config` VALUES (55, 'error_log_save', 2, '是否保存系统错误日志', '1', '', '', 1);
INSERT INTO `yzm_config` VALUES (56, 'comment_code', 2, '是否开启评论验证码', '0', '', '', 1);
INSERT INTO `yzm_config` VALUES (57, 'site_wap_open', 0, '是否启用手机站点', '1', '', '', 1);
INSERT INTO `yzm_config` VALUES (58, 'site_wap_theme', 0, 'WAP端模板风格', 'default', '', '', 1);
INSERT INTO `yzm_config` VALUES (59, 'member_theme', 3, '会员中心模板风格', 'default', '', '', 1);
INSERT INTO `yzm_config` VALUES (60, 'att_relation_content', 1, '是否开启内容附件关联', '0', '', '', 1);
INSERT INTO `yzm_config` VALUES (61, 'site_seo_suffix', 0, '站点SEO后缀', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (62, 'site_security_number', 0, '公安备案号', '', '', ' ', 1);
INSERT INTO `yzm_config` VALUES (63, 'words_code', 3, '是否开启留言验证码', '1', '', '', 1);
INSERT INTO `yzm_config` VALUES (64, 'watermark_minwidth', 2, '添加水印最小宽度', '300', '', '', 1);
INSERT INTO `yzm_config` VALUES (65, 'watermark_minheight', 2, '添加水印最小高度', '300', '', '', 1);
INSERT INTO `yzm_config` VALUES (66, 'auto_down_imag', 2, '自动下载远程图片', '1', '', '', 1);
INSERT INTO `yzm_config` VALUES (67, 'down_ignore_domain', 2, '下载远程图片忽略的域名', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (68, 'content_click_random', 2, '内容默认点击量', '1', '', '', 1);
INSERT INTO `yzm_config` VALUES (69, 'blacklist_ip', 3, ' 前端IP黑名单', '', '', '', 1);
INSERT INTO `yzm_config` VALUES (70, 'advertise', 99, '首页广告位', '冠牛木门官网平台', 'textarea', '', 1);
INSERT INTO `yzm_config` VALUES (71, 'kefu', 99, '顶部客服联系电话', '客服/招商电话:400-990-3306', 'textarea', '', 1);
INSERT INTO `yzm_config` VALUES (72, 'phone', 99, '侧边栏客服电话', '400-990-3306', 'textarea', '', 1);
INSERT INTO `yzm_config` VALUES (73, 'qq', 99, '侧边栏qq客服跳转链接', 'http://wpa.qq.com/msgrd?v=3&uin=2813566522&site=qq&menu=yes', 'textarea', '', 1);
INSERT INTO `yzm_config` VALUES (74, 'qrcode', 99, '侧边栏客服二维码图片', '/uploads/202303/06/230306041021826.jpg', 'image', '', 1);
INSERT INTO `yzm_config` VALUES (75, 'wei_qrcode', 99, '官方微信二维码图片', '/uploads/202303/09/230309055351202.jpg', 'image', '', 1);
INSERT INTO `yzm_config` VALUES (76, 'mobile_qrcode', 99, '手机网站二维码图片', '/uploads/202303/09/230309055528477.jpg', 'image', '', 1);
INSERT INTO `yzm_config` VALUES (77, 'tit_video', 99, '首页介绍视频', '/uploads/202303/11/230311033512943.mp4', 'attachment', '', 1);

-- ----------------------------
-- Table structure for yzm_download
-- ----------------------------
DROP TABLE IF EXISTS `yzm_download`;
CREATE TABLE `yzm_download`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `nickname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `title` varchar(180) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `color` char(9) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `keywords` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `click` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `copyfrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `thumb` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `flag` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '1置顶,2头条,3特荐,4推荐,5热点,6幻灯,7跳转',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `issystem` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `listorder` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `groupids_view` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '阅读权限',
  `readpoint` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '阅读收费',
  `paytype` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '收费类型',
  `is_push` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否百度推送',
  `down_url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '下载地址',
  `copytype` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '授权形式',
  `systems` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '平台',
  `language` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '语言',
  `version` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '版本',
  `filesize` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '文件大小',
  `classtype` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '软件类型',
  `stars` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '评分等级',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `status`(`status`, `listorder`) USING BTREE,
  INDEX `catid`(`catid`, `status`) USING BTREE,
  INDEX `userid`(`userid`, `status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_download
-- ----------------------------
INSERT INTO `yzm_download` VALUES (1, 49, 1, 'yzmcms', '管理员', '测试下载文件1', '', 1678709019, 1678709133, '测试,下载,文件', '测试下载文件1', 145, '<p>测试下载文件1</p>', '网络', '', 'http://test123.com/xiazaizhongxin/1.html', '', 1, 1, 10, 0, 0, 1, 0, '/uploads/202303/13/230313080405383.mp4', '免费版', 'Windows', '简体中文', '1.0', '', '', '');
INSERT INTO `yzm_download` VALUES (2, 49, 1, 'yzmcms', '管理员', '测试下载文件2', '', 1678709300, 1678709331, '测试,下载,文件', '测试下载文件2', 63, '<p>测试下载文件2</p>', '网络', '', 'http://test123.com/xiazaizhongxin/2.html', '', 1, 1, 10, 0, 0, 1, 0, '/uploads/202303/13/230313080837798.docx', '免费版', 'Windows', '简体中文', '1.0', '', '', '');

-- ----------------------------
-- Table structure for yzm_favorite
-- ----------------------------
DROP TABLE IF EXISTS `yzm_favorite`;
CREATE TABLE `yzm_favorite`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `title` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `userid`(`userid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of yzm_favorite
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_guestbook
-- ----------------------------
DROP TABLE IF EXISTS `yzm_guestbook`;
CREATE TABLE `yzm_guestbook`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题',
  `booktime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '添加时间',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '名字',
  `email` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '留言人电子邮箱',
  `phone` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '留言人电话',
  `qq` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '留言人qq',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '留言人地址',
  `bookmsg` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'ip地址',
  `ischeck` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否审核',
  `isread` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否读过',
  `ispc` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1电脑,0手机',
  `replyid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '回复的id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index_booktime`(`booktime`) USING BTREE,
  INDEX `index_replyid`(`replyid`) USING BTREE,
  INDEX `index_ischeck`(`siteid`, `ischeck`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_guestbook
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_keyword_link
-- ----------------------------
DROP TABLE IF EXISTS `yzm_keyword_link`;
CREATE TABLE `yzm_keyword_link`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `keyword` varchar(36) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '关键字',
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '地址',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_keyword_link
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_link
-- ----------------------------
DROP TABLE IF EXISTS `yzm_link`;
CREATE TABLE `yzm_link`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `typeid` smallint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1首页,2列表页,3内容页',
  `linktype` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0:文字链接,1:logo链接',
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `logo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `msg` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `email` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `listorder` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0未通过,1正常,2未审核',
  `addtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `siteid`(`siteid`, `status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_link
-- ----------------------------
INSERT INTO `yzm_link` VALUES (1, 0, 0, 0, 'YzmCMS官方网站', 'http://www.yzmcms.com/', '', '', '袁志蒙', '', 1, 1, 1483095485);
INSERT INTO `yzm_link` VALUES (2, 0, 0, 0, 'YzmCMS交流社区', 'http://www.yzmask.com/', '', '', '袁志蒙', '', 2, 1, 1483095496);
INSERT INTO `yzm_link` VALUES (3, 0, 0, 0, 'YzmCMS官方博客', 'http://blog.yzmcms.com/', '', '', '袁志蒙', '', 3, 1, 1483095596);

-- ----------------------------
-- Table structure for yzm_member
-- ----------------------------
DROP TABLE IF EXISTS `yzm_member`;
CREATE TABLE `yzm_member`  (
  `userid` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `password` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `regdate` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `lastdate` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `regip` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `lastip` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `loginnum` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `email` char(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `groupid` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `amount` decimal(8, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '金钱',
  `experience` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '经验',
  `point` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '积分',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0待审核,1正常,2锁定,3拒绝',
  `vip` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `overduedate` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `email_status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `problem` varchar(39) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '安全问题',
  `answer` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '答案',
  PRIMARY KEY (`userid`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  INDEX `email`(`email`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_member
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_member_authorization
-- ----------------------------
DROP TABLE IF EXISTS `yzm_member_authorization`;
CREATE TABLE `yzm_member_authorization`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `authname` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `token` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `userinfo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `authindex`(`authname`, `token`) USING BTREE,
  INDEX `userid`(`userid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_member_authorization
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_member_detail
-- ----------------------------
DROP TABLE IF EXISTS `yzm_member_detail`;
CREATE TABLE `yzm_member_detail`  (
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `sex` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `realname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '真实姓名',
  `nickname` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `qq` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `mobile` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `phone` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `userpic` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `birthday` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '生日',
  `industry` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '行业',
  `area` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `motto` varchar(210) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '个性签名',
  `introduce` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '个人简介',
  `guest` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `fans` mediumint(8) UNSIGNED NOT NULL DEFAULT 0 COMMENT '粉丝数',
  UNIQUE INDEX `userid`(`userid`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_member_detail
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_member_follow
-- ----------------------------
DROP TABLE IF EXISTS `yzm_member_follow`;
CREATE TABLE `yzm_member_follow`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户id',
  `followid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0 COMMENT '被关注者id',
  `followname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '被关注者用户名',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `userid`(`userid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_member_follow
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_member_group
-- ----------------------------
DROP TABLE IF EXISTS `yzm_member_group`;
CREATE TABLE `yzm_member_group`  (
  `groupid` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(21) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `experience` smallint(6) UNSIGNED NOT NULL DEFAULT 0,
  `icon` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图标',
  `authority` char(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '1短消息,2发表评论,3发表内容',
  `max_amount` tinyint(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '每日最大投稿量',
  `description` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `is_system` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '系统内置',
  PRIMARY KEY (`groupid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_member_group
-- ----------------------------
INSERT INTO `yzm_member_group` VALUES (1, '初来乍到', 50, 'icon1.png', '1,2', 1, '初来乍到组', 1);
INSERT INTO `yzm_member_group` VALUES (2, '新手上路', 100, 'icon2.png', '1,2', 2, '新手上路组', 1);
INSERT INTO `yzm_member_group` VALUES (3, '中级会员', 200, 'icon3.png', '1,2,3', 3, '中级会员组', 1);
INSERT INTO `yzm_member_group` VALUES (4, '高级会员', 300, 'icon4.png', '1,2,3', 4, '高级会员组', 1);
INSERT INTO `yzm_member_group` VALUES (5, '金牌会员', 500, 'icon5.png', '1,2,3,4', 5, '金牌会员组', 1);

-- ----------------------------
-- Table structure for yzm_member_guest
-- ----------------------------
DROP TABLE IF EXISTS `yzm_member_guest`;
CREATE TABLE `yzm_member_guest`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `space_id` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `guest_id` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `guest_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `guest_pic` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `ip` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `space_id`(`space_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_member_guest
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_menu
-- ----------------------------
DROP TABLE IF EXISTS `yzm_menu`;
CREATE TABLE `yzm_menu`  (
  `id` smallint(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` char(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `parentid` smallint(6) NOT NULL DEFAULT 0,
  `m` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `c` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `a` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `data` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `listorder` smallint(6) UNSIGNED NOT NULL DEFAULT 0,
  `display` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `listorder`(`listorder`) USING BTREE,
  INDEX `parentid`(`parentid`) USING BTREE,
  INDEX `module`(`m`, `c`, `a`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 318 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of yzm_menu
-- ----------------------------
INSERT INTO `yzm_menu` VALUES (1, '内容管理', 0, 'admin', 'content', 'top', 'yzm-iconneirong', 1, 1);
INSERT INTO `yzm_menu` VALUES (2, '会员管理', 0, 'member', 'member', 'top', 'yzm-iconyonghu', 2, 1);
INSERT INTO `yzm_menu` VALUES (3, '模块管理', 0, 'admin', 'module', 'top', 'yzm-icondaohang', 3, 1);
INSERT INTO `yzm_menu` VALUES (4, '管理员管理', 0, 'admin', 'admin_manage', 'top', 'yzm-iconguanliyuan', 4, 1);
INSERT INTO `yzm_menu` VALUES (5, '个人信息', 0, 'admin', 'admin_manage', 'top', 'yzm-iconrizhi', 5, 0);
INSERT INTO `yzm_menu` VALUES (6, '系统管理', 0, 'admin', 'system_manage', 'top', 'yzm-iconshezhi', 6, 1);
INSERT INTO `yzm_menu` VALUES (7, '数据管理', 0, 'admin', 'database', 'top', 'yzm-iconshujuku', 7, 1);
INSERT INTO `yzm_menu` VALUES (8, '稿件管理', 1, 'admin', 'admin_content', 'init', '', 13, 1);
INSERT INTO `yzm_menu` VALUES (9, '稿件浏览', 8, 'admin', 'admin_content', 'public_preview', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (10, '稿件删除', 8, 'admin', 'admin_content', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (11, '通过审核', 8, 'admin', 'admin_content', 'adopt', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (12, '退稿', 8, 'admin', 'admin_content', 'rejection', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (13, '后台操作日志', 6, 'admin', 'admin_log', 'init', '', 66, 1);
INSERT INTO `yzm_menu` VALUES (14, '操作日志删除', 13, 'admin', 'admin_log', 'del_log', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (15, '操作日志搜索', 13, 'admin', 'admin_log', 'search_log', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (16, '后台登录日志', 6, 'admin', 'admin_log', 'admin_login_log_list', '', 67, 1);
INSERT INTO `yzm_menu` VALUES (17, '登录日志删除', 16, 'admin', 'admin_log', 'del_login_log', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (18, '管理员管理', 4, 'admin', 'admin_manage', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (19, '删除管理员', 18, 'admin', 'admin_manage', 'delete', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (20, '添加管理员', 18, 'admin', 'admin_manage', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (21, '编辑管理员', 18, 'admin', 'admin_manage', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (22, '修改资料', 18, 'admin', 'admin_manage', 'public_edit_info', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (23, '修改密码', 18, 'admin', 'admin_manage', 'public_edit_pwd', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (24, '栏目管理', 1, 'admin', 'category', 'init', '', 11, 1);
INSERT INTO `yzm_menu` VALUES (25, '排序栏目', 24, 'admin', 'category', 'order', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (26, '删除栏目', 24, 'admin', 'category', 'delete', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (27, '添加栏目', 24, 'admin', 'category', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (28, '编辑栏目', 24, 'admin', 'category', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (29, '编辑单页内容', 24, 'admin', 'category', 'page_content', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (30, '内容管理', 1, 'admin', 'content', 'init', '', 10, 1);
INSERT INTO `yzm_menu` VALUES (31, '内容搜索', 30, 'admin', 'content', 'search', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (32, '添加内容', 30, 'admin', 'content', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (33, '修改内容', 30, 'admin', 'content', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (34, '删除内容', 30, 'admin', 'content', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (35, '数据备份', 7, 'admin', 'database', 'init', '', 70, 1);
INSERT INTO `yzm_menu` VALUES (36, '数据还原', 7, 'admin', 'database', 'databack_list', '', 71, 1);
INSERT INTO `yzm_menu` VALUES (37, '优化表', 35, 'admin', 'database', 'public_optimize', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (38, '修复表', 35, 'admin', 'database', 'public_repair', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (39, '备份文件删除', 36, 'admin', 'database', 'databack_del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (40, '备份文件下载', 36, 'admin', 'database', 'databack_down', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (41, '数据导入', 36, 'admin', 'database', 'import', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (42, '字段管理', 54, 'admin', 'model_field', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (43, '添加字段', 42, 'admin', 'model_field', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (44, '修改字段', 42, 'admin', 'model_field', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (45, '删除字段', 42, 'admin', 'model_field', 'delete', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (46, '排序字段', 42, 'admin', 'model_field', 'order', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (47, '模块管理', 3, 'admin', 'module', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (48, '模块安装', 47, 'admin', 'module', 'install', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (49, '模块卸载', 47, 'admin', 'module', 'uninstall', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (50, '角色管理', 4, 'admin', 'role', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (51, '删除角色', 50, 'admin', 'role', 'delete', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (52, '添加角色', 50, 'admin', 'role', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (53, '编辑角色', 50, 'admin', 'role', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (54, '模型管理', 1, 'admin', 'sitemodel', 'init', '', 15, 1);
INSERT INTO `yzm_menu` VALUES (55, '删除模型', 54, 'admin', 'sitemodel', 'delete', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (56, '添加模型', 54, 'admin', 'sitemodel', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (57, '编辑模型', 54, 'admin', 'sitemodel', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (58, '系统设置', 6, 'admin', 'system_manage', 'init', '', 60, 1);
INSERT INTO `yzm_menu` VALUES (59, '会员中心设置', 2, 'admin', 'system_manage', 'member_set', '', 26, 1);
INSERT INTO `yzm_menu` VALUES (60, '屏蔽词管理', 6, 'admin', 'system_manage', 'prohibit_words', '', 63, 1);
INSERT INTO `yzm_menu` VALUES (61, '自定义配置', 6, 'admin', 'system_manage', 'user_config_list', '', 62, 1);
INSERT INTO `yzm_menu` VALUES (62, '添加配置', 61, 'admin', 'system_manage', 'user_config_add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (63, '配置编辑', 61, 'admin', 'system_manage', 'user_config_edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (64, '配置删除', 61, 'admin', 'system_manage', 'user_config_del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (65, 'TAG管理', 1, 'admin', 'tag', 'init', '', 16, 1);
INSERT INTO `yzm_menu` VALUES (66, '添加TAG', 65, 'admin', 'tag', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (67, '编辑TAG', 65, 'admin', 'tag', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (68, '删除TAG', 65, 'admin', 'tag', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (69, '批量更新URL', 1, 'admin', 'update_urls', 'init', '', 17, 1);
INSERT INTO `yzm_menu` VALUES (70, '附件管理', 1, 'attachment', 'index', 'init', '', 14, 1);
INSERT INTO `yzm_menu` VALUES (71, '附件搜索', 70, 'attachment', 'index', 'search_list', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (72, '附件浏览', 70, 'attachment', 'index', 'public_att_view', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (73, '删除单个附件', 70, 'attachment', 'index', 'del_one', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (74, '删除多个附件', 70, 'attachment', 'index', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (75, '评论管理', 1, 'comment', 'comment', 'init', '', 12, 1);
INSERT INTO `yzm_menu` VALUES (76, '评论搜索', 75, 'comment', 'comment', 'search', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (77, '删除评论', 75, 'comment', 'comment', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (78, '评论审核', 75, 'comment', 'comment', 'adopt', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (79, '留言管理', 3, 'guestbook', 'guestbook', 'init', '', 1, 1);
INSERT INTO `yzm_menu` VALUES (80, '查看及回复留言', 79, 'guestbook', 'guestbook', 'read', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (81, '留言审核', 79, 'guestbook', 'guestbook', 'toggle', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (82, '删除留言', 79, 'guestbook', 'guestbook', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (88, '会员管理', 2, 'member', 'member', 'init', '', 20, 1);
INSERT INTO `yzm_menu` VALUES (89, '会员搜索', 88, 'member', 'member', 'search', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (90, '添加会员', 88, 'member', 'member', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (91, '修改会员信息', 88, 'member', 'member', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (92, '修改会员密码', 88, 'member', 'member', 'password', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (93, '删除会员', 88, 'member', 'member', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (94, '审核会员', 2, 'member', 'member', 'check', '', 21, 1);
INSERT INTO `yzm_menu` VALUES (95, '通过审核', 94, 'member', 'member', 'adopt', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (96, '锁定用户', 88, 'member', 'member', 'lock', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (97, '解锁用户', 88, 'member', 'member', 'unlock', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (98, '账单管理', 2, 'member', 'member', 'pay', '', 22, 1);
INSERT INTO `yzm_menu` VALUES (99, '入账记录搜索', 98, 'member', 'member', 'pay_search', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (100, '入账记录删除', 98, 'member', 'member', 'pay_del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (101, '消费记录', 98, 'member', 'member', 'pay_spend', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (102, '消费记录搜索', 98, 'member', 'member', 'pay_spend_search', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (103, '消费记录删除', 98, 'member', 'member', 'pay_spend_del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (104, '会员组管理', 2, 'member', 'member_group', 'init', '', 25, 1);
INSERT INTO `yzm_menu` VALUES (105, '添加组别', 104, 'member', 'member_group', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (106, '修改组别', 104, 'member', 'member_group', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (107, '删除组别', 104, 'member', 'member_group', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (108, '消息管理', 2, 'member', 'member_message', 'init', '', 23, 1);
INSERT INTO `yzm_menu` VALUES (109, '消息搜索', 108, 'member', 'member_message', 'search', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (110, '删除消息', 108, 'member', 'member_message', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (111, '发送单个消息', 108, 'member', 'member_message', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (112, '群发消息', 2, 'member', 'member_message', 'messages_list', '', 23, 1);
INSERT INTO `yzm_menu` VALUES (113, '新建群发', 112, 'member', 'member_message', 'add_messages', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (114, '删除群发消息', 112, 'member', 'member_message', 'del_messages', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (115, '权限管理', 50, 'admin', 'role', 'role_priv', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (116, '后台菜单管理', 6, 'admin', 'menu', 'init', '', 64, 1);
INSERT INTO `yzm_menu` VALUES (117, '删除菜单', 116, 'admin', 'menu', 'delete', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (118, '添加菜单', 116, 'admin', 'menu', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (119, '编辑菜单', 116, 'admin', 'menu', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (120, '菜单排序', 116, 'admin', 'menu', 'order', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (121, '邮箱配置', 6, 'admin', 'system_manage', 'init', 'tab=4', 61, 1);
INSERT INTO `yzm_menu` VALUES (122, '修改资料', 5, 'admin', 'admin_manage', 'public_edit_info', '', 51, 1);
INSERT INTO `yzm_menu` VALUES (123, '修改密码', 5, 'admin', 'admin_manage', 'public_edit_pwd', '', 52, 1);
INSERT INTO `yzm_menu` VALUES (134, '友情链接管理', 3, 'link', 'link', 'init', '', 6, 1);
INSERT INTO `yzm_menu` VALUES (135, '添加友情链接', 134, 'link', 'link', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (136, '修改友情链接', 134, 'link', 'link', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (137, '删除单个友情链接', 134, 'link', 'link', 'del_one', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (138, '删除多个友情链接', 134, 'link', 'link', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (139, 'URL规则管理', 6, 'admin', 'urlrule', 'init', '', 65, 1);
INSERT INTO `yzm_menu` VALUES (140, '添加URL规则', 139, 'admin', 'urlrule', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (141, '删除URL规则', 139, 'admin', 'urlrule', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (142, '编辑URL规则', 139, 'admin', 'urlrule', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (143, '批量移动', 30, 'admin', 'content', 'remove', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (144, 'SQL命令行', 6, 'admin', 'sql', 'init', '', 63, 1);
INSERT INTO `yzm_menu` VALUES (145, '提交SQL命令', 144, 'admin', 'sql', 'do_sql', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (156, '轮播图管理', 3, 'banner', 'banner', 'init', '', 1, 1);
INSERT INTO `yzm_menu` VALUES (157, '添加轮播', 156, 'banner', 'banner', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (158, '修改轮播', 156, 'banner', 'banner', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (159, '删除轮播', 156, 'banner', 'banner', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (160, '添加轮播分类', 156, 'banner', 'banner', 'cat_add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (161, '管理轮播分类', 156, 'banner', 'banner', 'cat_manage', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (162, '会员统计', 2, 'member', 'member', 'member_count', '', 24, 1);
INSERT INTO `yzm_menu` VALUES (165, '采集管理', 3, 'collection', 'collection_content', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (166, '添加采集节点', 165, 'collection', 'collection_content', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (167, '编辑采集节点', 165, 'collection', 'collection_content', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (168, '删除采集节点', 165, 'collection', 'collection_content', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (169, '采集测试', 165, 'collection', 'collection_content', 'collection_test', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (170, '采集网址', 165, 'collection', 'collection_content', 'collection_list_url', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (171, '采集内容', 165, 'collection', 'collection_content', 'collection_article_content', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (172, '内容导入', 165, 'collection', 'collection_content', 'collection_content_import', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (173, '新建内容发布方案', 165, 'collection', 'collection_content', 'create_programme', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (174, '采集列表', 165, 'collection', 'collection_content', 'collection_list', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (175, '删除采集列表', 165, 'collection', 'collection_content', 'collection_list_del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (200, '微信管理', 0, 'wechat', 'wechat', 'top', 'yzm-iconweixin', 3, 1);
INSERT INTO `yzm_menu` VALUES (201, '微信配置', 200, 'wechat', 'config', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (202, '保存配置', 201, 'wechat', 'config', 'save', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (203, '微信用户', 200, 'wechat', 'user', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (204, '关注者搜索', 203, 'wechat', 'user', 'search', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (205, '获取分组名称', 203, 'wechat', 'user', 'get_groupname', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (206, '同步微信服务器用户', 203, 'wechat', 'user', 'synchronization', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (207, '批量移动用户分组', 203, 'wechat', 'user', 'move_user_group', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (208, '设置用户备注', 203, 'wechat', 'user', 'set_userremark', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (209, '查询用户所在组', 203, 'wechat', 'user', 'select_user_group', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (210, '分组管理', 200, 'wechat', 'group', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (211, '创建分组', 210, 'wechat', 'group', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (212, '修改分组', 210, 'wechat', 'group', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (213, '删除分组', 210, 'wechat', 'group', 'delete', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (214, '查询所有分组', 210, 'wechat', 'group', 'select_group', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (215, '微信菜单', 200, 'wechat', 'menu', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (216, '添加菜单', 215, 'wechat', 'menu', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (217, '编辑菜单', 215, 'wechat', 'menu', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (218, '删除菜单', 215, 'wechat', 'menu', 'delete', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (219, '菜单排序', 215, 'wechat', 'menu', 'order', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (220, '创建菜单提交微信', 215, 'wechat', 'menu', 'create_menu', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (221, '查询远程菜单', 215, 'wechat', 'menu', 'select_menu', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (222, '删除所有菜单提交微信', 215, 'wechat', 'menu', 'delete_menu', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (223, '消息回复', 200, 'wechat', 'reply', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (224, '自动回复/关注回复', 223, 'wechat', 'reply', 'reply_list', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (225, '添加关键字回复', 223, 'wechat', 'reply', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (226, '修改关键字回复', 223, 'wechat', 'reply', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (227, '删除关键字回复', 223, 'wechat', 'reply', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (228, '选择文章', 223, 'wechat', 'reply', 'select_article', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (229, '消息管理', 200, 'wechat', 'message', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (230, '用户发送信息', 229, 'wechat', 'message', 'send_message', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (231, '标识已读', 229, 'wechat', 'message', 'read', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (232, '删除消息', 229, 'wechat', 'message', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (233, '微信场景', 200, 'wechat', 'scan', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (234, '添加场景', 233, 'wechat', 'scan', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (235, '编辑场景', 233, 'wechat', 'scan', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (236, '删除场景', 233, 'wechat', 'scan', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (237, '素材管理', 200, 'wechat', 'material', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (238, '素材搜索', 237, 'wechat', 'material', 'search', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (239, '添加素材', 237, 'wechat', 'material', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (240, '添加图文素材', 237, 'wechat', 'material', 'add_news', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (241, '删除素材', 237, 'wechat', 'material', 'delete', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (242, '选择缩略图', 237, 'wechat', 'material', 'select_thumb', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (243, '获取永久素材列表', 237, 'wechat', 'material', 'get_material_list', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (244, '高级群发', 200, 'wechat', 'mass', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (245, '新建群发', 244, 'wechat', 'mass', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (246, '查询群发状态', 244, 'wechat', 'mass', 'select_status', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (247, '删除群发', 244, 'wechat', 'mass', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (248, '选择素材', 244, 'wechat', 'mass', 'select_material', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (249, '选择用户', 244, 'wechat', 'mass', 'select_user', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (250, '自定义表单', 3, 'diyform', 'diyform', 'init', '', 2, 1);
INSERT INTO `yzm_menu` VALUES (251, '添加表单', 250, 'diyform', 'diyform', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (252, '编辑表单', 250, 'diyform', 'diyform', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (253, '删除表单', 250, 'diyform', 'diyform', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (254, '字段列表', 250, 'diyform', 'diyform_field', 'init', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (255, '添加字段', 254, 'diyform', 'diyform_field', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (256, '修改字段', 254, 'diyform', 'diyform_field', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (257, '删除字段', 254, 'diyform', 'diyform_field', 'delete', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (258, '排序排序', 254, 'diyform', 'diyform_field', 'order', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (259, '表单信息列表', 250, 'diyform', 'diyform_info', 'init', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (260, '查看表单信息', 259, 'diyform', 'diyform_info', 'view', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (261, '删除表单信息', 259, 'diyform', 'diyform_info', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (262, '广告管理', 3, 'adver', 'adver', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (263, '添加广告', 262, 'adver', 'adver', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (264, '修改广告', 262, 'adver', 'adver', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (265, '删除广告', 262, 'adver', 'adver', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (266, '网站地图', 1, 'admin', 'sitemap', 'init', '', 16, 1);
INSERT INTO `yzm_menu` VALUES (267, '生成地图', 266, 'admin', 'sitemap', 'make_sitemap', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (268, '导出模型', 54, 'admin', 'sitemodel', 'import', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (269, '导入模型', 54, 'admin', 'sitemodel', 'export', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (270, '导出配置', 61, 'admin', 'system_manage', 'user_config_export', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (271, '导入配置', 61, 'admin', 'system_manage', 'user_config_import', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (283, '支付模块', 3, 'pay', 'pay', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (284, '支付配置', 283, 'pay', 'pay', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (285, '订单管理', 2, 'member', 'order', 'init', '', 22, 1);
INSERT INTO `yzm_menu` VALUES (286, '订单搜索', 285, 'member', 'order', 'order_search', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (287, '订单改价', 285, 'member', 'order', 'change_price', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (288, '订单删除', 285, 'member', 'order', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (289, '订单详情', 285, 'member', 'order', 'order_details', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (290, '推送至百度', 30, 'admin', 'content', 'baidu_push', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (291, '增加/删除内容属性', 30, 'admin', 'content', 'attribute_operation', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (292, '更改model', 69, 'admin', 'update_urls', 'change_model', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (293, '更新栏目URL', 69, 'admin', 'update_urls', 'update_category_url', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (294, '更新内容页URL', 69, 'admin', 'update_urls', 'update_content_url', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (295, '留言搜索', 79, 'guestbook', 'guestbook', 'search', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (296, '内容关键字', 3, 'admin', 'keyword_link', 'init', '', 1, 1);
INSERT INTO `yzm_menu` VALUES (297, '添加关键字', 296, 'admin', 'keyword_link', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (298, '编辑关键字', 296, 'admin', 'keyword_link', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (299, '删除关键字', 296, 'admin', 'keyword_link', 'del', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (300, '应用商店', 3, 'admin', 'store', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (301, '批量添加栏目', 24, 'admin', 'category', 'adds', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (302, '内容复制', 30, 'admin', 'content', 'copy', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (303, '关联内容', 65, 'admin', 'tag', 'content', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (304, '加入/移除Tag', 65, 'admin', 'tag', 'content_oper', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (305, '删除地图', 266, 'admin', 'sitemap', 'delete', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (306, '保存配置', 58, 'admin', 'system_manage', 'save', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (307, '立即备份', 35, 'admin', 'database', 'export_list', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (308, '管理非自己发布的内容', 30, 'admin', 'content', 'all_content', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (309, '在线充值', 88, 'member', 'member', 'recharge', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (310, '登录到任意会员中心', 88, 'member', 'member', 'login_user', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (311, '友情链接排序', 134, 'link', 'link', 'order', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (312, '友情链接审核', 134, 'link', 'link', 'adopt', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (313, '标识已读', 79, 'guestbook', 'guestbook', 'set_read', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (314, '门店管理', 3, 'shop', 'shop', 'init', '', 0, 1);
INSERT INTO `yzm_menu` VALUES (315, '添加门店', 314, 'shop', 'shop', 'add', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (316, '修改门店', 314, 'shop', 'shop', 'edit', '', 0, 0);
INSERT INTO `yzm_menu` VALUES (317, '删除门店', 314, 'shop', 'shop', 'del', '', 0, 0);

-- ----------------------------
-- Table structure for yzm_message
-- ----------------------------
DROP TABLE IF EXISTS `yzm_message`;
CREATE TABLE `yzm_message`  (
  `messageid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `send_from` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '发件人',
  `send_to` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '收件人',
  `message_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `subject` char(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `replyid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '回复的id',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '1正常0隐藏',
  `isread` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否读过',
  `issystem` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '系统信息',
  PRIMARY KEY (`messageid`) USING BTREE,
  INDEX `replyid`(`replyid`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_message
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_message_data
-- ----------------------------
DROP TABLE IF EXISTS `yzm_message_data`;
CREATE TABLE `yzm_message_data`  (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `group_message_id` int(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '读过的信息ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `message`(`userid`, `group_message_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of yzm_message_data
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_message_group
-- ----------------------------
DROP TABLE IF EXISTS `yzm_message_group`;
CREATE TABLE `yzm_message_group`  (
  `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `groupid` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户组id',
  `subject` char(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_message_group
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_model
-- ----------------------------
DROP TABLE IF EXISTS `yzm_model`;
CREATE TABLE `yzm_model`  (
  `modelid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `name` char(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `tablename` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `alias` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `setting` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `items` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `disabled` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `sort` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `issystem` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `isdefault` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`modelid`) USING BTREE,
  INDEX `siteid`(`siteid`, `type`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_model
-- ----------------------------
INSERT INTO `yzm_model` VALUES (1, 0, '文章模型', 'article', 'article', '文章模型', '', 1466393786, 0, 0, 0, 0, 1, 1);
INSERT INTO `yzm_model` VALUES (2, 0, '产品模型', 'product', 'product', '产品模型', '', 1466393786, 0, 0, 0, 0, 1, 0);
INSERT INTO `yzm_model` VALUES (3, 0, '下载模型', 'download', 'download', '下载模型', '', 1466393786, 0, 0, 0, 0, 1, 0);
INSERT INTO `yzm_model` VALUES (4, 0, '我们的冠牛', 'women', 'women', '', NULL, 1677839331, 0, 0, 0, 0, 0, 0);

-- ----------------------------
-- Table structure for yzm_model_field
-- ----------------------------
DROP TABLE IF EXISTS `yzm_model_field`;
CREATE TABLE `yzm_model_field`  (
  `fieldid` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `modelid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `field` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `tips` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `css` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `minlength` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `maxlength` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `errortips` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `fieldtype` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `defaultvalue` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `setting` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `isrequired` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `issystem` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `isunique` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `isadd` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `listorder` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `disabled` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  PRIMARY KEY (`fieldid`) USING BTREE,
  INDEX `modelid`(`modelid`, `disabled`) USING BTREE,
  INDEX `field`(`field`, `modelid`) USING BTREE,
  INDEX `status`(`status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_model_field
-- ----------------------------
INSERT INTO `yzm_model_field` VALUES (1, 0, 'title', '标题', '', '', 1, 100, '请输入标题', 'input', '', '', 1, 1, 0, 1, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (2, 0, 'catid', '栏目', '', '', 1, 10, '请选择栏目', 'select', '', '', 1, 1, 0, 1, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (3, 0, 'thumb', '缩略图', '', '', 0, 100, '', 'image', '', '', 0, 1, 0, 1, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (4, 0, 'keywords', '关键词', '', '', 0, 50, '', 'input', '', '', 0, 1, 0, 1, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (5, 0, 'description', '摘要', '', '', 0, 255, '', 'textarea', '', '', 0, 1, 0, 1, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (6, 0, 'inputtime', '发布时间', '', '', 1, 10, '', 'datetime', '', '', 1, 1, 0, 0, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (7, 0, 'updatetime', '更新时间', '', '', 1, 10, '', 'datetime', '', '', 1, 1, 0, 0, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (8, 0, 'copyfrom', '来源', '', '', 0, 30, '', 'input', '', '', 0, 1, 0, 1, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (9, 0, 'url', 'URL', '', '', 1, 100, '', 'input', '', '', 1, 1, 0, 0, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (10, 0, 'userid', '用户ID', '', '', 1, 10, '', 'input', '', '', 1, 1, 0, 0, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (11, 0, 'username', '用户名', '', '', 1, 30, '', 'input', '', '', 1, 1, 0, 0, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (12, 0, 'nickname', '昵称', '', '', 0, 30, '', 'input', '', '', 0, 1, 0, 0, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (13, 0, 'template', '模板', '', '', 1, 50, '', 'select', '', '', 1, 1, 0, 0, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (14, 0, 'content', '内容', '', '', 1, 999999, '', 'editor', '', '', 1, 1, 0, 1, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (15, 0, 'click', '点击数', '', '', 1, 10, '', 'input', '0', '', 0, 1, 0, 0, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (16, 0, 'tag', 'TAG', '', '', 0, 50, '', 'checkbox', '', '', 0, 1, 0, 0, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (17, 0, 'readpoint', '阅读收费', '', '', 1, 5, '', 'input', '0', '', 0, 1, 0, 0, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (18, 0, 'groupids_view', '阅读权限', '', '', 1, 10, '', 'checkbox', '1', '', 0, 1, 0, 0, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (19, 0, 'status', '状态', '', '', 1, 2, '', 'checkbox', '', '', 1, 1, 0, 0, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (20, 0, 'flag', '属性', '', '', 1, 16, '', 'checkbox', '', '', 1, 1, 0, 0, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (21, 0, 'listorder', '排序', '', '', 1, 5, '', 'input', '1', '', 1, 1, 0, 0, 0, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (22, 2, 'brand', '品牌', '', '', 0, 30, '', 'input', '', '', 0, 0, 0, 1, 1, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (23, 2, 'standard', '型号', '', '', 0, 30, '', 'input', '', '', 0, 0, 0, 1, 1, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (24, 2, 'yieldly', '产地', '', '', 0, 50, '', 'input', '', '', 0, 0, 0, 1, 1, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (25, 2, 'pictures', '产品图集', '', '', 0, 1000, '', 'images', '', '', 0, 0, 0, 1, 1, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (26, 2, 'price', '单价', '请输入单价', '', 1, 10, '单价不能为空', 'decimal', '', '', 1, 0, 0, 1, 1, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (27, 2, 'unit', '价格单位', '', '', 1, 10, '', 'select', '', '{\"0\":\"件\",\"1\":\"斤\",\"2\":\"KG\",\"3\":\"吨\",\"4\":\"套\"}', 1, 0, 0, 1, 1, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (28, 2, 'stock', '库存', '库存量必须为数字', '', 1, 5, '库存不能为空', 'number', '99999', '', 1, 0, 0, 1, 1, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (29, 3, 'down_url', '下载地址', '', '', 1, 100, '下载地址不能为空', 'attachment', '', '', 1, 0, 0, 1, 1, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (30, 3, 'copytype', '授权形式', '', '', 0, 20, '', 'select', '', '{\"0\":\"免费版\",\"1\":\"正式版\",\"2\":\"共享版\",\"3\":\"试用版\",\"4\":\"演示版\",\"5\":\"注册版\",\"6\":\"破解版\"}', 0, 0, 0, 1, 1, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (31, 3, 'systems', '平台', '', '', 1, 30, '', 'select', '', '{\"0\":\"Windows\",\"1\":\"Linux\",\"2\":\"MacOS\"}', 1, 0, 0, 1, 1, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (32, 3, 'language', '语言', '', '', 0, 20, '', 'select', '', '{\"0\":\"简体中文\",\"1\":\"繁体中文\",\"2\":\"英文\",\"3\":\"多国语言\",\"4\":\"其他语言\"}', 0, 0, 0, 1, 1, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (33, 3, 'version', '版本', '', '', 1, 15, '版本号不能为空', 'input', '', '', 1, 0, 0, 1, 1, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (34, 3, 'filesize', '文件大小', '只输入数字即可，单位是字节', '', 0, 10, '', 'input', '', '', 0, 0, 0, 1, 1, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (35, 3, 'classtype', '软件类型', '', '', 1, 30, '', 'radio', '', '{\"0\":\"国产软件\",\"1\":\"国外软件\",\"2\":\"汉化补丁\",\"3\":\"程序源码\",\"4\":\"其他\"}', 1, 0, 0, 1, 1, 0, 0, 1);
INSERT INTO `yzm_model_field` VALUES (36, 3, 'stars', '评分等级', '', '', 0, 20, '', 'radio', '', '{\"0\":\"1:1星\",\"1\":\"2:2星\",\"2\":\"3:3星\",\"3\":\"4:4星\",\"4\":\"5:5星\"}', 0, 0, 0, 1, 1, 0, 0, 1);

-- ----------------------------
-- Table structure for yzm_module
-- ----------------------------
DROP TABLE IF EXISTS `yzm_module`;
CREATE TABLE `yzm_module`  (
  `module` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `iscore` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `version` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `setting` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `listorder` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `disabled` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `installdate` date NOT NULL DEFAULT '2016-01-01',
  `updatedate` date NOT NULL DEFAULT '2016-01-01',
  PRIMARY KEY (`module`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_module
-- ----------------------------
INSERT INTO `yzm_module` VALUES ('admin', '后台模块', 1, '1.0', '后台模块', '', 0, 0, '2016-08-27', '2016-08-27');
INSERT INTO `yzm_module` VALUES ('index', '前台模块', 1, '1.0', '前台模块', '', 0, 0, '2016-09-21', '2016-09-21');
INSERT INTO `yzm_module` VALUES ('api', '接口模块', 1, '1.0', '为整个系统提供接口', '', 0, 0, '2016-08-28', '2016-08-28');
INSERT INTO `yzm_module` VALUES ('install', '安装模块', 1, '1.0', 'CMS安装模块', '', 0, 0, '2016-10-28', '2016-10-28');
INSERT INTO `yzm_module` VALUES ('attachment', '附件模块', 1, '1.0', '附件模块', '', 0, 0, '2016-10-10', '2016-10-10');
INSERT INTO `yzm_module` VALUES ('member', '会员模块', 1, '1.0', '会员模块', '', 0, 0, '2016-09-21', '2016-09-21');
INSERT INTO `yzm_module` VALUES ('guestbook', '留言模块', 1, '1.0', '留言板模块', '', 0, 0, '2016-10-25', '2016-10-25');
INSERT INTO `yzm_module` VALUES ('search', '搜索模块', 1, '1.0', '搜索模块', '', 0, 0, '2016-11-21', '2016-11-21');
INSERT INTO `yzm_module` VALUES ('link', '友情链接', 0, '1.0', '友情链接模块', '', 0, 0, '2016-12-11', '2016-09-28');
INSERT INTO `yzm_module` VALUES ('comment', '评论模块', 1, '1.0', '全站评论', '', 0, 0, '2017-01-05', '2017-01-05');
INSERT INTO `yzm_module` VALUES ('mobile', '手机模块', 1, '1.0', '手机模块', '', 0, 0, '2017-04-05', '2017-04-05');
INSERT INTO `yzm_module` VALUES ('banner', '轮播图管理', 0, '2.0', '轮播图管理模块', '', 0, 0, '2017-05-12', '2020-05-17');
INSERT INTO `yzm_module` VALUES ('collection', '采集模块', 1, '1.0', '采集模块', '', 0, 0, '2017-08-16', '2017-08-16');
INSERT INTO `yzm_module` VALUES ('wechat', '微信模块', 1, '1.0', '微信模块', '', 0, 0, '2017-11-03', '2017-11-03');
INSERT INTO `yzm_module` VALUES ('diyform', '自定义表单模块', 1, '1.0', '自定义表单模块', '', 0, 0, '2018-01-15', '2018-01-15');
INSERT INTO `yzm_module` VALUES ('adver', '广告管理', 0, '1.0', '广告管理模块', '', 0, 0, '2018-01-18', '2018-01-18');
INSERT INTO `yzm_module` VALUES ('pay', '支付模块', 1, '1.0', '支付模块', '', 0, 0, '2018-07-03', '2018-07-03');

-- ----------------------------
-- Table structure for yzm_order
-- ----------------------------
DROP TABLE IF EXISTS `yzm_order`;
CREATE TABLE `yzm_order`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_sn` char(18) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单状态0未付款1已付款',
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户ID',
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '用户名',
  `addtime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '下单时间',
  `paytime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '支付时间',
  `paytype` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '支付方式1支付宝2微信',
  `transaction` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '第三方交易单号',
  `money` decimal(8, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '订单金额',
  `quantity` decimal(8, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '数量',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1积分,2金钱',
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `desc` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `id`(`id`) USING BTREE,
  INDEX `userid`(`userid`) USING BTREE,
  INDEX `order_sn`(`order_sn`) USING BTREE,
  INDEX `status`(`status`, `type`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '订单表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_order
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_page
-- ----------------------------
DROP TABLE IF EXISTS `yzm_page`;
CREATE TABLE `yzm_page`  (
  `catid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `title` varchar(160) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `introduce` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  INDEX `catid`(`catid`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_page
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_pay
-- ----------------------------
DROP TABLE IF EXISTS `yzm_pay`;
CREATE TABLE `yzm_pay`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `trade_sn` char(18) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `money` char(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '金钱或积分的量',
  `creat_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `msg` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '类型说明',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1积分,2金钱',
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '1成功,0失败',
  `remarks` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注说明',
  `adminnote` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '如是后台操作,管理员姓名',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `userid`(`userid`) USING BTREE,
  INDEX `trade_sn`(`trade_sn`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_pay
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_pay_mode
-- ----------------------------
DROP TABLE IF EXISTS `yzm_pay_mode`;
CREATE TABLE `yzm_pay_mode`  (
  `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `logo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `desc` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `config` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `enabled` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `author` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `version` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `action` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '支付调用方法',
  `template` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_pay_mode
-- ----------------------------
INSERT INTO `yzm_pay_mode` VALUES (1, '支付宝', 'alipay.png', '支付宝新版在线支付插件，要求PHP版本>=5.5', '{\"app_id\":\"\",\"merchant_private_key\":\"\",\"alipay_public_key\":\"\"}', 1, '袁志蒙', '1.0', 'alipay', 'alipay');
INSERT INTO `yzm_pay_mode` VALUES (2, '微信', 'wechat.png', '微信支付提供公众号支付、APP支付、扫码支付、刷卡支付等支付方式。', '{\\\"app_id\\\":\\\"\\\",\\\"app_secret\\\":\\\"\\\",\\\"mch_id\\\":\\\"\\\",\\\"key\\\":\\\"\\\"}', 0, '袁志蒙', '1.0', 'wechat', 'wechat');

-- ----------------------------
-- Table structure for yzm_pay_spend
-- ----------------------------
DROP TABLE IF EXISTS `yzm_pay_spend`;
CREATE TABLE `yzm_pay_spend`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `trade_sn` char(18) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '订单号',
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `money` char(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '金钱或积分的量',
  `creat_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `msg` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '类型说明',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1积分,2金钱',
  `ip` char(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `remarks` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '备注说明',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `userid`(`userid`) USING BTREE,
  INDEX `trade_sn`(`trade_sn`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_pay_spend
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_product
-- ----------------------------
DROP TABLE IF EXISTS `yzm_product`;
CREATE TABLE `yzm_product`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `nickname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `title` varchar(180) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `color` char(9) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `keywords` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `click` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `copyfrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `thumb` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `flag` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '1置顶,2头条,3特荐,4推荐,5热点,6幻灯,7跳转',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `issystem` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `listorder` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `groupids_view` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '阅读权限',
  `readpoint` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '阅读收费',
  `paytype` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '收费类型',
  `is_push` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否百度推送',
  `brand` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '品牌',
  `standard` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '型号',
  `yieldly` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '产地',
  `pictures` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '产品图集',
  `price` decimal(8, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '单价',
  `unit` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '价格单位',
  `stock` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '库存',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `status`(`status`, `listorder`) USING BTREE,
  INDEX `catid`(`catid`, `status`) USING BTREE,
  INDEX `userid`(`userid`, `status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_product
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_store
-- ----------------------------
DROP TABLE IF EXISTS `yzm_store`;
CREATE TABLE `yzm_store`  (
  `id` mediumint(6) UNSIGNED NOT NULL AUTO_INCREMENT,
  `store_name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '门店名称',
  `phone` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '联系方式',
  `keyword` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '关键字',
  `address` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '地址',
  `lat` decimal(10, 7) NULL DEFAULT NULL COMMENT '纬度',
  `lng` decimal(10, 7) NULL DEFAULT NULL COMMENT '经度',
  `province` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '省编号',
  `city` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '市编号',
  `sort` tinyint(5) NOT NULL COMMENT '排序',
  `updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '时间',
  `coordinate` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '坐标',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_store
-- ----------------------------
INSERT INTO `yzm_store` VALUES (6, '冠牛木门凯里品牌专卖店', '111222', '', '贵州凯里永丰路佰杰家居建材市场', NULL, NULL, '24', '522600', 0, 1678497394, '116.25322714334345,40.05126322195607');
INSERT INTO `yzm_store` VALUES (4, '123456', '111222', '山邓小平铜像保护规划编制项目招标公告123', '乌鲁木齐市水磨沟区', NULL, NULL, '13', '350600', 10, 1678446499, '87.64889666219182,43.838251163831174');
INSERT INTO `yzm_store` VALUES (5, '发的是大法官', '111222', 'DH2332F3018', '安徽省蚌埠市禹会区张公山公园', NULL, NULL, '12', '340300', 10, 1678446798, '117.35035802540577,32.92826415550582');
INSERT INTO `yzm_store` VALUES (7, '冠牛木门海口品牌专卖店', '1326547', '', '海南省海口市椰海大道321号喜盈门海口建材家具广场B2-80', NULL, NULL, '21', '460100', 0, 1678497445, '110.34093572262157,19.981976061481795');
INSERT INTO `yzm_store` VALUES (8, '冠牛木门石家庄品牌专卖店', '111', '', '河北省石家庄市长安区光华路321号建华北大街与光华路交叉口北', NULL, NULL, '3', '130100', 0, 1678497549, '114.54931271811493,38.06609540020324');
INSERT INTO `yzm_store` VALUES (9, '冠牛木门任丘品牌专卖店', '1326547', '', '任丘市106国道旁西环建材市场10号楼14号', NULL, NULL, '3', '130900', 0, 1678498184, '116.33112316665789,39.51988368058485');
INSERT INTO `yzm_store` VALUES (10, '冠牛木门深圳福田品牌专卖店1店', '0755-82022', '', '深圳市福田区世纪中心建材市场', NULL, NULL, '19', '440300', 0, 1678513373, '114.07106095831067,22.537448197888736');

-- ----------------------------
-- Table structure for yzm_tag
-- ----------------------------
DROP TABLE IF EXISTS `yzm_tag`;
CREATE TABLE `yzm_tag`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `siteid` tinyint(3) UNSIGNED NOT NULL DEFAULT 0,
  `catid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `tag` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `total` mediumint(9) UNSIGNED NOT NULL DEFAULT 0,
  `click` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `seo_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SEO标题',
  `seo_keywords` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SEO关键字',
  `seo_description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'SEO描述',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `siteid_catid`(`siteid`, `catid`) USING BTREE,
  INDEX `siteid_tag`(`siteid`, `tag`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_tag
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_tag_content
-- ----------------------------
DROP TABLE IF EXISTS `yzm_tag_content`;
CREATE TABLE `yzm_tag_content`  (
  `modelid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `catid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `aid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `tagid` int(10) UNSIGNED NOT NULL DEFAULT 0,
  INDEX `tag_index`(`modelid`, `aid`) USING BTREE,
  INDEX `tagid_index`(`tagid`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Fixed;

-- ----------------------------
-- Records of yzm_tag_content
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_urlrule
-- ----------------------------
DROP TABLE IF EXISTS `yzm_urlrule`;
CREATE TABLE `yzm_urlrule`  (
  `urlruleid` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '规则名称',
  `urlrule` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'URL规则',
  `route` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '指向的路由',
  `listorder` tinyint(3) UNSIGNED NOT NULL DEFAULT 50 COMMENT '优先级排序',
  PRIMARY KEY (`urlruleid`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_urlrule
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_wechat_auto_reply
-- ----------------------------
DROP TABLE IF EXISTS `yzm_wechat_auto_reply`;
CREATE TABLE `yzm_wechat_auto_reply`  (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1关键字回复2自动回复3关注回复',
  `keyword` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '关键字回复的关键字',
  `keyword_type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1完全匹配0模糊匹配',
  `relation_id` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图文回复的关联内容ID',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '文本回复的内容',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `type_index`(`type`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_wechat_auto_reply
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_wechat_group
-- ----------------------------
DROP TABLE IF EXISTS `yzm_wechat_group`;
CREATE TABLE `yzm_wechat_group`  (
  `id` mediumint(9) UNSIGNED NOT NULL DEFAULT 0,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `count` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  UNIQUE INDEX `id`(`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_wechat_group
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_wechat_mass
-- ----------------------------
DROP TABLE IF EXISTS `yzm_wechat_mass`;
CREATE TABLE `yzm_wechat_mass`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `message_type` char(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '消息类型',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0通过openid群发1通过分组群发2全部',
  `media_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `msg_id` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `msg_data_id` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '图文消息的数据ID',
  `receive` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '按组群发为组id，否则为openid列表',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '1任务提交成功2群发已结束',
  `masstime` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '发送时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_wechat_mass
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_wechat_media
-- ----------------------------
DROP TABLE IF EXISTS `yzm_wechat_media`;
CREATE TABLE `yzm_wechat_media`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `originname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `filename` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `filepath` char(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `type` char(6) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `media_type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0临时素材,1永久素材',
  `media_id` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `created_at` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '永久素材的图片url/图文素材标题',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_wechat_media
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_wechat_menu
-- ----------------------------
DROP TABLE IF EXISTS `yzm_wechat_menu`;
CREATE TABLE `yzm_wechat_menu`  (
  `id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `parentid` mediumint(6) NOT NULL DEFAULT 0,
  `name` varchar(48) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1关键字2跳转',
  `keyword` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `event` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `listorder` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parentid`(`parentid`) USING BTREE,
  INDEX `listorder`(`listorder`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_wechat_menu
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_wechat_message
-- ----------------------------
DROP TABLE IF EXISTS `yzm_wechat_message`;
CREATE TABLE `yzm_wechat_message`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `openid` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `issystem` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为系统回复',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `msgtype` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '消息类型',
  `isread` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '1已读0未读',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `openid`(`openid`) USING BTREE,
  INDEX `issystem`(`issystem`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_wechat_message
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_wechat_scan
-- ----------------------------
DROP TABLE IF EXISTS `yzm_wechat_scan`;
CREATE TABLE `yzm_wechat_scan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `scan` varchar(65) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '场景',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '0永久,1临时',
  `expire_time` char(7) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0' COMMENT '二维码有效时间',
  `ticket` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '场景备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_wechat_scan
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_wechat_user
-- ----------------------------
DROP TABLE IF EXISTS `yzm_wechat_user`;
CREATE TABLE `yzm_wechat_user`  (
  `wechatid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `openid` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `groupid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `subscribe` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '1关注0取消',
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `sex` tinyint(1) UNSIGNED NOT NULL DEFAULT 0,
  `city` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `province` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `country` char(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `headimgurl` char(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `subscribe_time` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `remark` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `scan` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '来源场景',
  PRIMARY KEY (`wechatid`) USING BTREE,
  UNIQUE INDEX `openid`(`openid`) USING BTREE,
  INDEX `groupid`(`groupid`) USING BTREE,
  INDEX `subscribe`(`subscribe`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_wechat_user
-- ----------------------------

-- ----------------------------
-- Table structure for yzm_women
-- ----------------------------
DROP TABLE IF EXISTS `yzm_women`;
CREATE TABLE `yzm_women`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `userid` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `username` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `nickname` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `title` varchar(180) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `color` char(9) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `inputtime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `updatetime` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `keywords` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `click` mediumint(8) UNSIGNED NOT NULL DEFAULT 0,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `copyfrom` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `thumb` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `flag` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '1置顶,2头条,3特荐,4推荐,5热点,6幻灯,7跳转',
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `issystem` tinyint(1) UNSIGNED NOT NULL DEFAULT 1,
  `listorder` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `groupids_view` tinyint(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '阅读权限',
  `readpoint` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '阅读收费',
  `paytype` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '收费类型',
  `is_push` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否百度推送',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `status`(`status`, `listorder`) USING BTREE,
  INDEX `catid`(`catid`, `status`) USING BTREE,
  INDEX `userid`(`userid`, `status`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of yzm_women
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
