<?php
/**
 * 广告管理模块
 * @author           袁志蒙  
 * @license          http://www.yzmcms.com
 * @lastmodify       2018-01-18
 */
 
defined('IN_YZMPHP') or exit('Access Denied'); 
yzm_base::load_controller('common', 'admin', 0);
yzm_base::load_sys_class('page','',0);
yzm_base::load_sys_class('form','',0);

class shop extends common {
	
	
	/**
	 * 列表
	 */
	public function init() {
		$adver = D('store');
		$total = $adver->total();
		$page = new page($total, 5);
		$data = $adver->order('id DESC')->limit($page->limit())->select();		
		include $this->admin_tpl('shop_list');
	}	
	


	/**
	 * 添加
	 */
	public function add() {

		if(isset($_POST['dosubmit'])) {
			$_POST['store_name'] = htmlspecialchars($_POST['store_name']);
			if(empty($_POST['store_name']) || empty($_POST['address'])){
				return_json(array('status'=>0,'message'=>'请填写名称和地址'));
			}
			$_POST['province'] = !empty($_POST['province']) ? $_POST['province'] : '1';
			$_POST['city'] = !empty($_POST['city']) ? $_POST['city'] : '110100';
			$_POST['updatetime'] = time();
			if(D('store')->insert($_POST)){
				return_json(array('status'=>1,'message'=>L('operation_success')));
			}else{
				return_json(array('status'=>0,'message'=>L('operation_failure')));
			}
		}else{
			include $this->admin_tpl('shop_add');
		}
	}


	
	/**
	 * 编辑
	 */
	public function edit() {
		
		if(isset($_POST['dosubmit'])) {
			$id = isset($_POST['id']) ? intval($_POST['id']) : 0;
			$_POST['store_name'] = htmlspecialchars($_POST['store_name']);
			if(empty($_POST['store_name']) || empty($_POST['address'])){
				return_json(array('status'=>0,'message'=>'请填写名称和地址'));
			}
			$_POST['province'] = !empty($_POST['province']) ? $_POST['province'] : '1';
			$_POST['city'] = !empty($_POST['city']) ? $_POST['city'] : '110100';
			$_POST['updatetime'] = time();
			if(D('store')->update($_POST, array('id'=>$id))){
				return_json(array('status'=>1,'message'=>L('operation_success')));
			}else{
				return_json();
			}
		}else{
			$id = isset($_GET['id']) ? intval($_GET['id']) : 0;
			$data = D('store')->where(array('id'=>$id))->find();
			include $this->admin_tpl('shop_edit');			
		}
	}

	
	/**
	 * 删除
	 */
	public function del() {
		if($_POST && is_array($_POST['id'])){
			if(D('store')->delete($_POST['id'], true)){
				showmsg(L('operation_success'), '', 1);
			}else{
				showmsg(L('operation_failure'));
			}
		}
	}
	
	
	/**
	 * 组合广告值
	 */
	private function get_code($data) {
		if($data['type'] == 1){
			return '<a href="'.$data['url'].'" target="_blank" title="'.$data['title'].'">'.strip_tags($data['text']).'</a>';
		}
		
		if($data['type'] == 2){
			return $data['text'];
		}		
			
		return '<a href="'.$data['url'].'" target="_blank" title="'.$data['title'].'"><img src="'.$data['img'].'"></a>';
	}

}