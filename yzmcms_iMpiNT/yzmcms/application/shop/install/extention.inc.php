<?php
defined('IN_YZMPHP') or exit('Access Denied');
defined('INSTALL') or exit('Access Denied');

$parentid = $menu->insert(array('name'=>'门店管理', 'parentid'=>3, 'm'=>'store', 'c'=>'shop', 'a'=>'init', 'data'=>'', 'listorder'=>0, 'display'=>'1'));
$menu->insert(array('name'=>'添加门店', 'parentid'=>$parentid, 'm'=>'store', 'c'=>'shop', 'a'=>'add', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu->insert(array('name'=>'修改门店', 'parentid'=>$parentid, 'm'=>'store', 'c'=>'shop', 'a'=>'edit', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
$menu->insert(array('name'=>'删除门店', 'parentid'=>$parentid, 'm'=>'store', 'c'=>'shop', 'a'=>'del', 'data'=>'', 'listorder'=>0, 'display'=>'0'));
