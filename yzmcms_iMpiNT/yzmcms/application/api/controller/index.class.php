<?php
/**
 * 系统API接口类
 * @author           袁志蒙  
 * @license          http://www.yzmcms.com
 * @lastmodify       2017-01-18
 */

defined('IN_YZMPHP') or exit('Access Denied');
new_session_start();

class index{

		/**
	 * 百度地图获取坐标
	 * 请求方式：GET
	 */
	public function get_site($str){
		//API控制台申请得到的ak（此处ak值仅供验证参考使用）
		$ak = 'gAaG8T7u72QAH9DGkwzntRagn9TLxnGB';

		//应用类型为for server, 请求校验方式为sn校验方式时，系统会自动生成sk，可以在应用配置-设置中选择Security Key显示进行查看（此处sk值仅供验证参考使用）
		$sk = 'K4K78ZnzAHxLen16IbDZolgRDGzIxE6B';

		//以Geocoding服务为例，地理编码的请求url，参数待填
		$url = "https://api.map.baidu.com/place/v2/search/?region=%s&query=%s&output=%s&ak=%s&sn=%s";

		//get请求uri前缀
		$uri = '/place/v2/search/';

		//地理编码的请求中address参数
		$region = $str;
		$query = $str;

		//地理编码的请求output参数
		$output = 'json';

		//构造请求串数组
		$querystring_arrays = array (
			'region' => $region,
			'query' => $query,
			'output' => $output,
			'ak' => $ak
		);

		//调用sn计算函数，默认get请求
		$sn = $this->caculateAKSN($ak, $sk, $uri, $querystring_arrays);

		//请求参数中有中文、特殊字符等需要进行urlencode，确保请求串与sn对应
		$target = sprintf($url, urlencode($region), urlencode($query),  $output, $ak, $sn);
        $info=file_get_contents($target);
        $info=json_decode($info,1);
        return $info;
        //var_dump($info['results'][0]['location']);
		//输出计算得到的sn
		//echo "sn: $sn \n";

		//输出完整请求的url（仅供参考验证，故不能正常访问服务）
		//echo "url: $target \n";	
       // $sheng=isset($_GET['sheng']) ? intval($_GET['sheng']) : 1;
       // $url="https://api.map.baidu.com/place/v2/search?ak=gAaG8T7u72QAH9DGkwzntRagn9TLxnGB&&query=".$sheng."&region=".$sheng;
       // $info=$this->curl_get($url);
       // var_dump($url);
	}
    
    public function caculateAKSN($ak, $sk, $url, $querystring_arrays, $method = 'GET')
	{  
	    if ($method === 'POST'){  
	        ksort($querystring_arrays);  
	    }  
	    $querystring = http_build_query($querystring_arrays);  
	    return md5(urlencode($url.'?'.$querystring.$sk));  
	}

	public function curl_get($url){	
		//初始化

		$ch = curl_init();

		//设置选项，包括URL

		curl_setopt($ch, curlOPT_URL, $url);

		curl_setopt($ch, curlOPT_RETURNTRANSFER, 1);

		curl_setopt($ch, curlOPT_HEADER, 0);

		//执行并获取HTML文档内容

		$output = curl_exec($ch);

		//释放curl句柄

		curl_close($ch);

		//打印获得的数据

		return $output;
	}

		/**
	 * 百度地图获取门店
	 * 请求方式：GET
	 */
	public function getmarket(){
		$sheng = isset($_REQUEST['sheng']) ? intval($_REQUEST['sheng']) : '';
		$shi = isset($_REQUEST['shi']) ? intval($_REQUEST['shi']) : '';
		$keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
		$where='1=1';
        if(!empty($sheng)){
        	$where.=' AND province='.$sheng;
        }
        if(!empty($shi)){
        	$where.=' AND city='.$shi;
        }
        if(!empty($keyword)){
        	$where .= ' AND `address` LIKE \'%'.$keyword.'%\'';
        }
        //var_dump($where,$_REQUEST['keyword']);exit;
		$data = D('store')->where($where)->order('id DESC')->select();
        return_json($data);
	}

		/**
	 * 百度地图获取市
	 * 请求方式：GET
	 */
	public function getshi(){
		$sheng = isset($_POST['sheng']) ? intval($_POST['sheng']) : 1;
		$area = file_get_contents(STATIC_URL . 'lucation.json');
        $area_arr = json_decode($area,1);
        $list = [];
        $arr=[];
        $city=[];
        foreach ($area_arr as $k=>$v) {
        	$list=$v['86'];
        	$city[]=$v;
            // foreach ($v as $k2 => $v2) {
                
            //     if($k == '86'){
            //         $list[$k2] = [
            //             'value' => $k2,
            //             'label' => $v2,
            //         ];
            //     }else{
            //         $list[$k]['children'][$k2] = [
            //             'value' => $k2,
            //             'label' => $v2,
            //         ];
            //     }
            // }
        }
        $a=1;
        foreach ($list as $k=>$v) {
        	$arr[$a]['value']=$v;
        	$arr[$a]['key']=$k;
        	$a++;
        }
        foreach ($list as $k=>$v) {
        	$arr[$a]['value']=$v;
        	$arr[$a]['key']=$k;
        	$a++;
        }
        $info=[];
        foreach ($city[0][$arr[$sheng]['key']] as $kk=>$vv) {
        	$info[]="<option class='city_".$kk."' value='".$kk."'>".$vv."</option>";
        }
        //var_dump($info);exit;
        return_json($info);
        // foreach ($list as $k=>&$v){
        //     foreach ($v['children'] as $k2 => &$v2) {
        //         if(isset($area_arr[$k2]) && $area_arr[$k2]){
        //             foreach ($area_arr[$k2] as $k3=>$v3){
        //                 $v2['children'][] = [
        //                     'value' => $v3,
        //                     'label' => $k3,
        //                 ];
        //             }

        //         }
        //     }
        // }

        // $arr = array_values($list);
        // foreach ($arr as &$item){
        //     $item['children'] = array_values($item['children']);
        //     foreach ($item['children'] as &$child){
        //         if (isset($child['children']) && $child['children']){
        //             $child['children'] = array_values($child['children']);

        //         }
        //     }
        // }
        
	}
	
	
	/**
	 * 验证码图像
	 * 请求方式：GET
	 */
	public function code(){	
		$code = yzm_base::load_sys_class('code');
		if(isset($_GET['width']) && intval($_GET['width'])) $code->width = intval($_GET['width']);
		if(isset($_GET['height']) && intval($_GET['height'])) $code->height = intval($_GET['height']);
		if(isset($_GET['code_len']) && intval($_GET['code_len'])) $code->code_len = intval($_GET['code_len']);
		if(isset($_GET['font_size']) && intval($_GET['font_size'])) $code->font_size = intval($_GET['font_size']);
		if($code->width > 500 || $code->width < 10)  $code->width = 100;
		if($code->height > 300 || $code->height < 10)  $code->height = 35;
		if($code->code_len > 8 || $code->code_len < 2)  $code->code_len = 4;
		$code->show_code();
		$_SESSION['code'] = $code->get_code();
	}


	/**
	 * 检测内容标题是否存在
	 * 请求方式：GET
	 * @param title
	 * @param modelid 可选
	 * @return {-1:错误;0存在;1:不存在;}
	 */	
	public function check_title(){

		$modelid = isset($_GET['modelid']) ? intval($_GET['modelid']) : 0;
		$title = isset($_GET['title']) ? htmlspecialchars($_GET['title']) : '';
		if(!$title) return_json(array('status'=>-1, 'message'=>L('lose_parameters')));

		if(!isset($_SESSION['adminid']) && !isset($_SESSION['_userid'])) return_json(array('status'=>-1, 'message'=>L('login_website')));

		$dbname =  $modelid ? get_model($modelid) : 'all_content';
		if(!$dbname)  return_json(array('status'=>-1, 'message'=>L('illegal_parameters')));
		$title = D($dbname)->field('id')->where(array('title'=>$title))->one();
		$title ? return_json(array('status'=>0, 'message'=>'内容标题已存在！')) : return_json(array('status'=>1, 'message'=>'内容标题不存在！'));
	}
	

	/**
	 * 收藏文档，必须登录
	 * 请求方式：POST
	 * @param url 地址
	 * @param title 标题
	 * @return {2:取消收藏;1:已收藏;-1:未登录;-2:缺少参数}
	 */	
	public function favorite(){	
		if(isset($_POST['title']) && isset($_POST['url'])) {
			$title = htmlspecialchars(addslashes($_POST['title']));
			$url = safe_replace(addslashes($_POST['url']));
		} else {
			return_json(array('status'=>-2, 'message'=>L('lose_parameters')));
		}

		$userid = $this->_check_login();

		$favorite = D('favorite');
		$id = $favorite->field('id')->where(array('userid'=>$userid, 'url'=>$url))->one();
		if($id) {
			$favorite->delete(array('id'=>$id));
			return_json(array('status'=>2, 'message'=>'取消收藏'));
		}else{
			$data = array('title'=>$title, 'url'=>$url, 'inputtime'=>SYS_TIME, 'userid'=>$userid);
			$favorite->insert($data);
			return_json(array('status'=>1, 'message'=>'已收藏'));
		}
	}


	/**
	 * 检查是否收藏，必须登录
	 * 请求方式：POST
	 * @param url 地址
	 * @return {1:已收藏;0:未收藏;-1:未登录;-2:缺少参数}
	 */	
	public function check_favorite(){	

		$userid = $this->_check_login();
		$url = isset($_POST['url']) ? safe_replace(addslashes($_POST['url'])) : return_json(array('status'=>-2, 'message'=>L('lose_parameters')));

		$id = D('favorite')->field('id')->where(array('userid'=>$userid, 'url'=>$url))->one();
		$id ? return_json(array('status'=>1, 'message'=>'已收藏')) : return_json(array('status'=>0, 'message'=>'未收藏'));

	}


	/**
	 * 获取会员中心消息数量，必须登录
	 * 请求方式：GET
	 * @return {1:成功;-1:未登录;}
	 */	
	public function message(){

		$userid = $this->_check_login();

		$member = D('member');
		$memberinfo = $member->field('groupid,username')->where(array('userid'=>$userid))->find();

		//系统消息[群发]
		$system_totnum = D('message_group')->where(array('groupid' => $memberinfo['groupid']))->total(); //总条数
		$total = D('message_group')->alias('a')->join('yzmcms_message_data b ON a.id=b.group_message_id', 'LEFT')->where(array('a.groupid'=>$memberinfo['groupid'], 'a.status'=>1, 'b.userid'=>$userid))->total();  //已读信息
		$data['system_msg'] = $system_totnum - $total;   //系统消息，未读条数
		
		//收件箱消息，未读条数
		$data['inbox_msg'] = D('message')->where(array('send_to' => $memberinfo['username'], 'status' => '1', 'isread' => '0'))->total(); 
		return_json(array('status'=>1, 'message'=>L('operation_success'), 'data'=>$data));
	}


	/**
	 * 检查是否关注会员，必须登录
	 * 请求方式：GET
	 * @param title
	 * @param modelid 或 catid
	 * @return {-1:错误;0已关注;1:未关注;}
	 */	
	public function check_follow(){

		$current_userid = $this->_check_login();
		$userid = isset($_GET['userid']) ? intval($_GET['userid']) : return_json(array('status'=>-1, 'message'=>L('lose_parameters')));
		$id = D('member_follow')->field('id')->where(array('userid'=>$current_userid, 'followid'=>$userid))->one();
		$id ? return_json(array('status'=>1, 'message'=>'已关注')) : return_json(array('status'=>0, 'message'=>'未关注'));
	}


	/**
	 * 下载远程图片
	 * 请求方式：POST
	 * @param contnet
	 * @return {0错误;1正确;}
	 */	
	public function down_remote_img(){
		if(!isset($_SESSION['adminid']) && !isset($_SESSION['_userid'])) return_json(array('status'=>0, 'message'=>L('login_website')));
		$content = isset($_POST['content']) ? trim($_POST['content']) : return_json(array('status'=>0, 'message'=>L('lose_parameters')));

		return_json(array('status'=>1, 'message'=>L('operation_success'), 'data'=>down_remote_img($content))); 
	}


	/**
	 * 私有方法，检查是否登录
	 */	
	private function _check_login(){
		$userid = intval(get_cookie('_userid'));
		if(!$userid) return_json(array('status'=>-1, 'message'=>L('login_website')));
		return $userid;
	}

}