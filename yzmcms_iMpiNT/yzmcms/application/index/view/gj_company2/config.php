<?php return array (
  'name' => '经纬-企业站模板01',
  'author' => '经纬',
  'dirname' => 'gj_company',
  'version' => '1.0',
  
  //频道页模板
  'category_temp' =>  
  array (
		'category_article' => '新闻频道页模板', 
		'category_product' => '产品频道页模板',
		'category_download' => '下载频道页模板',
		'category_case' => '案例频道页模板',
		'category_join' => '加入我们频道页模板',
		'category_page' => '单页面模板',
		'category_about' => '我们的冠牛',  
  ),
  
  
  //列表页模板
  'list_temp' =>  
  array (
		'list_article' => '新闻列表页模板', 
		'list_article_img' => '新闻列表页模板', 
		'list_case' => '案例列表页模板', 
		'list_product' => '产品列表页模板',
		'list_download' => '下载列表页模板',
		'list_diyform' => '自定义表单列表模板', 		
		'list_join' => '加入我们列表模板', 		
  ),
  
  
   //内容页模板
  'show_temp' => 
  array (
	    'show_article' => '新闻内容页模板', 
	    'show_product' => '产品内容页模板', 
	    'show_download' => '下载内容页模板', 
	    'show_diyform' => '自定义表单内容页模板', 
	    'show_case' => '案例内容页模板', 
  ) 
  
);?>