<?php
/**
 * 动态修改前端模板颜色
 */
class style{
	

	public function init() {
		$arr = array('col-yh', 'col-ml', 'col-sl', 'col-hs', 'col-cs');
		$style = isset($_POST['style']) ? intval($_POST['style']) : 0;
		$color = isset($arr[$style]) ? $arr[$style] : $arr[0];
		set_cookie('color', $color, 3600*24*7);
	}

}