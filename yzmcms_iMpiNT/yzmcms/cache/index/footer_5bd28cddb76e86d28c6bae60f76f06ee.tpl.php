<?php defined('IN_YZMPHP') or exit('No permission resources.'); ?><style>
	.bd_weixin_popup {
		box-sizing: content-box;
	}

	.bdshare-button-style0-24:after {
		content: "" !important;
	}

	.footer {
		/* position: relative;
        margin-top: 2rem;
        height: 25rem;*/

	}

	.footer-tp {
		padding: 62px 0 46px;
		/*width: 92%;*/
		margin: auto;
	}

	@media screen and (max-width: 769px) {
		.footer-tp{
			padding-top: 30px;
		}
		.bottom .bottomfix {
			display: block;
		}
        .bdsharebuttonbox{
            margin: auto;
        }
		.bottom .bottom-le,
		.bottom .bottom-lis {
			width: 100%;
			text-align: center;
		}
		.footer-tp .footer-nav {
			display: none;
		}

		.bottomfix .footer-nav-mobile {
			display: block;
		}

		.footer-nav-mobile .footer-list {
			height: 31px;
			overflow: hidden;
		}
	}

	@media screen and (max-width: 1024px) {
	    .footer-tp .bottomfix {
			display: block;
		}
		.footer-nav,.footer-ewm{
		    width: 100%;
		}
		.footer-ewm {
			margin: auto;
			margin-top: 30px;
		}
	}

	.footer-list dt {
		font-size: 16px;
		color: #555;
		line-height: 2.2em;
		padding-bottom: 8px;
	}

	.footer-list dd {
		font-size: 14px;
		line-height: 2.1em;
	}

	.footer-list dd a {
		color: #777;
	}

	.footer-list {
		display: inline-block;
		vertical-align: top;
		margin-right: 35px;
	}

	.footer-nav {
		/* float: left; */
		font-size: 0;
	}

	.footer-nav-mobile {
		display: none;
	}

	.footer-nav-mobile .footer-list,
	.footer-nav-mobile .footer-list dt,
	.footer-nav-mobile .footer-list dd p {
		display: block;
		width: 100%;
		margin: 0px;
		padding: 0px;
	}

	.footer-nav-mobile .footer-list dt,
	.footer-nav-mobile .footer-list dd p{
		border-bottom: 1px solid #333;
		cursor: pointer;
		line-height: 30px;
	}

	.bottomfix {
		display: flex;
		justify-content: center;
	}
	.clearfix{
		display: flex;
		justify-content: center;
		align-items: center;
		justify-content: space-between;
		flex-wrap: wrap;
	}

	.container2 {
		width: 92%;
		margin: auto;
	}

	.footer-list dd a:hover {
		color: #eb9030;
	}

	.footer-ewm {
		/* float: right; */
		width: 242px;
		/* position: relative; */
		/* right: 7rem; */
	}

	.footer-ewm .imgbox {
		overflow: hidden;
		padding-bottom: 20px;
	}

	.footer-ewm .box {
		width: 110px;
		text-align: center;
	}

	.footer-ewm p {
		font-size: 16px;
		color: #555;
		line-height: 1.7em;
		padding-bottom: 6px;
	}

	.footer-ewm .box:nth-child(2n+1) {
		float: left;
	}

	.footer-ewm .box:nth-child(2n) {
		float: right;
	}

	.footer-ewm .kf {
		font-size: 16px;
		color: #555;
		line-height: 2.5em;
		text-align: center;
		border: 1px solid #ccc;
	}

	.kuan1 {
		height: 25px;
		line-height: 25px;
		width: 72%;
		max-width: 500px;
		margin-bottom: 20px;
		font-size: 14px;
		border: 1px solid #002e73;
	}

	.fenye {
		width: 55%;
		text-align: center;
		margin: 0 auto;
		padding: 80px 0;
	}

	.fenye li {
		display: inline;
		margin: 5px;
	}

	.footer-ewm .tel {
		font-size: 24px;
		color: #eb9030;
		line-height: 1.3em;
		padding-top: 18px;
	}

	.footer-ewm .tel span {
		font-size: 16px;
	}

	.bottom {
		background: #333;
		/* padding: 12px 0 16px; */
		/* position: relative; */
		/* top: 0rem; */
		/* height: 2rem; */
	}

	.bottom-le {
		/*float:left;
    width:70%;*/
	}

	.bottom-le {
		float: left;
		color: #fff;
		/* height: 30px; */
		line-height: 30px;
		font-size: 14px;
		/* position: relative; */
		/* left: 7.5rem; */
		/* top: 0.3rem; */

	}

	.bottom-le a {
		color: #ccc;
		margin-left: 12px;
	}

	.bottom-le span {
		margin-right: 8px;
	}

	.bottom-lis {
		/* float: right;
    		position: relative;
    		right: 6.5rem;
    		top: -0.2rem; */
	}

	.bottom-lis li {
		display: inline-block;
		vertical-align: middle;
		width: 30px;
		height: 30px;
		background-repeat: no-repeat;
		background-size: 100% 100%;
		background-position: center center;
		margin-left: 8px;
		-moz-border-radius: 50%;
		-webkit-border-radius: 50%;
		-ms-border-radius: 50%;
		-o-border-radius: 50%;
		border-radius: 0%;
		overflow: hidden;
	}

	.bottom-lis a {
		display: block;
		width: 100%;
		height: 100%;
	}

	.bottom-lis .li1 {
		background-image: url(../images/f1.png);
	}

	.bottom-lis .li1:hover {
		background-image: url(../images/ff1.png);
	}

	.bottom-lis .li2 {
		background-image: url(../images/f2.png);
	}

	.bottom-lis .li2:hover {
		background-image: url(../images/ff2.png);
	}

	.bottom-lis .li3 {
		background-image: url(../images/f3.png);
	}

	.bottom-lis .li3:hover {
		background-image: url(../images/ff3.png);
	}



	.hd-bt .btn {
		float: right;
		width: 36px;
		height: 40px;
		margin: 4px 0;
		cursor: pointer;
		display: none;
	}

	.hd-bt {
		position: relative;
	}

	.tabs-list {
		border-bottom: 10px solid #f1f1f1;
		padding: 18px 0;
		text-align: center;
	}

	.tabs-list li {
		font-size: 16px;
		color: #555;
		line-height: 2em;
		display: inline-block;
		vertical-align: top;
		padding: 0 38px;
	}

	.tabs-list li a {
		color: #555;
	}



	.menu {
		font-size: 0;
		text-align: right;
		display: block;
	}

	.menu>li {
		display: inline-block;
		vertical-align: top;
		font-size: 14px;
		line-height: 52px;
		background: url(../images/hb.jpg) no-repeat right center;
		position: relative;
	}

	.menu>li>a {
		padding: 0 30px;
		display: block;
		color: #555;
	}

	.menu>li:last-child {
		background: none;
	}

	.menu>li.cur>a {
		color: #eb9030;
	}

	.hd-tp .container2 {
		position: relative;
		padding-top: 30px;
		padding-bottom: 30px;
	}

	.hd-tp .logo {
		position: absolute;
		left: 0;
		top: 0;
	}


	#img-list li {
		display: none;
		opacity: 0.2;
	}

	#img-list li.show {
		display: block;
		opacity: 1;
	}

	.banner-box {
		position: relative;
	}

	#dot {
		position: absolute;
		width: 100%;
		text-align: center;
		left: 0;
		bottom: 38px;
		z-index: 1100;
	}

	#dot li {
		display: inline-block;
		vertical-align: top;
		width: 30px;
		height: 5px;
		background: #eee;
		border-radius: 12px;
		margin: 0 4px;
		cursor: pointer;
		position: relative;
	}

	#dot li.cur {
		width: 60px;
		background: #cca581;
	}

	#img-list li {
		z-index: 999;
	}
</style>
<div class="footer">
	<div class="footer-tp">
		<div class="container2 bottomfix">
			<div class="footer-nav">
				<?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'nav')) {$data = $tag->nav(array('field'=>'catid,catname,arrchildid,pclink,target','where'=>"parentid=0",'limit'=>'20',));}?>
				<?php if(is_array($data)) foreach($data as $v) { ?>
				<dl class="footer-list">
					<dt num="0"><?php echo $v['catname'];?></dt>
					<?php if(is_childid($v)) { ?>
					<?php $r = get_childcat($v['catid']);?>
					<dd>
						<?php if(is_array($r)) foreach($r as $v) { ?>
						<?php if(!$v['display']) continue;?>
						<p><a href="<?php echo $v['pclink'];?>" target="<?php echo $v['target'];?>"><?php echo $v['catname'];?></a></p>
						<?php } ?>
					</dd>
					<?php } ?>
				</dl>
				<?php } ?>
				<!--<dl class="footer-list">-->
				<!--	<dt num="0">下载中心</dt>-->
				<!--	<dd>-->
				<!--		<p><a href="<?php echo $site['site_url'];?>xiazaizhongxin">资料下载</a></p>-->
				<!--	</dd>-->
				<!--</dl>-->
			</div>
			<div class="footer-nav-mobile">
				<?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'nav')) {$data = $tag->nav(array('field'=>'catid,catname,arrchildid,pclink,target','where'=>"parentid=0",'limit'=>'20',));}?>
				<?php if(is_array($data)) foreach($data as $v) { ?>
				<dl class="footer-list">
					<dt num="0"><?php echo $v['catname'];?></dt>
					<?php if(is_childid($v)) { ?>
					<?php $r = get_childcat($v['catid']);?>
					<dd>
						<?php if(is_array($r)) foreach($r as $v) { ?>
						<?php if(!$v['display']) continue;?>
						<p><a href="<?php echo $v['pclink'];?>" target="<?php echo $v['target'];?>"><?php echo $v['catname'];?></a></p>
						<?php } ?>
					</dd>
					<?php } ?>
				</dl>
				<?php } ?>
				<!--<dl class="footer-list">-->
				<!--	<dt num="0">下载中心</dt>-->
				<!--	<dd>-->
				<!--		<p><a href="<?php echo $site['site_url'];?>xiazaizhongxin">资料下载</a></p>-->
				<!--	</dd>-->
				<!--</dl>-->
			</div>
			<div class="footer-ewm">
				<div class="imgbox">
					<div class="box">
						<img src="<?php echo $wei_qrcode;?>" alt="">
						<p>官方微信</p>
					</div>
					<div class="box">
						<img src="<?php echo $mobile_qrcode;?>" alt="">
						<p>手机网站</p>
					</div>
				</div>
				<div class="kf">在线客服</div>
				<div class="tel" style="width:270px;">
					<span>服务热线：</span>
					<?php echo $phone;?>
				</div>
			</div>
		</div>
	</div>
	<div class="bottom">
		<div class="container2 clearfix">
			<div class="bottom-le">
				<a href="https://beian.miit.gov.cn/" target="_blank"><?php echo $site['site_filing'];?></a>&nbsp; &nbsp; &nbsp; &nbsp;
				<!-- <a style="color: #fff" href="http://www.heyou51.com/" target="_blank">网站建设</a><a style="color: #fff">：心雨</a>&nbsp; --><a
					style="color: #fff" href="http://www.heyou51.cn/" target="_blank">网易企业邮箱</a>
			</div>


			<div class="bdsharebuttonbox bdshare-button-style0-24" data-bd-bind="1677670744592">
				<ul class="bottom-lis">
					<li>
						<a href="<?php echo $site['site_url'];?>#" class="bds_weixin" data-cmd="weixin"
							title="分享到微信"></a>
					</li>
					<li>
						<a href="<?php echo $site['site_url'];?>#" class="bds_tsina" data-cmd="tsina"
							title="分享到新浪微博"></a>
					</li>
					<li>
						<a href="<?php echo $site['site_url'];?>#" class="bds_sqq" data-cmd="sqq"
							title="分享到QQ好友"></a>
					</li>

					<script>
						window._bd_share_config = {
							"common": {
								"bdSnsKey": {},
								"bdText": "",
								"bdMini": "1",
								"bdMiniList": false,
								"bdPic": "",
								"bdStyle": "0",
								"bdSize": "24"
							},
							"share": {}
						};
						with(document) 0[(getElementsByTagName('head')[0] || body).appendChild(createElement('script')).src =
							'/static/api/js/share.js?v=89860593.js?cdnversion=' + ~(-new Date() /
								36e5)];
					</script>
				</ul>
			</div>
		</div>
	</div>
</div>
</div>

</body>

</html>
<script type="text/javascript">
	$(".footer-nav-mobile .footer-list dt").click(function() {
		if ($(this).parent(".footer-list").height() > 31) {
			$(this).parent(".footer-list").animate({
				'height': '31px'
			}, 500)
		} else {
			var length = $(this).parent().find("dd p").length;
			$(this).parent(".footer-list").animate({
				'height': (length + 1) * 31 + 'px'
			}, 500).siblings().animate({
				'height': '31px'
			}, 500)
		}
	})
</script>
