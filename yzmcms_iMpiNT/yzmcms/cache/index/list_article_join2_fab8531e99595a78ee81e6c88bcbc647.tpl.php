<?php defined('IN_YZMPHP') or exit('No permission resources.'); ?><?php include template("index","header"); ?>
<style>
.banner {
    position:relative;
     /* height: 20rem; */
}
.banner img {
    width:100%;
    height: 100%;
}
.banner-bg {
    position:absolute;
    left:0;
    top:0;
    width:100%;
    height:100%;
    background:rgba(0, 0, 0, 0.3);
}
.banner-menu {
   position:absolute;
   transform: translateY(-50%);
   left:0;
   top:62%;
   width:100%;
   text-align:center;
   z-index: 888;
   font-size:0;
}
.banner-menu .menu-tit {
    font-size:28px;
    color:#fff;
    line-height:1.4em;
    padding-bottom:3.8%;
    font-weight:normal;
    letter-spacing:1px;
}
.banner-lis {
    display:inline-block;
    padding:0 50px 0 30px;
    -moz-border-radius:30px;
    -webkit-border-radius:30px;
    -ms-border-radius:30px;
    -o-border-radius:30px;
    background:rgba(255, 255, 255, 0.9);
    border-radius:30px;
}
.banner-lis li {
    display:inline-block;
    vertical-align:top;
    font-size:16px;
    line-height:62px;
    margin:0 26px;
}
.banner-lis li a {
    padding-left:20px;
    display:block;
    color:#555;
}
.banner-lis li.on a {
    background:url("<?php echo $site['site_url'];?>common/static/image/arr.png") no-repeat left center;
}
.banner-lis a:hover {
    text-decoration:none;
    background:url("<?php echo $site['site_url'];?>common/static/image/arr.png") no-repeat left center;
}
.banner-img {
	position: relative;
	margin-top: 4rem;
	width: 100%;
}


.nwmd-xq {
    padding:50px 0 90px;
}
.nwmd-xqli {
    padding-top:20px;
}
.nwmd-xqli li {
    font-size:14px;
    color:#555;
    line-height:2.1em;
    display: inline-block;
    vertical-align: middle;
    margin:0 6px;
    -moz-border-radius:20px;
    -webkit-border-radius:20px;
    -ms-border-radius:20px;
    -o-border-radius:20px;
    border-radius:20px;
    overflow: hidden;
}
.nwmd-xqli {
    text-align:center;
}
.nwmd-xqli li a {
    padding:0 18px;
    display: block;
    color:#555;
}
.nwmd-xqli li.on,.nwmd-xqli li:hover {
    background:#eb9030;
    color:#fff;
}
.nwmd-xqli li.on a,.nwmd-xqli li:hover a{
    color:#fff;
}
.nwmd-xqcont {
    padding-top:60px;
}
.nwmd-xqtp {
    overflow: hidden;
    position: relative;
    -moz-border-bottom-right-radius: 36px;
    -ms-border-bottom-right-radius: 36px;
    -webkit-border-bottom-right-radius: 36px;
    -o-border-bottom-right-radius: 36px;
    border-bottom-right-radius: 36px;
    background:#f3f3f3;
    -moz-border-top-left-radius: 36px;
    -ms-border-top-left-radius: 36px;
    -webkit-border-top-left-radius: 36px;
    -o-border-top-left-radius: 36px;
    border-top-left-radius: 36px;
}
.nwmd-xqle {
    width: 40%;
    overflow: hidden;
    float: left;
}
.nwmd-xq .nwmd-xqcont:nth-of-type(2n+1) .nwmd-xqtp .nwmd-xqle{
    float: right;
}
.nwmd-xqlr {
    float: left;
    -moz-box-sizing:border-box;
    -webkit-box-sizing:border-box;
    -ms-box-sizing:border-box;
    -o-box-sizing:border-box;
    box-sizing:border-box;
    width: 60%;
    padding:40px 40px 0;
}
.nwmd-xqtxt {
    font-size:13px;
    color:#777;
    line-height:1.6em;
}
.nwmd-xqtxt p {
    padding-bottom:30px;
}
.nwmd-xqbt {
    padding-top:26px;
}
.nwmd-xqbt li {
    display: inline-block;
    vertical-align: top;
    width:31.6%;
    margin-right:2.6%;
    position: relative;
    -moz-border-top-left-radius: 18px;
    -ms-border-top-left-radius: 18px;
    -o-border-top-left-radius: 18px;
    -webkit-border-top-left-radius: 18px;
    border-top-left-radius: 18px;
    overflow: hidden;
    -moz-border-bottom-right-radius: 18px;
    -webkit-border-bottom-right-radius: 18px;
    -ms-border-bottom-right-radius: 18px;
    -o-border-bottom-right-radius: 18px;
    border-bottom-right-radius: 18px;
    margin-bottom:2.6%;
}
.nwmd-xqbt li:nth-child(3n) {
    margin-right:0;
}
.nwmd-xqbt li p {
    position: absolute;
    left:0;
    bottom:0;
    width:100%;
    background: rgba(0, 0, 0, 0.6);
    width:100%;
    text-align:center;
    font-size:16px;
    color:#fff;
    line-height:2.8em;
}
.nwmd-xqbt {
    font-size: 0;
}
.tit-box {
    text-align:center;
}
.tit-box .en {
    font-size:26px;
    color:#ccc;
    line-height:1.3em;
    font-weight: normal;
    font-family: font;
    text-transform:uppercase;
}
.tit-box .ch {
    font-size:24px;
    color:#555;
    line-height:1.75em;
    font-weight: normal;
}
.tit-box .ch b {
    font-weight: normal;
    color:#cca581;
}
.tit-box span {
    display:inline-block;
    padding:0 40px;
    position:relative;
}
.tit-box span:before,
.tit-box span:after {
    content:"";
    position:absolute;
    top:50%;
    width:15vw;
    height:1px;
    background:#eaeaea;
}
.tit-box span:before {
    left:100%;
}
.tit-box span:after {
    right:100%;
}
@media screen and (max-width:1280px) {
	.banner .banner-img {
		margin-top: 2.6rem;
	}
}
/* 手机 */
	@media screen and (max-width: 769px) { 
	    .banner-menu .banner-lis li.on a {
            color: #eb9030;
            background: none;
        }
		.tit-box .ch {
		    font-size: 18px;
		}
		.tit-box .en {
		    font-size: 20px;
			margin-bottom: 0;
		}
		.tit-box .ch {
		    font-size: 18px;
			margin:0px;
			padding:0px;
		}
		.tit-box .en {
		    font-size: 20px;
			margin-bottom: 0;
		}
		.menu-tit {
			display: none;
		}
	
		.banner .banner-img {
			width: 100%;
			height: auto;
			margin-top: 2.5rem;
		}
	
		.banner {
			height: auto !important;
		}
	
		.banner .banner-menu { transform: translateY(0); border-bottom: 10px solid #f1f1f1;
			position: static;
			top: 0;
		}
	
		.nwod .nwod-box {
			padding: 7% 0 8%;
		}
	
		.banner-menu .banner-lis {
			width: 100%;
			text-align: left;
		}
	
		.banner-menu .banner-lis {
			margin: 0px;
			padding: 0px;
		}
	
		.gnsx-xq .gnsx-xqli li {
			width: 100%;
			text-align: center;
		}
	
		.gnsx-xq .gnsx-xqli li .li-tit {
			padding-top: 10px;
			font-size: 16px;
		}
	
		.gnsx-xqli .li-img {
			width: 60%;
			margin: auto;
		}
	
		.gnsx-xq .gnsx-xqli .zw {
			font-size: 16px;
		}
		/*.banner .banner-menu .banner-lis li {*/
		/*	font-size: 12px;*/
		/*}*/
		.banner-menu .banner-lis li {
		    line-height: 40px;
			margin: 0 3% !important;
			font-size: 12px;
		}
		.banner-menu .banner-lis li a {
            margin: 0px;
            padding: 0px;
        }
	}
	
	@media screen and (max-width: 1024px) {
		.banner-menu .banner-lis li {
		    /*line-height: 40px;*/
			margin: 0 26px 0px 6px;
			/*font-size: 14px;*/
		}
	
		/*.banner-menu .banner-lis li a {*/
		/*	padding: 0px;*/
		/*}*/
		/*.banner-lis a:hover {*/
		/*	background: none !important;*/
		/*}*/
	
		/*.banner-menu .banner-lis {*/
		/*	display: block;*/
		/*}*/
		.nwmd-xqtp .nwmd-xqle,.nwmd-xqtp .nwmd-xqlr{
			width: 100%;
		}
		.nwmd-xqtp  .nwmd-xqlr{
			padding-top: 15px;
		}
	}

</style>





<div class="banner">
        <div class="banner-img">
                            <img src="<?php echo $site['site_url'];?>common/static/image/2018072116514484.jpg">            <div class="banner-bg"></div>
        </div>
        <div class="banner-menu">
            <div class="container">
                <h3 class="menu-tit">加盟冠牛</h3>
                <ul class="banner-lis">
                                            <li>
                            <a href="<?php echo $site['site_url'];?>/pinpaiyoushi">品牌优势</a>
                        </li>
                                                <li class="on">
                            <a href="<?php echo $site['site_url'];?>/xingyeyoushi">行业优势</a>
                        </li>
                                                <li>
                            <a href="<?php echo $site['site_url'];?>/jiamengzhichi">加盟支持</a>
                        </li>
                                                <li>
                            <a href="<?php echo $site['site_url'];?>/jiamengliucheng">加盟流程</a>
                        </li>
                                                <li>
                            <a href="<?php echo $site['site_url'];?>/woyaojiameng">我要加盟</a>
                        </li>
                                        </ul>
            </div>
        </div>
    </div>


    <div class="nymain">
    <div class="nwmd-xq">
        <div class="container">
            <div class="tit-box">
                                    <h3 class="en">ABOUT CROWN COW</h3>
                    <h3 class="ch"><span>行业优势</span></h3>
                    
            </div>

            <div class="nwmd-xqcont">
                <div class="nwmd-xqtp">
                   <?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'pro_list')) {$data = $tag->pro_list(array('field'=>'title,url,description,pro_img','id'=>'232','limit'=>'1','page'=>'page',));$pages = $tag->pages();}?>
                    <?php if(is_array($data)) foreach($data as $v) { ?>
					<div class="nwmd-xqle">
						<img src="<?php echo $v['url'];?>" alt="">
					</div>
					<div class="nwmd-xqlr">
						<div class="nwmd-xqtxt">
							<p style="font-size:14px;line-height:2.5;font-family:SimSun;color:#666666;">
							   <?php echo $v['alt'];?> 
							</p>
						</div>
					</div>
					<?php } ?>
            </div>
        </div>
    </div>
</div>  



   <?php include template("index","footer"); ?>



    <script type="text/javascript">

    

    </script>





