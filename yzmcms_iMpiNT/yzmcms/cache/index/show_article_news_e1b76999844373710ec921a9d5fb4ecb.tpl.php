<?php defined('IN_YZMPHP') or exit('No permission resources.'); ?><?php include template("index","header"); ?>

<style>
img{
    max-width: 100%;
}
ul{
    padding: 0px;
    margin: 0px;
}
.zxxq-le img{
    height: auto;
}
.banner {
    width: 100%;
     /* height: 20rem; */
    position:relative;
    /*top: 4.1rem;*/
}
.banner img {
    width:100%;
    /*height: 16rem;*/
}
.banner-bg {
    position:absolute;
    left:0;
    top:0;
    width:100%;
    height: 100%;
    /*height:16rem;*/
    background:rgba(0, 0, 0, 0.3);
}
.banner-menu {
   position:absolute;
   transform: translateY(-50%);
   left:0;
   top:62%;
   width:100%;
   text-align:center;
   z-index: 888;
   font-size:0;
}
.banner-menu .menu-tit {
    font-size:28px;
    color:#fff;
    line-height:1.4em;
    padding-bottom:3.8%;
    font-weight:normal;
    letter-spacing:1px;
}
.banner-lis {
    display:inline-block;
    padding:0 50px 0 30px;
    -moz-border-radius:30px;
    -webkit-border-radius:30px;
    -ms-border-radius:30px;
    -o-border-radius:30px;
    background:rgba(255, 255, 255, 0.9);
    border-radius:30px;
}
.banner-lis li {
    display:inline-block;
    vertical-align:top;
    font-size:16px;
    line-height:62px;
    margin:0 26px;
}
.banner-lis li a {
    padding-left:20px;
    display:block;
    color:#555;
}
.banner-lis li.on a {
    background:url("<?php echo $site['site_url'];?>common/static/image/arr.png") no-repeat left center;
}
.banner-lis a:hover {
    text-decoration:none;
    background:url("<?php echo $site['site_url'];?>common/static/image/arr.png") no-repeat left center;
}
.banner-img {
	position: relative;
	margin-top: 4rem;
	width: 100%;
}



.zxxq-inn {
    margin:36px 0 60px;
    height: auto;
}
.zxxq-le {
    padding-right: 25px;
    border-right: 1px solid #f1f1f1
}
.zxxq-tit {
    font-size:30px;
    color:#555;
    line-height:1.9em;
    font-weight: normal;
    text-align:center;
}
.zxxq-tip {
    font-size:14px;
    color:#999;
    line-height:2.5em;
    text-align:center;
}
.zxxq-tip span {
    padding:0 12px;
    display: inline-block;
}
.zxxq-tip .icon {
    display:inline-block;
    width:19px;
    height:12px;
    background:url(<?php echo $site['site_url'];?>common/static/image/ico.png) no-repeat;
    vertical-align: middle;
    margin-right:8px;
}
.zxxq-icon {
    padding:20px 0 36px;
}
.zxxq-icon {
    text-align:center;
}
.zxxq-icon li {
    display:inline-block;
    vertical-align: middle;
    width:12px;
    margin:0 10px;
}

.zxxq-icon li .img2 {
    display: none;
}
.zxxq-icon li:hover .img1{
    display:none;
}
.zxxq-icon li:hover .img2 {
    display: inline-block;
}
.zxxq-txt {
    font-size:14px;
    color:#777;
    line-height:2em;
    padding-bottom:50px;
}
.zxxq-img {
    text-align:center;
}
.zxxq-box {
    padding-bottom:66px;
}
.qheh {
    font-size:14px;
    color:#444;
    line-height:1.8em;
    padding-top:26px;
    border-top:1px solid #f1f1f1;
}
.qheh a {
    color:#444;
}
.qheh .le {
    float:left;
    width:50%;
}
.qheh .lr  {
    float:left;
    width:50%;
}
.zxxq-inn{
    position: relative;
    display: flex;
}
.zxxq-lr {
    /*position: absolute;*/
    /*right: 0;*/
    /*top:0;*/
    padding-top:20px;
    padding-left: 25px;
    width:320px;
}
.zxxq-lr a:hover{
  text-decoration: none;
}
.zxxq-lrti {
    font-size:20px;
    color:#444;
    line-height: 1.5em;
    text-align:center;   
    font-weight: normal;
    text-align:center;
}
.zxxq-lrti span {
    padding-bottom:10px;
    display: inline-block;
    border-bottom:1px solid #cba580;
}
.zxxq-lrli {
    padding-top:36px;
}
.zxxq-lrli .li-img {
    border:1px solid #f1f1f1;
}
.zxxq-lrli p {
    font-size:14px;
    color:#444;
    line-height: 2em;
    padding:16px 0 36px;
}
.zxxq-bg {
    position: absolute;
    right: 354px;
    top:0;
    width:1px;
    height: 100%;
    background:#f1f1f1;
}
.zxxq {
    position: relative;
}
.zxxq .container {
   padding-top:1px;
   padding-bottom: 1px;
    position: relative;
}


.wmxq-qh {
    font-size: 18px;
    color:#555;
    line-height: 2em;
    padding:10px 25px;
    overflow: hidden;
}
.wmxq-qh .le,
.wmxq-qh .lr {
    float: left;
    width: 50%;
}
.wmxq-qh a {
    color:#555;
}
.wmxq-qh a:hover {
    color:#cba580;
    text-decoration: none;
}
.wmxq-tp {
    border-bottom:2px solid #f1f1f1;
}
.wmxq-min li{
    position: relative;
}
.wmxq-min li:before {
    content:"";
    position: absolute;
    left:0;
    top:0;
    width: 100%;
    height: 100%;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -ms-box-sizing: border-box;
    -o-box-sizing: border-box;
    box-sizing: border-box;
    border:3px solid #eb9030;
    display: none;
}
.wmxq-min li.cur::before{  
    display: block;
}
.wmxq-min ul {
    position: relative;
}
.nymain{
    position: relative;
    top: 0.5rem;
}
.li-img img{
    max-width: 100%;
}
	   @media screen and (max-width:1280px) {
	.banner .banner-img {
		margin-top: 2.6rem;
	}
}

	@media screen and (max-width: 1024px) {
	.wmxq-tple,
		.wmxq-tplr {
			width: 100%;
		}

		.wmxq-big {
			height: auto;
		}

		.wmxq-tplr {
			margin: 0px;
			padding: 0px;
		}
	}

	/* 手机 */
	@media screen and (max-width: 769px) {
	    .zxxq-inn{
	        flex-direction: column;
	    }
	    .zxxq-bg {
	        display: none;
	    }
		.zxxq-tit{
			font-size: 20px;
		}
		.zxxq{
			margin: 0px;
			border-bottom: 30px solid #f1f1f1;
		}
		.zxxq-le,.zxxq-lr{
			width: 100%;
			margin: 0px;
			padding: 0px;
			border:none;
			position: relative;
		}
		.zxxq-lrli{
			display: flex;
			flex-wrap: wrap;
			justify-content: space-between;
		}
		.zxxq-lrli li{
			width: 48%;
		}
		.zxxq-lrli p {
		    padding: 6px 0 26px;
			font-size: 12px;
			margin-bottom: 0px;
		}
		.tit-box .ch {
			font-size: 18px;
		}

		.tit-box .en {
			font-size: 20px;
			margin-bottom: 0;
		}

		.tit-box .ch {
			font-size: 18px;
			margin: 0px;
			padding: 0px;
		}

		.tit-box .en {
			font-size: 20px;
			margin-bottom: 0;
		}

		.menu-tit {
			display: none;
		}

		.banner-menu {
			border-bottom: 10px solid #f1f1f1;
		}

		.wmxq {
			padding: 5% 0 8.3%;
		}

		.price {
			padding: 10px 0;
		}

		.price-le {
			font-size: 12px;
		}

		.price-lr {
			font-size: 16px;
		}

		.banner .banner-img {
			width: 100%;
			height: auto;
			margin-top: 2.5rem;
		}

		.banner {
			height: auto !important;
		}

		.banner .banner-menu {
			transform: translateY(0);
			border-bottom: 10px solid #f1f1f1;
			position: static;
			top: 0;
		}

		.nwod .nwod-box {
			padding: 7% 0 8%;
		}

		.banner-menu .banner-lis {
			width: 100%;
			/* text-align: left; */
		}

		.banner-menu .banner-lis,
		.banner-menu .banner-lis li a {
			margin: 0px;
			padding: 0px;
		}

		.banner-menu .banner-lis li {
			line-height: 40px;
			margin: 0 3%;
			font-size: 12px;
		}

		.banner-menu .banner-lis li.on a {
			color: #eb9030;
			background: none;
		}

		.banner-lis a:hover {
			background: none !important;
		}

		.wmxq-qh {
			font-size: 14px;
			padding: 2% 4%;
		}
	}
</style>






 <div class="banner">
        <div class="banner-img">
                            <img src="<?php echo $site['site_url'];?>common/static/image/2018072116493186.jpg">            <div class="banner-bg"></div>
        </div>
        <div class="banner-menu">
            <div class="container">
                <h3 class="menu-tit">资讯中心</h3>
                <ul class="banner-lis">
                                            <li <?php if($catid==28) { ?>class="on"<?php } ?>>
                            <a href="<?php echo $site['site_url'];?>gongsixinwen">公司新闻</a>
                        </li>
                                                <li <?php if($catid==29) { ?>class="on"<?php } ?>>
                            <a href="<?php echo $site['site_url'];?>pinpaihuodong">品牌活动</a>
                        </li>
                                                <li <?php if($catid==30) { ?>class="on"<?php } ?>>
                            <a href="<?php echo $site['site_url'];?>xingyezixun">行业资讯</a>
                        </li>
                                        </ul>
            </div>
        </div>
  </div> 


    <div class="nymain">
        <div class="zxxq">
            <div class="container">
                <!--<div class="zxxq-bg"></div>-->
                <div class="zxxq-inn">
                                        <div class="zxxq-le">
                        <h3 class="zxxq-tit"><?php echo $title;?></h3>
                        <p class="zxxq-tip"><span>冠牛木门</span><span><?php echo date('Y/m/d H:i:s',$inputtime);?></span><span><i class="icon"></i>阅读 <?php echo $click;?></span></p>
                        <div class="zxxq-icon">
                            <div class="bdsharebuttonbox bdshare-button-style0-24" data-bd-bind="1678333892306">
                            <ul>

                                <script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"1","bdMiniList":false,"bdPic":"","bdStyle":"0","bdSize":"32"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='/static/api/js/share.js?v=89860593.js?cdnversion='+~(-new Date()/36e5)];</script>
                                <li>
                                    <a href="<?php echo $url;?>&amp;uid=277#" class="bds_more" data-cmd="more"></a>
                                </li>
                                <li>
                                    <a href="<?php echo $url;?>&amp;uid=277#" class="bds_weixin" data-cmd="weixin" title="分享到微信"></a>
                                </li>
                                <li>
                                    <a href="<?php echo $url;?>&amp;uid=277#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a>
                                </li>
                                <li>
                                    <a href="<?php echo $url;?>&amp;uid=277#" class="bds_sqq" data-cmd="sqq" title="分享到QQ好友"></a>
                                </li>
                                <li>
                                    <a href="<?php echo $url;?>&amp;uid=277#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a>
                                </li>
                            </ul></div>
                        </div>
                         <?php echo $content;?>

                                               <div class="wmxq-qh">
                                                            <span class="le">上一条：<?php echo $pre;?>  </span>                                                            <span class="lr">下一条：<?php echo $next;?> </span>
                                                        </div>
                    </div>
                                            <div class="zxxq-lr">
                        <h3 class="zxxq-lrti"><span>相关新闻</span></h3>
                        <ul class="zxxq-lrli">
                            <?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'lists')) {$data = $tag->lists(array('field'=>'title,url,thumb','catid'=>$catid,'limit'=>'3',));}?><!-- flag="4" -->
                            <?php if(is_array($data)) foreach($data as $v) { ?>
                                                        <li>
                                <a href="<?php echo $v['url'];?>">
                                    <div class="li-img">
                                        <img src="<?php echo get_thumb($v['thumb']);?>" alt="<?php echo $v['title'];?>">
                                    </div>
                                    <p><?php echo $v['title'];?> </p>
                                </a>
                            </li>
                            <?php } ?>

                                                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>





   <?php include template("index","footer"); ?>



    <script type="text/javascript">

    

    </script>





