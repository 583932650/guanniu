<?php defined('IN_YZMPHP') or exit('No permission resources.'); ?><?php include template("index","header"); ?>
<style>
	   @media screen and (max-width:1280px) {
	.banner .banner-img {
		margin-top: 2.6rem;
	}
}
/* 手机 */
@media screen and (max-width: 769px) { 
     .banner-menu .banner-lis li.on a {
        color: #eb9030;
        background: none;
    }
	.menu-tit {
		display: none;
	}

	.banner .banner-img {
		width: 100%;
		height: auto;
		margin-top: 2.5rem;
	}

	.banner {
		height: auto !important;
	}

	.banner .banner-menu { transform: translateY(0); border-bottom: 10px solid #f1f1f1;
		position: static;
		top: 0;
	}

	.nwod .nwod-box {
		padding: 7% 0 8%;
	}

	.banner-menu .banner-lis {
		width: 100%;
		text-align: left;
	}

	.banner-menu .banner-lis,
	.banner-menu .banner-lis li a {
		margin: 0px;
		padding: 0px;
	}

	.banner-menu .banner-lis li {line-height: 40px;
		margin: 0 3%;
		font-size: 12px;
	}
	.gnsx-xq .gnsx-xqli li{
		width: 100%;
		text-align: center;
	}
	.gnsx-xq .gnsx-xqli li .li-tit{
		padding-top: 10px;
		font-size: 16px;
	}
	.gnsx-xqli .li-img{
		width: 60%;
		margin: auto;
	}
	.gnsx-xq .gnsx-xqli .zw {
		font-size: 16px;
	}
	.banner-lis a:hover {
		background: none !important;
	}
}
	.gj-page .curpage{
			    background: #333;
	            color: #fff;
	}
.gj-page{
	text-align: center;
 /*   position: relative;
    left: 8rem; */
}
.gj-page .pageinfo{
  
    margin: 0px 10px;
    color: #808080;
    font-size: 14px;
    padding: 6px 15px;
    border: 1px solid #aaa;


}
.gj-page a {
    border: 1px solid #aaa;
    background: #fff;
    color: #808080;
    padding: 6px 15px;
    margin-left: 5px;
    transition: all 0.2s;
    font-size: 14px;
}
.gj-page a:hover{
    background: #333;
    color: #fff;
    text-decoration: none;
    border: 1px solid #333;
}
.banner {
    position:relative;
     /* height: 20rem; */
}
.banner img {
    width:100%;
    height: 100%;
}
.banner-bg {
    position:absolute;
    left:0;
    top:0;
    width:100%;
    height:100%;
    background:rgba(0, 0, 0, 0.3);
}
.banner-menu {
   position:absolute;
   transform: translateY(-50%);
   left:0;
   top:62%;
   width:100%;
   text-align:center;
   z-index: 888;
   font-size:0;
}
.banner-menu .menu-tit {
    font-size:28px;
    color:#fff;
    line-height:1.4em;
    padding-bottom:3.8%;
    font-weight:normal;
    letter-spacing:1px;
}
.banner-lis {
    display:inline-block;
    padding:0 50px 0 30px;
    -moz-border-radius:30px;
    -webkit-border-radius:30px;
    -ms-border-radius:30px;
    -o-border-radius:30px;
    background:rgba(255, 255, 255, 0.9);
    border-radius:30px;
}
.banner-lis li {
    display:inline-block;
    vertical-align:top;
    font-size:16px;
    line-height:62px;
    margin:0 26px;
}
.banner-lis li a {
    padding-left:20px;
    display:block;
    color:#555;
}
.banner-lis li.on a {
    background:url("<?php echo $site['site_url'];?>common/static/image/arr.png") no-repeat left center;
}
.banner-lis a:hover {
    text-decoration:none;
    background:url("<?php echo $site['site_url'];?>common/static/image/arr.png") no-repeat left center;
}
.banner-img {
	position: relative;
	margin-top: 4rem;
	width: 100%;
}



.zxzx {
    background:#f1f1f1;
    padding:60px 0;
}
.zxzx-list li {
    position:relative;
    margin-bottom:30px;
}
.zxzx-list .li-img {
    width:47.5%;
}
.zxzx-list .li-con {
    -o-box-sizing:border-box;
    -ms-box-sizing:border-box;
    -webkit-box-sizing:border-box;
    -moz-box-sizing:border-box;
    box-sizing:border-box;
    padding:1% 3.3%;
    font-size:0;
    position:absolute;
     width:15.6rem;
    /*width:52.5%;*/
    top:0;
    height:100%;
    background:#fff;
}
.zxzx-list li:nth-child(2n+1) .li-con {
    left:47.5%;
}
.zxzx-list li .li-con:before {
    content: "";
    display:inline-block;
    vertical-align:middle;
    height:100%;
    width:0;
}
.zxzx-list .li-time {
    font-size:14px;
    color:#777;
    line-height:2.3em;
}
.zxzx-list .li-tit {
    font-size:18px;
    color:#555;
    line-height:2em;
    font-weight:normal;
    padding-bottom:16px;
}
.zxzx-list .li-txt {
    font-size:14px;
    color:#777;
    line-height:2em;
}
.zxzx-list .li-inn {
    display:inline-block;
    vertical-align: middle;
}
.zxzx-list li:hover .li-tit {
    color:#eb9030;
}
.zxzx-list li:nth-child(2n) .li-img {
    padding-left:52.5%;
}
.zxzx-list li:nth-child(2n) .li-con {
    left:0;
}
.zxzx-list .li-arr {
    top:11.5%;
    position:absolute;
    width:0;
    height:0;
    border-top:24px solid transparent;
    border-bottom:24px solid transparent;
}
.zxzx-list li:nth-child(2n+1) .li-arr {
    border-right:28px solid #fff;
    right:100%;
}
.zxzx-list li:nth-child(2n) .li-arr {
    left:100%;
    border-left:28px solid #fff;
}
.zxzx-list {
    padding-bottom:20px;
}
.fenye{width: 55%;text-align: center;margin: 0 auto;padding: 80px 0;}
.fenye li{
    display: inline;
    margin:5px;
}

.gnsx-xqli .li-tit {
    font-size:22px;
    color:#555;
    line-height:1.6em;
    text-align: center;
    font-weight: normal;
    padding-top:20px;
}
.gnsx-xqli .zw {
    font-size:18px;
    color:#9a7241;
    line-height:2em;
}
.gnsx-xqli .li-txt {
    font-size:14px;
    color:#777;
    line-height:1.8em;
    padding-top:16px;
}
.gnsx-xqli .li-xq {
    position: absolute;
    left: 0;
    top:0;
    width: 100%;
    height: 100%;
    padding:10px;
    -moz-box-sizing:border-box;
    -webkit-box-sizing:border-box;
    -ms-box-sizing:border-box;
    -o-box-sizing:border-box;
    box-sizing:border-box;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;
    border-radius: 50%;
    overflow: hidden;
    display: none;
}

.gnsx-xqli .li-xqbg {
/*    background: #eb9030;*/
    width: 100%;
    height: 100%;
/*    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    -ms-border-radius: 50%;
    -o-border-radius: 50%;*/
/*    border-radius: 50%;*/
    position: relative;
}
.gnsx-xqli .li-xqin {
    position: absolute;
    left:0;
    top:50%;
    -moz-transform: translateY(-50%);;
    -ms-transform: translateY(-50%);
    -webkit-transform: translateY(-50%);
    -o-transform: translateY(-50%);
    transform: translateY(-50%);
    width:100%;
}
.gnsx-xqli .li-fd {
    width:30px;
    height:30px;
    margin:0 auto;
    margin-bottom:7.8%;
}
.gnsx-xqli .li-xqin p {
    font-size:20px;
    color:#fff;
    line-height:2.2em;
    text-align: center;
    padding-top:8px;
}
.gnsx-xqli .li-x {
    width: 73%;
    height: 1px;
    margin: 0 auto;
    background:#fff;
    opacity: 0.5;
}
.gnsx-xqli  {
    font-size:0;
}
.gnsx-xqli li a:hover .li-xq {
    display: block;
}
.gnsx-xqli  a:hover  {
    text-decoration: none;
}


.gnsx-xqli  {
    padding:82px 0 120px;
}
.gnsx-xqli li {
    display: inline-block;
    vertical-align: middle;
    text-align: center;
    width:50%;
    -moz-box-sizing:border-box;
    -webkit-box-sizing:border-box;
    -ms-box-sizing:border-box;
    -o-box-sizing:border-box;
    box-sizing:border-box;
    padding:0 30px;
}
.gnsx-xqli .li-img {
    /*width:60%;*/
    margin:0 auto;
    position: relative;
}
.gnsx-xqli .li-img img {
    max-width:100%;
}

</style>





 <div class="banner">
        <div class="banner-img">
                            <img src="<?php echo $site['site_url'];?>common/static/image//2018072116510735.jpg">            <div class="banner-bg"></div>
        </div>
        <div class="banner-menu">
            <div class="container">
                <h3 class="menu-tit">冠牛商学院</h3>
                <ul class="banner-lis">
                	<li>
                		<a href="<?php echo $site['site_url'];?>shangxueyuan">商学院</a>
                	</li>
                	<li>
                		<a href="<?php echo $site['site_url'];?>youxiuyuangong">优秀员工</a>
                	</li>
                	<li>
                		<a href="<?php echo $site['site_url'];?>zhimingjiangshi">知名讲师</a>
                	</li>
                	<li class="on">
                		<a href="<?php echo $site['site_url'];?>xueyuanfengcai">学员风采</a>
                	</li>
                </ul>
            </div>
        </div>
  </div> 

    <div class="nymain">
        <div class="gnsx-xq">
            <div class="container">
                <ul class="gnsx-xqli">
                    <?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'lists')) {$data = $tag->lists(array('field'=>'title,thumb,url,description,inputtime,click,catid','modelid'=>'1','catid'=>$catid,'limit'=>'8','page'=>'page','order'=>'id asc',));$pages = $tag->pages();}?>
                    <?php if(is_array($data)) foreach($data as $v) { ?>
                                        <li>
                        <a href="<?php echo $v['url'];?>">
                            <div class="li-img">
                                <img src="<?php echo $v['thumb'];?>" alt="">
                                <div class="li-xq">
                                    <div class="li-xqbg">
                                        <!-- <div class="li-xqin">
                                            <div class="li-fd">
                                                <img src="images/fd.png" alt="">
                                            </div>
                                            <div class="li-x"></div>
                                            <p>查看详情</p>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <h3 class="li-tit"><?php echo $v['title'];?></h3>
                            <!--<p class="zw"><?php echo $v['description'];?></p>-->
                            <div class="li-txt">
                                <p></p>
                            </div>
                        </a>
                    </li>
                     <?php } ?>  
                </ul>
                 <nav class="gj-page">
                        <?php echo $pages;?>
                    </nav>
            </div>
        </div>
    </div>

    


   <?php include template("index","footer"); ?>



    <script type="text/javascript">

    

    </script>





