<?php defined('IN_YZMPHP') or exit('No permission resources.'); ?><?php include template("index","header"); ?>
<style>
	.main {
		/*height: 18rem;*/
	}
    .ijia-leim img{
        width: 100%;
    }
    body, h1, h2, h3, h4, h5, p, dl, dd, ul, ol, form, input, textarea, th, td, select {
        margin: 0;
        padding: 0;
    }
	.swiper {
		position: relative;
		top: 2.5rem;
		width: 100%;
		/* height: 18rem; */
	}

	.swiper .swiper-slide {
		text-align: center;
		font-size: 18px;
		background: #000;

		/* Center slide text vertically */
		display: -webkit-box;
		display: -ms-flexbox;
		display: -webkit-flex;
		display: flex;
		-webkit-box-pack: center;
		-ms-flex-pack: center;
		-webkit-justify-content: center;
		justify-content: center;
		-webkit-box-align: center;
		-ms-flex-align: center;
		-webkit-align-items: center;
		align-items: center;
	}

	.swiper .swiper-slide img {
		display: block;
		width: 100%;
		/* height: 18rem; */
		object-fit: cover;
	}

	#dot {
		position: absolute;
		width: 100%;
		text-align: center;
		left: 0;
		top: 16rem;
		bottom: 38px;
		z-index: 1100;
	}

	#dot li {
		display: inline-block;
		vertical-align: top;
		width: 30px;
		height: 5px;
		background: #eee;
		border-radius: 12px;
		margin: 0 4px;
		cursor: pointer;
		position: relative;
	}

	#dot li.cur {
		width: 60px;
		background: #cca581;
	}

	.swiper-pagination {
		position: relative;
		top: -1.5rem;
	}

	.swiper-pagination .swiper-pagination-bullet {
		display: inline-block;
		vertical-align: top;
		width: 30px;
		height: 5px;
		/*background:#eee;*/
		border-radius: 12px;
		margin: 0 4px;
		cursor: pointer;
		position: relative;
		outline: none;
	}

	.swiper-pagination .swiper-pagination-bullet-active {
		width: 60px;
		background: #cca581;
		outline: none;
	}


	.wmxq {
		padding: 60px 0 100px;
		background: #f1f1f1;
	}

	.wmxq-cont {
		margin-top: 36px;
		background: #fff;
	}

	.wmxq-tp {
		padding: 25px;
		overflow: hidden;
	}

	.wmxq-big li.show {
		display: block;
		opacity: 1;
	}

	.wmxq-big li {
		display: none;
		opacity: 0;
	}

	.wmxq-tple {
		width: 70%;
		float: right;
	}

	.wmxq-min {
		padding: 20px 0 6px;
		overflow: hidden;
	}

	.wmxq-min li {
		float: left;
		margin-right: 20px;
		cursor: pointer;
	}

	.wmxq-min ul {
		width: 1200px;
	}

	.wmxq-tplr {
		width: 26.6%;
		float: left;
		padding-top: 26px;
	}

	.wmxq-tit {
		font-size: 24px;
		color: #555;
		line-height: 2em;
		font-weight: normal;
		padding-bottom: 16px;
	}

	.wmxq-tip {
		font-size: 16px;
		color: #555;
		line-height: 1.9em;
		padding-bottom: 10px;
	}

	.wmxq-txt {
		font-size: 12px;
		color: #777;
		line-height: 2em;
	}

	.wmxq-txt p {
		padding-bottom: 20px;
	}

	.wmxq-qh {
		font-size: 18px;
		color: #555;
		line-height: 2em;
		padding: 10px 25px;
		overflow: hidden;
	}

	.wmxq-qh .le,
	.wmxq-qh .lr {
		float: left;
		width: 50%;
	}

	.wmxq-qh a {
		color: #555;
	}

	.wmxq-qh a:hover {
		color: #cba580;
		text-decoration: none;
	}

	.wmxq-tp {
		border-bottom: 2px solid #f1f1f1;
	}

	.wmxq-min li {
		position: relative;
	}

	.wmxq-min li:before {
		content: "";
		position: absolute;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		-ms-box-sizing: border-box;
		-o-box-sizing: border-box;
		box-sizing: border-box;
		border: 3px solid #eb9030;
		display: none;
	}

	.wmxq-min li.cur::before {
		display: block;
	}

	.wmxq-min ul {
		position: relative;
	}

	.ipro {
		position: relative;
		top: 3rem;
		/*padding:54px 0 80px;*/
	}

	.ipro-txt {
		font-size: 12px;
		color: #777;
		line-height: 2em;
		padding: 18px 0;
	}

	.ipro-list {
		position: relative;
		right: 0.5rem;
		text-align: center;
		font-size: 0;
		margin-bottom: 30px;
		padding: 20px 0;
	}

	.ipro-list li {
		display: inline-block;
		vertical-align: middle;
		width: 74px;
		margin: 0 32px;
		cursor: pointer;
	}

	.ipro-list li .img2 {
		display: none;
	}

	.ipro-list img {
		max-width: 100%;
	}

	.ipro-list li:hover .img1 {
		display: none;
	}

	.ipro-list li:hover .img2 {
		display: inline-block;
	}

	.ipro-list li:hover {
		-moz-transition: all .5s;
		-ms-transition: all .5s;
		-webkit-transition: all .5s;
		-o-transition: all .5s;
		transition: all .5s;
		-moz-transform: scale(1.3);
		-webkit-transform: scale(1.3);
		-ms-transform: scale(1.3);
		-o-transform: scale(1.3);
		transform: scale(1.3);
	}

	.ipro-img li {
		float: left;
		width: 33.3%;
		-ms-box-sizing: border-box;
		-o-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		box-sizing: border-box;
		-moz-transition: all .5s;
		-webkit-transition: all .5s;
		-ms-transition: all .5s;
		-o-transition: all .5s;
		transition: all .5s;
	}

	.ipro-img li .ipro-ibo {
		margin: 0 12px;
		width: 100%;
	}

	.ipro-box {
		overflow: hidden;
	}

	.tieti {
		width: 10rem;
		/* position: relative;
    top: 2rem;
    left: -2rem;*/
		font-size: 18px;
		color: #555;
		line-height: 2em;
		padding: 14px 0;
		text-align: center;
		background: #f1f1f1;
	}

	.ipro-box .li-img {
		position: relative;
	}
	
	.ipro-img {
		padding-bottom: 10px;
	}

	.tit-box {
		text-align: center;
	}

	.tit-box .en {
		font-size: 26px;
		color: #ccc;
		line-height: 1.3em;
		font-weight: normal;
		font-family: font;
		text-transform: uppercase;
	}

	.tit-box .ch {
		font-size: 24px;
		color: #555;
		line-height: 1.75em;
		font-weight: normal;
	}

	.tit-box .ch b {
		font-weight: normal;
		color: #cca581;
	}

	.tit-box span {
		display: inline-block;
		padding: 0 40px;
		position: relative;
	}

	.tit-box span:before,
	.tit-box span:after {
		content: "";
		position: absolute;
		top: 50%;
		width: 15vw;
		height: 1px;
		background: #eaeaea;
	}

	.tit-box span:before {
		left: 100%;
	}

	.tit-box span:after {
		right: 100%;
	}

	.ipro-cont img {
		max-width: 100%;
	}

	.swiper1 {
		width: 92vw;
		/* height: 10rem; */
		margin: auto;
		overflow: hidden;
	}

	.swiper1 .swiper-slide {
		/* max-width: 13rem; */
		overflow: hidden;
		text-align: center;
		font-size: 18px;
		background: #fff;
		/* width: 33%; */
		/* Center slide text vertically */
		display: -webkit-box;
		display: -ms-flexbox;
		display: -webkit-flex;
		display: flex;
		-webkit-box-pack: center;
		-ms-flex-pack: center;
		-webkit-justify-content: center;
		justify-content: center;
		-webkit-box-align: center;
		-ms-flex-align: center;
		-webkit-align-items: center;
		align-items: center;
	}

/* 	@media (max-width: 760px) {
		.swiper-button-next {
			right: 20px;
			transform: rotate(90deg);
		}

		.swiper-button-prev {
			left: 20px;
			transform: rotate(90deg);
		}
	} */

	.swiper1 img {
		width: 100%;
		height: 6rem;
		overflow: hidden;
	}

	.swiper1 .li-xq {
		/*display: none;*/
		position: absolute;
		left: 0rem;
		top: -101%;
		width: 100%;
		height: 100%;
		background: rgba(255, 255, 255, 0.8);
		text-align: center;
		-moz-transition: all .5s;
		-webkit-transition: all .5s;
		-ms-transition: all .5s;
		-o-transition: all .5s;
		transition: all .5s;
	}

	.swiper1 .li-btn {
		position: absolute;
		left: 0;
		top: 4rem;
		width: 100%;
		text-align: center;
		-moz-transform: translateY(-50%);
		-webkit-transform: translateY(-50%);
		-ms-transform: translateY(-50%);
		-o-transform: translateY(-50%);
		transform: translateY(-50%);
	}

	.swiper1 .li-btn a {
		display: inline-block;
		vertical-align: top;
		padding: 0 30px;
		font-size: 14px;
		color: #fff;
		line-height: 50px;
		-moz-border-radius: 4px;
		-ms-border-radius: 4px;
		-webkit-border-radius: 4px;
		-o-border-radius: 4px;
		border-radius: 4px;
	}

	.swiper1 .li-btn .btn1 {
		background: #eb9030;
		margin-right: 26px;
	}

	.swiper1 .li-btn .btn2 {
		background: #555;
	}

	.swiper1 .ipro-ibo:hover .li-xq {
		/* display: block;*/
		top: -1rem;
	}

	.swiper1 .ipro-ibo:hover p {
		background: #fff;
	}

	.swiper1 .ipro-ibo:hover {
		box-shadow: 2px 2px 10px rgba(221, 221, 221, 0.75)
	}

	.swiper1 .ipro-ibo:nth-child(3n) {
		margin-right: 0;
	}

	.ipro-img li {
		float: left;
	}

	.ipro-dot {
		/* position: relative; */
		display: flex;
		margin: 0 10px;
		/* padding-left: 17.8rem; */
		/* top: -1rem; */
		/* display: inline-block;
    vertical-align: middle;*/
	}

	.ipro-qh {
		padding-top: 40px;
		padding-left: 11rem;
		height: 10rem;
		/* display: inline-block;
    vertical-align: middle;*/
	}

	.swiper1 .swiper-slide {}

	.overs {
		width: 100%;
		height: 14rem;
		overflow-x: hidden;
	}

	/*去掉默认样式*/
	.swiper-button-prev {
		/* position: relative;*/
		/*top: 5rem;*/
	}

	.swiper-button-next {
		/*position: relative;*/
	}

	/*自定义样式*/
	/*.swiper-button-prev{
    width: 54px;
    height: 54px;
    background: url('common/static/image/left-arrow.svg');
    left: 64px;
}
 
.swiper-button-next{
    width: 54px;
    height: 54px;
    background:url('common/static/image/right-arrow.svg');
    right: 64px;
}*/
	.prev,
	.next {
		cursor: pointer;
		width: 60px;
		height: 30px;
		border: 1px solid #777;
		-moz-border-radius: 16px;
		-webkit-border-radius: 16px;
		-ms-border-radius: 16px;
		-o-border-radius: 16px;
		border-radius: 16px;
		font-size: 24px;
		color: #777;
		line-height: 30px;
		text-align: center;
		margin: 0 8px;
		font-weight: normal;
		font-family: "宋体";
		z-index: 888;
	}

	.next {
		position: relative;
		/* left: 20rem; */
		/* top: -2rem; */
		outline: none;
	}

	.prev {
		position: relative;
		/* left: 16rem; */
		/* top: 0.15rem; */
		outline: none;
	}

	.prev:hover,
	.next:hover {
		background: #cca581;
		border: 1px solid #cca581;
		color: #fff;
	}

	.page a.num.on,
	.page a.num:hover,
	.ipro-dot .swiper-pagination-bullet-active1,
	.ipro-dot li:hover,
	.fwxq-lrbt .dot li.on {
		background: url(common/static/image/dot.png) no-repeat center center;
		background-size: 100% 100%;
	}

	.swiper-pagination-clickable .swiper-pagination-bullet1 {
		cursor: pointer;
	}

	.page a.num,
	.ipro-dot span,
	.fwxq-lrbt .dot li {
		width: 16px;
		height: 16px;
		background: url(common/static/image/dot.png) no-repeat center center;
		background-size: 100%;
		margin: 0 8px;
		cursor: pointer;
		opacity: 1;
	}
	.swiper-pagination-bullet-active{
		background: url(common/static/image/dotc.png) no-repeat center center !important;
	}
	.swiper-pagination-bullet-active1 {
		opacity: 1;
		background: #007aff;
	}

	.swiper-pagination-bullet1 {
		width: 8px;
		height: 8px;
		display: inline-block;
		border-radius: 100%;
		background: #000;
		opacity: .2;
	}

	.iwmd {
		width: 100%;
		background: #e3e3e3;
		padding: 64px 0 30px;
		position: relative;
		top: -3rem;
	}

	.iwmd-cont {
		padding-top: 36px;
	}

	.iwmd-tp {
		overflow: hidden;
		position: relative;
		margin-bottom: 20px;
		display: flex;
		flex-wrap: wrap;
	}

	.iwmd-tple {
		width: 66%;
		overflow: hidden;
	}

	.iwmd-tplr {
		width: 32%;
		height: 486px;
		/* position: absolute; */
		/* right: -0.38rem; */
		top: 0;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		-ms-box-sizing: border-box;
		-o-box-sizing: border-box;
		box-sizing: border-box;
		background: #fff;
		/* height: 100%; */
		padding: 3.5%;
		border: 1px solid #cdcccc;
	}

	.iwmd-tit {
		font-size: 24px;
		color: #555;
		line-height: 1.7em;
		padding-top: 15px;
		background: url(common/static/image/qp.jpg) no-repeat left top;
		font-weight: normal;
	}

	.iwmd-txt {
		padding-top: 20px;
		font-size: 14px;
		color: #777;
		line-height: 1.8em;
	}

	.iwmd-inn {
		height: 100%;
	}

	.iwmd-more {
		/* position:absolute; */
		/*  left:0;
    bottom:0; */
		font-size: 14px;
		color: #fff;
		line-height: 3.5em;
		-moz-border-radius: 6px;
		-ms-border-radius: 6px;
		-webkit-border-radius: 6px;
		-o-border-radius: 6px;
		border-radius: 6px;
		background: #cca581;
		padding: 10px 36px;
	}

	.iwmd-blis {
		font-size: 0;
	}

	.iwmd-blis ul {
		display: flex;
		flex-wrap: wrap;
	}

	.iwmd-blis li {
		/*display:inline-block;*/

		vertical-align: top;
		width: 31.4%;
		height: 6rem;
		margin-right: 1.85%;
		background: #fff;
		border: 1px solid #cdcccc;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		-ms-box-sizing: border-box;
		-o-box-sizing: border-box;
		box-sizing: border-box;
		text-align: center;
	}

	.iwmd-blis li:nth-child(3n) {
		margin-right: 0;
	}

	.iwmd-blis .li-img {
		width: 100%;
		margin: 0 auto;
	}

	.iwmd-blis .img img {
		height: 6rem;

	}

	.iwmd-blis .text p {
		font-size: 20px;
		color: #555;
		line-height: 2em;
		padding-top: 16px;
	}

	.izxz {
		padding: 80px 0 40px;
	}

	.izxz-list li {
		display: inline-block;
		vertical-align: middle;
	}

	.izxz-list {
		font-size: 0;
		text-align: center;
	}

	.izxz-list li {
		width: 160px;
		height: 160px;
		border: 1px solid #eee;
		-moz-border-radius: 50%;
		-webkit-border-radius: 50%;
		-ms-border-radius: 50%;
		-o-border-radius: 50%;
		border-radius: 50%;
		font-size: 0;
		cursor: pointer;
		margin: 0 12px;
		overflow: hidden;
	}

	.izxz-list li a {
		display: block;
		height: 100%;
	}

	.izxz-list li a:before {
		content: "";
		display: inline-block;
		height: 100%;
		width: 0;
		vertical-align: middle;
	}

	.izxz-list li .box {
		display: inline-block;
		vertical-align: middle;
	}

	.izxz-list .img2 {
		display: none;
	}

	.izxz-list .li-img {
		width: 38px;
		margin: 0 auto;
	}
	.izxz-list .li-img img{
		width: 100%;
	}

	.izxz-list p {
		font-size: 16px;
		color: #555;
		line-height: 1.6em;
		text-align: center;
		padding-top: 6px;
	}

	.izxz-list {
		padding: 22px 0 30px;
	}

	.izxz-list li a:hover .img2 {
		display: inline-block;
		text-decoration: none;
	}

	.izxz-list li a:hover .img1 {
		display: none;
		text-decoration: none;
	}

	.izxz-list li a:hover p {
		color: #eb9030;
		text-decoration: none;
	}

	.izxz-qh li {
		position: relative;
		overflow: hidden;
	}

	.izxz-qh .li-con {
		background: #f8f8f8;
		padding: 50px 538px 74px 36px;
		position: absolute;
		left: 0;
		top: 0;
		height: 100%;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		-ms-box-sizing: border-box;
		-o-box-sizing: border-box;
		box-sizing: border-box;
	}

	.izxz-qh .li-img {
		float: right;
		position: relative;
		z-index: 666;
	}

	.izxz-qh .li-tit {
		font-size: 20px;
		color: #555;
		line-height: 1.75em;
		font-weight: normal;
	}

	.izxz-qh .li-txt {
		font-size: 14px;
		color: #777;
		line-height: 1.9em;
		padding-top: 22px;
	}

	.izxz-btli a:hover {
		text-decoration: none;
	}

	.izxz-dot {
		position: absolute;
		left: 22.6%;
		bottom: 50px;
	}

	.izxz-dot li {
		display: inline-block;
		vertical-align: middle;
		width: 20px;
		height: 2px;
		background: #777;
		margin-right: 10px;
	}

	.izxz-dot li.on {
		width: 30px;
		height: 2px;
		background: #cca581;
	}

	.izxz-tp {
		margin-bottom: 52px;
	}

	.izxz-btli li {
		float: left;
		width: 32.1%;
		margin-right: 1.85%;
		background: #f8f8f8;
		-moz-box-sizing: border-box;
		-ms-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		-o-box-sizing: border-box;
		box-sizing: border-box;
		padding: 0 22px 0 28px;
		box-shadow: 0 7px 10px rgba(221, 221, 221, 0.75);
	}

	.izxz-btli li:nth-child(3n) {
		margin-right: 0;
	}

	.izxz-btli .li-time {
		background: #999;
		margin-top: -28px;
		padding: 8px 12px;
		display: inline-block;
		text-align: center;
		color: #fff;
		-moz-transition: all .5s;
		-webkit-transition: all .5s;
		-ms-transition: all .5s;
		-o-transition: all .5s;
		transition: all .5s;
		font-weight: normal;
	}

	.izxz-btli .li-time .t1 {
		font-size: 24px;
		line-height: 1.75em;
		border-bottom: 1px solid #b8b8b8;
		font-weight: normal;
	}

	.izxz-btli .li-time .t2 {
		font-size: 14px;
		line-height: 2.5em;
	}

	.izxz-btli .li-con {
		padding-top: 24px;
		padding-bottom: 20px;
	}

	.izxz-btli .li-tit {
		font-size: 16px;
		color: #555;
		line-height: 2em;
		font-weight: normal;
		-moz-transition: all .5s;
		-webkit-transition: all .5s;
		-ms-transition: all .5s;
		-o-transition: all .5s;
		transition: all .5s;
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
	}

	.izxz-btli .li-txt {
		font-size: 12px;
		color: #777;
		line-height: 1.8em;
		padding-top: 12px;
		height: 90px;
		overflow: hidden;
		box-sizing: content-box;
	}

	.izxz-btli a:hover .li-tit {
		color: #eb9030;
		text-decoration: none;
	}

	.izxz-btli a:hover .li-time {
		background: #eb9030;
		text-decoration: none;
	}

	.ijia {
		padding: 38px 0 0;
	}

	.ijia-cont {
		margin-top: 40px;
		padding: 54px 0;
		background: url(common/static/image/jia.jpg) no-repeat left top #333;
		background-size: 52.7% 100%;
	}

	.ijia-le {
		width: 50%;
		background: #fff;
		position: relative;
		float: left;
	}

	.ijia-leim {
		width: 50%;
	}

	.ijia-leco {
		position: absolute;
		left: 50%;
		top: 0;
		height: 100%;
		width: 50%;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		-ms-box-sizing: border-box;
		-o-box-sizing: border-box;
		box-sizing: border-box;
		padding: 36px 28px 0;
		background: #fff;
	}

	.ijia-leco .li-tit {
		font-size: 24px;
		color: #cca581;
		line-height: 2.3em;
		font-weight: normal;
		border-bottom: 1px dashed #ddd;
	}

	.ijia-leco .li-txt {
		/* padding-top: 14px; */
		font-size: 14px;
		color: #777;
		line-height: 2em;
	}

	.ijia-lr {
		padding-top: 72px;
	}

	.ijia-lr li {
		width: 31%;
		padding-bottom: 31%;
		background: url(common/static/image/bg1.png) no-repeat;
		background-size: 100% 100%;
		font-size: 0;
		cursor: pointer;
		position: relative;
	}

	.ijia-lr .wz {
		padding: 0 30%;
		text-align: center;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		-ms-box-sizing: border-box;
		-o-box-sizing: border-box;
		box-sizing: border-box;
		position: absolute;
		left: 0;
		top: 50%;
		-moz-transform: translateY(-50%);
		-webkit-transform: translateY(-50%);
		-ms-transform: translateY(-50%);
		-o-transform: translateY(-50%);
		transform: translateY(-50%);
		font-size: 18px;
		color: #555;
		line-height: 1.3em;
		width: 100%;
	}

	.ijia-lr li:before {
		content: "";
		display: inline-block;
		vertical-align: middle;
		width: 0;
		height: 100%;
	}

	.ijia-lr {
		width: 40.8%;
		float: right;
	}

	.ijia-lr li {
		position: absolute;
		text-align: center;
	}

	.ijia-lr .li0 {
		left: 0;
		top: 0;
	}

	.ijia-lr .li1 {
		left: 34.7%;
		top: 0;
	}

	.ijia-lr .li2 {
		right: 0;
		top: 0;
	}

	.ijia-lrli {
		position: relative;
	}

	.ijia-lr .li3 {
		top: 120px;
		left: 17.7%;
	}

	.ijia-lr .li4 {
		top: 120px;
		right: 17.7%;
	}

	.ijia-lr li.on {
		background: url(common/static/image/bg2.png) no-repeat;
		color: #fff;
		background-size: 100% 100%;
	}

	.ijia-lr li.on .wz {
		color: #fff;
	}

	.icxf {
		padding-top: 80px;
	}

	.icxf-cont {
		margin-top: 40px;
		padding: 86px 0 86px;
		background: url(common/static/image/cxf.jpg) no-repeat;
		background-size: cover;
	}

	.icxf-list {
		font-size: 0;
	}

	.icxf-list li {
		display: inline-block;
		width: 20%;
		padding: 0 4%;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		-ms-box-sizing: border-box;
		-o-box-sizing: border-box;
		box-sizing: border-box;
		text-align: center;
		vertical-align: top;
	}

	.icxf-list .li-img {
		-moz-border-radius: 50%;
		-webkit-border-radius: 50%;
		-ms-border-radius: 50%;
		-o-border-radius: 50%;
		border-radius: 50%;
		background: rgba(255, 255, 255, 0.3);
		padding: 9px;
		overflow: hidden;
		max-width: 20rem;
		margin: 0 auto;
	}

	.icxf-list .img-inn {
		-moz-border-radius: 50%;
		-webkit-border-radius: 50%;
		-ms-border-radius: 50%;
		-o-border-radius: 50%;
		border-radius: 50%;
		background: #f8f8f8;
	}

	.img-inn img {
		max-width: 100%;
	}

	.icxf-list p {
		font-size: 18px;
		color: #fff;
		line-height: 1.8em;
		padding-top: 16px;
	}

	.izxz-qh li {
		display: none;
		opacity: 0;
	}

	.izxz-qh li.on {
		display: block;
		opacity: 1;
	}

	.izxz-tp {
		position: relative;
	}

	.izxz-dot {
		z-index: 888;
	}

	.ijia-leim {
		overflow: hidden;
	}

	.ijia-leim li {
		float: left;
	}

	.ijia-leim ul {
		width: 50000px;
		position: relative;
	}

	.ijia-leco li {
		opacity: 0;
		display: none;
	}

	.ijia-leco li.on {
		opacity: 1;
		display: block;
	}

	.ijia-leim li:hover,
	.izxz-qh li .li-img:hover,
	.iwmd-tple:hover,
	.ipro-box li:hover .li-img {
		filter: alpha(opacity=70);
		opacity: 0.7;
		-webkit-transition: all 0.5s ease;
		-moz-transition: all 0.5s ease;
		-ms-transition: all 0.5s ease;
		-o-transition: all 0.5s ease;
		transition: all 0.5s ease;
	}

	.ijia-leco:hover {
		background-color: #fdac31;
		-webkit-transition: all 0.5s ease;
		-moz-transition: all 0.5s ease;
		-ms-transition: all 0.5s ease;
		-o-transition: all 0.5s ease;
		transition: all 0.5s ease;
	}

	.ijia-leco:hover .li-txt {
		color: #fff;
	}

	.ijia-leco:hover .li-tit {
		color: #fff;
	}

	.ijia-lr li {
		text-align: center;
	}

	.ipro-img {
		position: relative;
	}

	.nwmd-xq {
		padding: 50px 0 90px;
	}

	.nwmd-xqli {
		padding-top: 20px;
	}

	.nwmd-xqli li {
		font-size: 14px;
		color: #555;
		line-height: 2.1em;
		display: inline-block;
		vertical-align: middle;
		margin: 0 6px;
		-moz-border-radius: 20px;
		-webkit-border-radius: 20px;
		-ms-border-radius: 20px;
		-o-border-radius: 20px;
		border-radius: 20px;
		overflow: hidden;
	}

	.nwmd-xqli {
		text-align: center;
	}

	.nwmd-xqli li a {
		padding: 0 18px;
		display: block;
		color: #555;
	}

	.nwmd-xqli li.on,
	.nwmd-xqli li:hover {
		background: #eb9030;
		color: #fff;
	}

	.nwmd-xqli li.on a,
	.nwmd-xqli li:hover a {
		color: #fff;
	}

	.nwmd-xqcont {
		padding-top: 60px;
	}

	.nwmd-xqtp {
		overflow: hidden;
		position: relative;
		-moz-border-bottom-right-radius: 36px;
		-ms-border-bottom-right-radius: 36px;
		-webkit-border-bottom-right-radius: 36px;
		-o-border-bottom-right-radius: 36px;
		border-bottom-right-radius: 36px;
		background: #f3f3f3;
		-moz-border-top-left-radius: 36px;
		-ms-border-top-left-radius: 36px;
		-webkit-border-top-left-radius: 36px;
		-o-border-top-left-radius: 36px;
		border-top-left-radius: 36px;
	}

	.nwmd-xqle {
		width: 40%;
		overflow: hidden;
		float: left;
	}

	.nwmd-xq .nwmd-xqcont:nth-of-type(2n+1) .nwmd-xqtp .nwmd-xqle {
		float: right;
	}

	.nwmd-xqlr {
		float: left;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		-ms-box-sizing: border-box;
		-o-box-sizing: border-box;
		box-sizing: border-box;
		width: 60%;
		padding: 40px 40px 0;
	}

	.nwmd-xqtxt {
		font-size: 13px;
		color: #777;
		line-height: 1.6em;
	}

	.nwmd-xqtxt p {
		padding-bottom: 30px;
	}

	.nwmd-xqbt {
		padding-top: 26px;
	}

	.nwmd-xqbt li {
		display: inline-block;
		vertical-align: top;
		width: 31.6%;
		margin-right: 2.6%;
		position: relative;
		-moz-border-top-left-radius: 18px;
		-ms-border-top-left-radius: 18px;
		-o-border-top-left-radius: 18px;
		-webkit-border-top-left-radius: 18px;
		border-top-left-radius: 18px;
		overflow: hidden;
		-moz-border-bottom-right-radius: 18px;
		-webkit-border-bottom-right-radius: 18px;
		-ms-border-bottom-right-radius: 18px;
		-o-border-bottom-right-radius: 18px;
		border-bottom-right-radius: 18px;
		margin-bottom: 2.6%;
	}

	.nwmd-xqbt li:nth-child(3n) {
		margin-right: 0;
	}

	.nwmd-xqbt li p {
		position: absolute;
		left: 0;
		bottom: 0;
		width: 100%;
		background: rgba(0, 0, 0, 0.6);
		width: 100%;
		text-align: center;
		font-size: 16px;
		color: #fff;
		line-height: 2.8em;
	}

	.nwmd-xqbt {
		font-size: 0;
	}

	.fwxq {
		padding: 62px 0 104px;
		background: #f1f1f1;
	}

	.fwxq-list li {
		width: 20%;
		display: inline-block;
		vertical-align: top;
		height: 75px;
		position: relative;
		line-height: 75px;
		background: #fff;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		-ms-box-sizing: border-box;
		-o-box-sizing: border-box;
		box-sizing: border-box;
		border-right: 1px solid #f1f1f1;
	}

	.fwxq-list li select {
		opacity: 0;
		position: absolute;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
	}

	.fwxq-list {
		font-size: 0;
	}

	.fwxq-list select {
		display: block;
		width: 20%;
		height: 73px;
		display: inline;
	}

	.fwxq-list li .box {
		padding: 0 30px 0 34px;
		background: url(common/static/image/arrl.png) no-repeat 83% center;
	}

	.fwxq-list li .box span {
		font-size: 16px;
		color: #555;
	}

	.fwxq-list .btn {
		width: 100%;
		height: 70px;
		background: #cba580;
		border: none;
		font-size: 18px;
		text-align: center;
		color: #fff;
	}

	.fwxq-cont {
		margin-top: 44px;
		padding: 10px;
		background: #fff;
		position: relative;
	}

	.fwxq-le {
		padding-right: 330px;
		height: 572px;
	}

	.fwxq-lr {
		height: 572px;
		position: absolute;
		width: 340px;
		right: 0;
		top: 0;
	}

	.fwxq-lrtp {
		padding: 10px 0;
		font-size: 16px;
		color: #555;
		line-height: 2.8em;
		padding-left: 14px;
		line-height: 3em;
		border-bottom: 1px solid #f1f1f1;
	}

	.fwxq-lrtp span {
		color: #eb9030;
	}

	.fwxq-lrin li {
		padding: 22px;
		border-bottom: 1px solid #f1f1f1;
		height: 108px;
	}

	.fwxq-lrin .li-tit {
		font-size: 16px;
		color: #555;
		line-height: 2em;
		font-weight: normal;
	}

	.fwxq-lrin .li-tit .num {
		width: 31px;
		height: 31px;
		display: inline-block;
		vertical-align: middle;
		text-align: center;
		line-height: 31px;
		background: url(common/static/image/num2.png) no-repeat;
		background-size: 100% 100%;
		font-size: 14px;
		color: #fff;
		margin-right: 14px;
	}

	.fwxq-lrin .li-txt {
		font-size: 14px;
		color: #777;
		line-height: 1.9em;
		padding-left: 46px;
		padding-top: 10px;
	}

	.fwxq-lrin li:hover .num {
		background: url(common/static/image/num.png) no-repeat;
		background-size: 100% 100%;
	}

	.fwxq-lrin {
		height: 460px;
		overflow: hidden;
		position: relative;
	}

	.fwxq-lrbt {
		position: relative;
		padding: 10px 0;
		height: 40px;
		line-height: 20px;
		text-align: center;
	}

	.fwxq-lrin ul {
		position: relative;
	}

	.fwxq-le img {
		height: 100%;
	}

	.icxf-list li .li-img img {
		-moz-transition: all .5s;
		-webkit-transition: all .5s;
		-ms-transition: all .5s;
		-o-transition: all .5s;
		transition: all .5s;
	}

	.icxf-list li:hover .li-img img {
		-moz-transform: rotate(360deg);
		-webkit-transform: rotate(360deg);
		-ms-transform: rotate(360deg);
		-o-transform: rotate(360deg);
		transform: rotate(360deg);
	}

	.izxz-dot li {
		cursor: pointer;
	}

	.iwmd-blis .text {
		position: absolute;
		left: 0;
		top: 0;
		width: 100%;
		height: 101%;
		background: #fff;
		z-index: 333;
	}

	.iwmd-blis li {
		position: relative;
		overflow: hidden;
	}

	.iwmd-blis li .inn {
		display: inline-block;
		vertical-align: middle;
	}

	.iwmd-blis li .text:before {
		content: "";
		display: inline-block;
		vertical-align: middle;
		height: 100%;
		width: 0;
	}

	.iwmd-blis li:hover .text {
		filter: alpha(opacity=0);
		opacity: 0;
		-webkit-transition: all 0.5s ease;
		-moz-transition: all 0.5s ease;
		transition: all 0.5s ease;
	}

	.iwmd-blis li .img p {
		position: absolute;
		left: 0;
		bottom: 0;
		width: 100%;
		line-height: 3.6em;
		text-align: center;
		color: #fff;
		background: rgba(0, 0, 0, 0.3);
		text-align: center;
		font-size: 14px;
	}

	.iwmd-blis li .img {
		position: relative;
	}

	.iwmd-more:hover,
	.nwod-box a:hover .nwod-img {
		text-decoration: none;
		filter: alpha(opacity=70);
		opacity: 0.7;
		-webkit-transition: all 0.5s ease;
		-moz-transition: all 0.5s ease;
		-ms-transition: all 0.5s ease;
		-o-transition: all 0.5s ease;
		transition: all 0.5s ease;
	}

	.njia-list li:last-child {
		display: none;
	}

	.njia-xqli li:hover .li-img {
		-moz-transition: all .5s;
		-webkit-transition: all .5s;
		-ms-transition: all .5s;
		-o-transition: all .5s;
		transition: all .5s;
		-moz-transform: rotate(360deg);
		-ms-transform: rotate(360deg);
		-webkit-transform: rotate(360deg);
		-o-transform: rotate(360deg);
		transform: rotate(360deg);
	}

	/*.nwmd-xqle:hover,
.nwmd-xqbt li:hover img,
.zxzx-list li:hover .li-img {
    -moz-transition:all .5s;
    -webkit-transition:all .5s;
    -ms-transition:all .5s;
    -o-transition:all .5s;
    transition:all .5s;
    opacity: 0.7;
}*/
	.nwod-icon .img2 {
		display: none;
	}

	.nwod-box a:hover .nwod-icon .img2 {
		display: inline-block;
		text-decoration: none;
	}

	.nwod-box a:hover .nwod-icon .img1 {
		display: none;
		text-decoration: none;
	}

	.nwod-box a:hover .nwod-icon {
		border: 2px solid #cca581;
		text-decoration: none;
	}

	.fwcx .search {
		opacity: 0;
		position: absolute;
		width: 200px;
		-moz-box-sizing: border-box;
		-ms-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		-o-box-sizing: border-box;
		box-sizing: border-box;
		height: 30px;
		line-height: 30px;
		border: 1px solid #ccc;
		right: 0%;
		display: none;
		top: 50%;
		margin-top: -15px;
		margin-right: 20px;
		background: #fff;
		padding-right: 30px;
	}

	.fwcx {
		position: relative;
	}

	.fwcx .search-txt {
		width: 100%;
		font-size: 14px;
		color: #333;
		line-height: 30px;
		text-indent: 10px;
		border: none;
		background: none;
	}

	.fwcx .search-btn {
		position: absolute;
		width: 18px;
		height: 18px;
		position: absolute;
		right: 10px;
		top: 50%;
		margin-top: -9px;
		background: url(common/static/image/search.png) no-repeat;
		border: none;
		cursor: pointer;
	}

	.nwmd-xqlr {
		padding-right: 20px;
	}

	.nwmd-xqtxt {
		overflow: hidden;
		padding-right: 20px;
	}

	#ascrail2000 {
		width: 1px;
		height: 420px;
		background: #bbbbbb;
	}

	#ascrail2000 div {
		border: 2px solid #8db2d9;
		margin-right: -2px;
		height: 20px;
	}

	/* 弹出框 */
	.tbg {
		position: fixed;
		width: 100%;
		height: 100%;
		left: 0;
		top: 0;
		background: rgba(0, 0, 0, 0.3);
		display: none;
		z-index: 888;
	}

	.tanc {
		display: none;
		position: fixed;
		left: 0;
		top: 0;
		right: 0;
		bottom: 0;
		max-width: 480px;
		margin: auto;
		width: 90%;
		background-color: #fff;
		height: 320px;
		z-index: 888;
	}

	.tanc-top {
		background-color: #f1a400;
		padding: 12px 0 10px;
		text-align: center;
		height: 40px;
	}

	.tanc-con {
		padding: 18px 30px 24px;
	}

	.tanc-txt {
		font-size: 14px;
		color: #4f4f4f;
		line-height: 42px;
		height: 160px;
	}

	.tanc-bt {
		font-size: 14px;
		color: #4f4f4f;
		line-height: 22px;
		padding-top: 22px;
		border-top: 1px solid #ddd;
	}

	.swiper-button-prev:after {
		display: none;
	}

	.swiper-button-next:after {
		display: none;
	}
	.mySwiper .swiper-pagination-horizontal{
		height: 5px;
	}

	button {
		/*border: none;
    outline: none;*/
	}
	@media screen and (max-width: 992px) {
	    .izxz-qh .li-img {
			width: 300px;
		}
		.izxz-qh .li-img img{
		    width: 100%;
		}
		.ijia-le {
    		display: flex;
            flex-direction: column;
    	}
    	.ijia-leco {
            position: static;
            width: 100%;
        }
        .ijia-leim{
            width: 300px;
            margin: auto;
        }
        .izxz-tp .izxz-qh .li-con {
		    padding: 20px 320px 30px 30px;
		}
        .ijia-cont .ijia-lr {
            padding-top: 170px;
        }
        .icxf-cont {
            padding: 7% 0;
        }
		.izxz-qh{
			margin: 0px;
		}
		.izxz-qh,.izxz-btli ul{
			padding: 0px;
			overflow-x:scroll ;
			display: flex;
		}
		.swiper1 .prev,.swiper1 .next{
			width: 40px;
			height: 20px;
		}
		.ijia-cont .ijia-lr .wz{
			font-size: 14px;
		}
	}
	@media screen and (max-width: 769px) { 
		.izxz-tp .izxz-dot {
			width: 100%;
			left: 0;
			text-align: center;
			position: static;
			background: #f8f8f8;
			padding: 0px;
		}
	    .tit-box,.icxf-cont{
	        margin-top: 20px;
	    }
	    .ijia-lr{
	        padding-top: 50px !important;
	    }
	    .swiper-pagination {
    		position: relative;
    		top: -0.5rem;
    	}
		.tit-box .ch {
		    font-size: 18px;
		}
		.tit-box .en {
		    font-size: 20px;
			margin-bottom: 0;
		}
		.tit-box .ch {
		    font-size: 18px;
			margin:0px;
			padding:0px;
		}
		.tit-box .en {
		    font-size: 20px;
			margin-bottom: 0;
		}
		.overs{
			height: 13rem;
		}
		.izxz-btli .li-time{
			padding: 4px 6px;
		}
		.izxz-btli .li-time .t1,.izxz-btli .li-time .t2,.ijia,.izxz,.icxf,.tit-box .ch{
			padding: 0px;
			margin: 0px;
			
		}
		.izxz-btli .li-time .t1{
			font-size: 20px;
		}
		.izxz-btli .li-tit{
			font-size: 14px;
		}
		.izxz-list p,.izxz-btli .li-time .t2{
			font-size: 12px;
		}
		.iwmd-tp {
			display: block;
		}
	
		.iwmd-cont .iwmd-tple,
		.iwmd-cont .iwmd-tplr {
			width: 100%;
		}
	
		.iwmd-tp .iwmd-tplr {
			height: auto;
		}
	
		#a1 {
			margin: 0px !important;
			height: auto !important;
		}
	
		.iwmd-tit,
		.iwmd-txt {
			padding-top: 5px;
			margin-top: 5px;
		}
	
		.iwmd-blis ul,
		.izxz-list ul {
			padding-left: 0px;
		}
	
		.izxz .izxz-list li {
			width: 20vw;
			height: 20vw;
			
			margin: 0 2vw;
		}
		.izxz .izxz-list .li-img {
			width: 22px;
			margin: 0 auto;
		}
		.iwmd-cont .iwmd-blis li {
			width: 32%;
			margin-bottom: 3vw;
			height: 40vw;
		}
	
		.iwmd-cont .iwmd-blis li: .iwmd-cont .iwmd-blis ul {
			flex-wrap: wrap;
		}
	
		.izxz .izxz-btli li {
			width: 100%;
			margin-bottom: 45px;
		}
	
		.ijia-cont .ijia-le,
		.ijia-cont .ijia-lr {
			width: 100%;
			float: none;
		}
	
		.ijia-cont .ijia-lr {
			padding-bottom: 70%;
		}
	
		.ijia .ijia-cont {
			background: #333;
		}
	
		.icxf-cont .icxf-list p {
			font-size: 12px;
		}
	
		.ijia-cont .ijia-le {
			padding: 0 5%;
		}
	
		.icxf-cont .icxf-list li {
			width: 33.3%;
			/*padding: 0 5%;*/
			margin-bottom: 5%;
		}
	
		.ijia-leim .clearfix,
		.ijia-leco ul,
		.li-tit,
		.icxf-list {
			padding: 0px;
			margin: 0px;
		}
	
		.ijia-le .ijia-leco,
		.ijia-le .ijia-leim {
			position: static;
			width: 100%;
			padding: 0px;
		}
		.izxz-tp .izxz-qh .li-img,.izxz-tp .izxz-qh .li-con,.izxz-qh .li-img a,.izxz-qh .li-img a img{
			float: none;
			width: 100%;
		}
		.izxz-tp .izxz-qh .li-con{
			position: static;
			font-size: 16px;
			padding: 10px;
		}
		.izxz-tp .izxz-qh .li-tit{
			font-size: 16px;
		}
		.izxz-tp .izxz-qh .li-txt{
			padding: 0px;
		}
		.izxz .izxz-btli li {
			width: 45%;
			margin: 45px 2%;
		}
	}
	@media screen and (min-width: 769px) and (max-width: 1028px) {
	    .swiper1 img{
	        height: 5.3rem;
	    }
	    .swiper1 .li-btn a{
	       padding: 0px 20px;
	       line-height: 35px;
	    }
	    .footer-list {
            margin-right: 40px;
        }
	    .ijia-lr{
	        padding-top: 35px;
	    }
	    .ijia-leco .li-tit {
            font-size: 22px;
        }
	    .izxz-list li {
            width: 140px;
            height: 140px;
        }
		.izxz-tp .izxz-qh .li-txt{
			padding-top: 0px;
		}
		.izxz-tp .izxz-dot{
			left: 10%;
			bottom: -10px;
		}
		/*.izxz-qh .li-img img {*/
		/*	width: 100%;*/
		/*}*/
	
	
		.izxz-qh .li-txt {
			padding-top: 0px;
		}
		.ijia-leco {
            padding: 20px 20px 0;
        }
	    /*.ijia-le{*/
	    /*    display: flex;*/
	    /*    flex-direction: column;*/
	    /*}*/
	    /*.ijia-leco{*/
	    /*    position: static;*/
	    /*}*/
	    /*.ijia-leim{*/
	    /*    width: 300px;*/
	    /*    margin: auto;*/
	    /*}*/
	    
		/*.ijia-cont .ijia-le,*/
		/*.ijia-cont .ijia-lr {*/
		/*	width: 100%;*/
		/*	float: none;*/
		/*}*/
	
		/*.ijia-cont .ijia-lr {*/
		/*	padding-bottom: 50%;*/
		/*}*/
	
		/*.ijia-lr li {*/
		/*	width: 25%;*/
		/*	padding-bottom: 25%;*/
		/*}*/
	
		/*.ijia .ijia-cont {*/
		/*	background: #333;*/
		/*}*/
	
		.icxf-cont .icxf-list p {
			font-size: 13px;
		}
	}
</style>
 

<!-- Swiper -->
<div class="swiper mySwiper">
	<div class="swiper-wrapper">
		<?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'banner')) {$data = $tag->banner(array('field'=>'title,image,typeid,status,url','typeid'=>'1','limit'=>'10',));}?>
		<?php if(is_array($data)) foreach($data as $v) { ?>
		<div class="swiper-slide"><a href="<?php echo $v['url'];?>">
				<img src="<?php echo $v['image'];?>" alt="<?php echo $v['title'];?>" title="<?php echo $v['title'];?>"></a></div>
		<?php } ?>
	</div>
	<!-- <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div> -->
	<div class="swiper-pagination"></div>
</div>

<div class="main">
	<div class="ipro">
		<!--  <div class="container"> -->

		<div class="tit-box">
			<h3 class="en">our products</h3>
			<h3 class="ch"><span>我们的<b>产品</b></span></h3>
		</div>
		<div class="ipro-txt">
			<p>
			</p>
		</div>
		<div class="ipro-list">
			<ul>
				<li>
					<a href="<?php echo $site['site_url'];?>product?lm=35">
						<img src="common/static/image/2019010711255881.png" alt="" class="img1">
						<img src="common/static/image/2019010711260919.png" alt="" class="img2">
					</a>
				</li>
				<li>
					<a href="<?php echo $site['site_url'];?>product?lm=93">
						<img src="common/static/image/2019011608585154.jpg" alt="" class="img1">
						<img src="common/static/image/2019011608590226.jpg" alt="" class="img2">
					</a>
				</li>
			</ul>
		</div>

		<div class="overs">
			<div class="swiper1" id="swiper1">
				<div class="swiper-wrapper">
					<?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'banner')) {$data = $tag->banner(array('field'=>'title,image,typeid,status,url,url1','typeid'=>'2','limit'=>'6',));}?>
					<?php if(is_array($data)) foreach($data as $v) { ?>
					<div class="swiper-slide">
						<div class="ipro-ibo">
							<img src="<?php echo $v['image'];?>" alt="<?php echo $v['title'];?>" title="<?php echo $v['title'];?>">
							<p class="tieti"><?php echo $v['title'];?></p>
							<div class="li-xq">
								<div class="li-btn">
									<a href="<?php echo $v['url'];?>" class="btn1">查看详情 &gt;</a>
									<!--<a href="<?php echo $v['url1'];?>" class="btn2">商城购买 &gt;</a>-->
								</div>
							</div>
						</div>

					</div>
					<?php } ?>

				</div>
				<!-- <div class="swiper-button-next"></div>
                      <div class="swiper-button-prev"></div> -->
				<!-- <div class="ipro-qh"> -->
				<div style="display: flex; align-items: center; justify-content: center; padding-top: 40px;">
				    
					<div class="prev prev1 swiper-button-prev">&lt;</div>
					
					
					<div class="ipro-dot ipro-dot1 swiper-pagination-clickable swiper-pagination-bullets" style="width: auto;">
					    <span class="swiper-pagination-bullet1"></span>
					    <span class="swiper-pagination-bullet swiper-pagination-bullet-active1"></span>
					    <span class="swiper-pagination-bullet1"></span>
					</div>
					    
					    
					    
					<div class="next next1 swiper-button-next">&gt;</div>
				</div>
				<!-- </div> -->
			</div>
		</div>

		<!--                 <div class="ipro-cont">
                    <div class="ipro-box swiper-container swiper-container-horizontal swiper-container-autoheight" id="ipro-box">
                        <ul class="ipro-img clearfix swiper-wrapper" style="height: 344px; transform: translate3d(-1600px, 0px, 0px); transition-duration: 0ms;">
                        <li class="swiper-slide swiper-slide-duplicate swiper-slide-duplicate-prev" data-swiper-slide-index="0" style="width: 400px;">
                                <div class="ipro-ibo">
                                    <div class="li-img">
                                        <img src="common/static/image/2020060511043125.jpg" alt="">
                                        <div class="li-xq">
                                            <div class="li-btn">
                                                <a href="http://www.szguanniu.com/product_showmore.php?id=191" class="btn1">查看详情  &gt;</a>
                                                <a href="http://www.szguanniu.com/index.php" class="btn2">商城购买  &gt;</a>
                                            </div>
                                        </div>
                                    </div>
                                    <p>极简意式</p>
                                </div>
                            </li></ul>
                        <div class="ipro-qh">
                            <div class="prev prev1">&lt;</div>
                            <div class="ipro-dot ipro-dot1 swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active"></span><span class="swiper-pagination-bullet"></span></div>
                            <div class="next next1">&gt;</div>
                        </div>
                    </div>
                </div> -->



		<div class="iwmd">
			<div class="container">
				<div class="tit-box">
					<h3 class="en">Our crown cattle</h3>
					<h3 class="ch"><span>我们的<b>冠牛 </b></span></h3>
				</div>
				<div class="iwmd-cont">
					<div class="iwmd-tp">
						<div class="iwmd-tple">
							<!-- <img style="width: 792px" src="" alt=""> -->
							<div id="a1"
								style="width: 100%; height: 486px;  background-color: rgb(0, 0, 0); cursor: pointer;">
								<video controls="controls" src="<?php echo $tit_video;?>"
									id="ckplayer_a1" width="100%" height="100%" class="ckplayer_a1"></video></div>
							<script type="text/javascript" src="common/static/image/ckplayer.js" charset="utf-8">
							</script>
							<script type="text/javascript">
								/*
                                f:视频地址
                                i:视频默认图片
                                c:调用配置文件方式 =0:调用ckplayer.js =1:调用ckplayer.xml
                                p:打开页面默认播放方式 =0:暂停但视频会加载 =1:播放 =2:不播放不加载视频
                                h:是否可以拖拽 =0:普通播放 =3:可以拖拽播放未加载好的视频
                                e:播放结束的动作 =0:调用js函数(function playerstop(){}) =1:循环播放 =2:暂停播放并且不调用广告
                                b:是否允许js和播放器交互 =1是不允许，=0是允许
                                */
								//flash播放--视频地址+参数设置
								var flashvars = {
									f: '<?php echo $tit_video;?>',
									i: '',
									c: 0,
									p: 0,
									h: 3,
									e: 2,
									b: 1
								};
								//html5播放--视频地址
								var html5url = ['<?php echo $tit_video;?>->video/mp4'];
								CKobject.embed('demo/ckplayer/ckplayer.swf', 'a1', 'ckplayer_a1', '100%', '100%', false, flashvars, html5url)
							</script>
						</div>
						<div class="iwmd-tplr">
							<div class="iwmd-inn">
								<h3 class="iwmd-tit">关于冠牛</h3>
								<div class="iwmd-txt">
									<p>冠牛公司20余年专注实木家居定制服务领域，在为消费者提供高品质产品及服务的同时，争取为社会提供更多就业机会、为投资者创造更大商业价值。</p>
								</div>
								<a href="<?php echo $site['site_url'];?>guanyuguanniu" class="iwmd-more">查看详情 &gt;</a>
							</div>
						</div>
					</div>
					<div class="iwmd-blis">
						<ul>
						    <?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'pro_list')) {$data = $tag->pro_list(array('field'=>'title,url,description,pro_img','id'=>'236','limit'=>'1','page'=>'page',));$pages = $tag->pages();}?>
                            <?php if(is_array($data)) foreach($data as $v) { ?>
							<li>
								<a href="<?php echo $v['text'];?>">
									<div class="text">
										<div class="inn">
											<div class="li-img">
												<img src="<?php echo $v['word'];?>" alt="">
											</div>
											<p><?php echo $v['alt'];?></p>
										</div>
									</div>
									<div class="img">
										<img src="<?php echo $v['url'];?>" alt="">
										<p><?php echo $v['alt'];?></p>
									</div>
								</a>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!--  </div> -->
	</div>
</div>



<div class="izxz">
	<div class="container">
		<div class="tit-box">
			<h3 class="en">information centre</h3>
			<h3 class="ch"><span>资讯<b>中心</b></span></h3>
		</div>
		<div class="izxz-list">
			<ul>
				<li>
					<a href="<?php echo $site['site_url'];?>gongsixinwen">
						<div class="box">
							<div class="li-img">
								<img src="common/static/image/z1.png" alt="" class="img1">
								<img src="common/static/image/zz1.png" alt="" class="img2">
							</div>
							<p>公司新闻</p>
						</div>
					</a>
				</li>
				<li>
					<a href="<?php echo $site['site_url'];?>pinpaihuodong">
						<div class="box">
							<div class="li-img">
								<img src="common/static/image/z2.png" alt="" class="img1">
								<img src="common/static/image/zz2.png" alt="" class="img2">
							</div>
							<p>品牌活动</p>
						</div>
					</a>
				</li>
				<li>
					<a href="<?php echo $site['site_url'];?>xingyezixun">
						<div class="box">
							<div class="li-img">
								<img src="common/static/image/z3.png" alt="" class="img1">
								<img src="common/static/image/zz3.png" alt="" class="img2">
							</div>
							<p>行业资讯</p>
						</div>
					</a>
				</li>
			</ul>
		</div>
		<div class="izxz-tp">
			<ul class="izxz-qh">
				<?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'pro_list')) {$data = $tag->pro_list(array('field'=>'title,url,description,pro_img','id'=>'237','limit'=>'1','page'=>'page',));$pages = $tag->pages();}?>
                    <?php if(is_array($data)) foreach($data as $k => $v) { ?>
				<li <?php if($k==0) { ?>class="on"<?php } ?>>   
				<!--style="display: none; opacity: 0.2;"-->
					<div class="li-img">
						<a href="<?php echo $site['site_url'];?>news">
							<img width="500px" height="auto" src="<?php echo $v['url'];?>" alt="">
						</a>
					</div>
					<div class="li-con">
						<h3 class="li-tit"><?php echo $v['word'];?></h3>
						<div class="li-txt">
							<p></p>
							<?php echo $v['text'];?>
						</div>
					</div>
				</li>
			  <?php } ?>
			
			</ul>
			<ul class="izxz-dot"></ul>
		</div>
		<div class="izxz-btli">
			<ul>
				<li>
					<a href="<?php echo $site['site_url'];?>gongsixinwen/54.html">
						<div class="li-time">
							<h3 class="t1">11/27</h3>
							<p class="t2">2021</p>
						</div>
						<div class="li-con">
							<h3 class="li-tit">恭喜冠牛工程团队荣获华为工程项目类卓越贡献奖</h3>
							<div class="li-txt">
								<p>恭喜冠牛工程团队荣获华为工程项目类卓越贡献奖感谢华为对冠牛工程团队的认可冠牛会继续努力，做更好的产品，提供更好的服务！</p>
							</div>
						</div>
					</a>
				</li>
				<li>
					<a href="<?php echo $site['site_url'];?>gongsixinwen/53.html">
						<div class="li-time">
							<h3 class="t1">12/10</h3>
							<p class="t2">2021</p>
						</div>
						<div class="li-con">
							<h3 class="li-tit">冠牛快讯近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。</h3>
							<div class="li-txt">
								<p>冠牛快讯：近日2021年第九届中国门墙柜技术大会在辽宁沈阳顺利召开。冠牛副总裁庞晓良女士应邀出席本次大会，并在高端辩论会环节，与行业优秀企业进行观点交锋，从各自为导向的优势，来对定制木作高端发展，提供一些...
								</p>
							</div>
						</div>
					</a>
				</li>
				<li>
					<a href="<?php echo $site['site_url'];?>gongsixinwen/56.html">
						<div class="li-time">
							<h3 class="t1">10/22</h3>
							<p class="t2">2021</p>
						</div>
						<div class="li-con">
							<h3 class="li-tit">冠牛庞晓良：坚守对实木文化的传承与创新</h3>
							<div class="li-txt">
								<p>10月22日，2021年中国木门窗企业家峰会暨中国木材与木制品流通协会产业链分会成立大会在山东滕州顺利召开。</p>
							</div>
						</div>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div>
<div class="ijia">
	<div class="container">
		<div class="tit-box">
			<h3 class="en">Franchise Cow</h3>
			<h3 class="ch"><span>加盟<b>冠牛 </b></span></h3>
		</div>
	</div>
	<div class="ijia-cont">
		<div class="container">
			<div class="ijia-le">
				<div class="ijia-leim">
					<ul  style="">
						<li style="width: 300px;">
							<img src="common/static/image/2018071910133272.jpg" alt="">
						</li>
						<li style="width: 300px;">
							<img src="common/static/image/2018071910133272.jpg" alt="">
						</li>
						<li style="width: 300px;">
							<img src="common/static/image/2018071910133272.jpg" alt="">
						</li>
						<li style="width: 300px;">
							<img src="common/static/image/2018071910133272.jpg" alt="">
						</li>
						<li style="width: 300px;">
							<img src="common/static/image/2018071910133272.jpg" alt="">
						</li>
					</ul>
				</div>
				<div class="ijia-leco">
					<ul>
                        <?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'pro_list')) {$data = $tag->pro_list(array('field'=>'title,url,description,pro_img','id'=>'238','limit'=>'1','page'=>'page',));$pages = $tag->pages();}?>
                        <?php if(is_array($data)) foreach($data as $k => $v) { ?>
    						<li <?php if($k==0) { ?>class="on"<?php } ?>>
    							<h3 class="li-tit"><?php echo $v['alt'];?></h3>
    							<div class="li-txt">
    								<p><?php echo $v['word'];?></p>
    							</div>
    						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
			<div class="ijia-lr">
				<ul class="ijia-lrli">
				     <?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'pro_list')) {$data = $tag->pro_list(array('field'=>'title,url,description,pro_img','id'=>'238','limit'=>'1','page'=>'page',));$pages = $tag->pages();}?>
                        <?php if(is_array($data)) foreach($data as $k => $v) { ?>
					<li class="li<?php echo $k;?> <?php if($k==0) { ?>on<?php } ?>">
						<a href="<?php echo $v['text'];?>">
							<div class="wz">
								<?php echo $v['alt'];?>
							</div>
						</a>
					</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</div>


<div class="icxf">
	<div class="container">
		<div class="tit-box">
			<h3 class="en">Innovative service system</h3>
			<h3 class="ch"><span>创新<b>服务体系</b></span></h3>
		</div>
	</div>
	<div class="icxf-cont">
		<div class="container">
			<ul class="icxf-list">
			    <?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'pro_list')) {$data = $tag->pro_list(array('field'=>'title,url,description,pro_img','id'=>'239','limit'=>'1','page'=>'page',));$pages = $tag->pages();}?>
                    <?php if(is_array($data)) foreach($data as $v) { ?>
				<li>
					<div class="li-img">
						<div class="img-inn">
							<img src="<?php echo $v['url'];?>" alt="">
						</div>
					</div>
					<p><?php echo $v['alt'];?></p>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>
</div>

<?php include template("index","footer"); ?>







<!-- <div class="banner-box">
        <ul id="img-list">
                        <li class="show" style="display: none; opacity: 0.2;"><a href="javascript:;">
                <img src="common/static/image/2020092415064110.jpg" alt=""></a>
            </li>
                        <li style="display: list-item; opacity: 1;"><a href="javascript:;">
                <img src="common/static/image/2019112616363625.jpg" alt=""></a>
            </li>
                        <li style="display: none; opacity: 0.2;"><a href="javascript:;">
                <img src="common/static/image/2022061709291683.jpg" alt=""></a>
            </li>
                        <li style="display: none; opacity: 0.2;"><a href="javascript:;">
                <img src="common/static/image/2018090316330673.jpg" alt=""></a>
            </li>
                        <li style="display: none; opacity: 0.2;"><a href="javascript:;">
                <img src="common/static/image/2018101316100795.jpg" alt=""></a>
            </li>
                    </ul>
        
        <ul id="dot"><li class=""></li><li class="cur"></li><li class=""></li><li class=""></li><li class=""></li></ul>
    </div> -->

<script type="text/javascript">
	var swiper = new Swiper(".mySwiper", {
		spaceBetween: 30,
		centeredSlides: true,
		autoplay: {
			delay: 2500,
			disableOnInteraction: false,
		},
		pagination: {
			el: ".swiper-pagination",
			clickable: true,
			renderBullet: function(index, className) {
				return '<li class="' + className + '"></li>';
			},
		},
		autoHeight: true,
		paginationClickable: true,
		// navigation: {
		//   nextEl: ".swiper-button-next",
		//   prevEl: ".swiper-button-prev",
		// },
	});

	var swiper1 = new Swiper('.swiper1', {
		slidesPerView: getSlidesPerView(),
		spaceBetween: 30,
		centeredSlides: true,
		// direction: getDirection(),  		////移动端就横向滑动
		pagination: {
			el: ".ipro-dot",
			clickable: true,
// 			type:"custom",
// 			renderCustom:function(swiper, current, total){
// 			    console.log(current,"current")
// 				$('.ipro-dot span').eq((current-1)).addClass('dotc').siblings().removeClass("dotc")
// 			}
		},
		navigation: {
			nextEl: ".swiper-button-next",
			prevEl: ".swiper-button-prev",
		},
		paginationClickable: true,
		autoplay: 2000,
		autoHeight: true,
		loop: true,
		

        // breakpoints: {
        //     768: {
        //         slidesPerView: 2,
        //         spaceBetween: 0
        //     },
        //     420: {
        //         slidesPerView: 1,
        //         spaceBetween: 0
        //     }
        // }
    
        // var comtainer = document.getElementById('swiper1');
        // comtainer.onmouseenter = function () {
        //     swiper.stopAutoplay();
        // };
        // comtainer.onmouseleave = function () {
        //     swiper.startAutoplay();
        // }
  
		// on: {
		// 	resize: function() {
		// 		swiper.changeDirection(getDirection());
		// 	},
		// },
	});
	function getSlidesPerView(){
		var windowWidth = window.innerWidth;
		if(windowWidth>=1024){
			return 3
		}
		if(windowWidth<769){
			return 1
		}
		if(windowWidth<1024){
			return 2
		}
		
	}
	function getDirection() {
		var windowWidth = window.innerWidth;
		var direction = window.innerWidth <= 760 ? 'vertical' : 'horizontal';

		return direction;
	}

	var $banner1 = $(".izxz-tp"),
		$pic1 = $(".izxz-qh li"),
		$do1 = $(".izxz-dot"),
		length1 = $pic1.length,
		index1 = 0,
		clickTime1 = 0,
		timer11 = null,
		timer21 = null
	for (var i = 0; i < length1; i++) {
		$('<li></li>').appendTo($do1);
	}
	//console.log($pic1);
	var $tab1 = $(".izxz-dot li");
	$pic1.hide().eq(0).show();
	$tab1.eq(0).addClass('on');
	//tab移入
	$tab1.mouseenter(function() {
		$tab1.removeClass('on');
		$(this).addClass("on");
		var x1 = $(this).index();
		if (x1 !== index1) {
			clearTimeout(timer11);
			timer11 = setTimeout(change1, 300, x1);
		}
	});
	$banner1.hover(function() {
		clearInterval(timer21);
	}, auto1())

	function auto1() {
		timer21 = setInterval(function() {
			var x1 = index1;
			x1++;
			x1 %= length1;
			change1(x1);
		}, 3000);
		return auto1;
	}

	//变化函数
	function change1(i) {
		$pic1.eq(index1).stop().hide().css({
			"opacity": "0.2"
		});
		$tab1.eq(index1).removeClass("on");
		index1 = i;
		$pic1.eq(index1).stop().show().animate({
			"opacity": "1"
		})
		$tab1.eq(index1).addClass("on");
	}
	$(window).resize(function() {
		if (isMbl()) {
			$(".footer-list dd").hide();
		} else {
			$(".footer-list dd").show();
		}
		if ($(window).width() > 868) {
			$(".menu").show();
		} else {
			$(document).click(function() {
				$('.menu').slideUp();
			});
		}
	})
	
	
	$(function(){	
	var lenth1 = $('#img_ul>li').length;
	//初始化
	function qiuye_resize1() {
		var liw1 = $('.ijia-leim').width();	
		var lenth1 = $('.ijia-leim li').length;
		$('.ijia-leim li').width(liw1);//动态设置li的宽度
		$('.ijia-leim ul').width(liw1 * lenth1);//动态设置ul的宽度	
	}
	var f = 0;
	qiuye_resize1();
	//改变窗口修正初始化
			
	var liw1 = $('.ijia-leim').width();	
    //焦点点击事件
    var timef=1000;
    var sha=0;
    var cha=0;
    var times=null;
	$('.ijia-lr li .wz').click(function() {
        f = $(this).parent("li").index();
        $(".ijia-lr li").eq(f).addClass("on").siblings("li").removeClass("on");
        $(".ijia-leco li").eq(f).stop().show().animate({"opacity":"1"},1000).siblings("li").hide().css({"opacity":0});
        cha=f-sha;
        if(cha<0) {
            cha=-cha;
        }
        times=cha*timef;
        $(".ijia-leim ul").stop().animate({"left":liw1*-f},times);
        sha=f;
	})
	
	
	    window.onresize = function() {
// 		qiuye_resize();
//         qiuye_bo();
        qiuye_resize1();
    }
})

	// var swiper1 = new Swiper('#ipro-box', {
	//        pagination: '.ipro-dot',
	//        nextButton: '.ipro-qh .next',
	//        prevButton: '.ipro-qh .prev',
	//        slidesPerView: 3,
	//        paginationClickable: true,
	//        spaceBetween: 0,
	//        loop:true,
	//        autoplay:2000,
	//        autoHeight: true,
	//        breakpoints: {
	//            768: {
	//                slidesPerView: 2,
	//                spaceBetween: 0
	//            },
	//            420: {
	//                slidesPerView: 1,
	//                spaceBetween: 0
	//            }
	//        }
	//    })
	// var comtainer = document.getElementById('ipro-box');
	//   comtainer.onmouseenter = function () {
	//     swiper1.stopAutoplay();
	//   };
	//   comtainer.onmouseleave = function () {
	//     swiper1.startAutoplay();
	//   }
</script>
