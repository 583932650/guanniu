<?php defined('IN_YZMPHP') or exit('No permission resources.'); ?><?php include template("index","header"); ?>
<style>
@media screen and (max-width:1280px) {
	.banner .banner-img {
		margin-top: 2.6rem;
	}
}
@media screen and (max-width: 550px) {
	.nymain .njia .njia-list li{
		width: 100%;
		margin: 0px;
	}
	.njia-list{
		padding: 0px;
	}
}
/* 手机 */
@media screen and (max-width: 769px) { 
    .banner-menu .banner-lis li.on a {
        color: #eb9030;
        background: none;
    }
	.menu-tit {
		display: none;
	}

	.banner .banner-img {
		width: 100%;
		height: auto;
		margin-top: 2.5rem;
	}

	.banner {
		height: auto !important;
	}

	.banner .banner-menu { transform: translateY(0); border-bottom: 10px solid #f1f1f1;
		position: static;
		top: 0;
	}

	.nwod .nwod-box {
		padding: 7% 0 8%;
	}

	.banner-menu .banner-lis {
		width: 100%;
		text-align: left;
	}

	.banner-menu .banner-lis,.banner-menu .banner-lis li a {
		margin: 0px;
		padding: 0px;
	}

	.gnsx-xq .gnsx-xqli li{
		width: 100%;
		text-align: center;
	}
	.gnsx-xq .gnsx-xqli li .li-tit{
		padding-top: 10px;
		font-size: 16px;
	}
	.gnsx-xqli .li-img{
		width: 60%;
		margin: auto;
	}
	.gnsx-xq .gnsx-xqli .zw {
		font-size: 16px;
	}
	.banner-lis a:hover {
		background: none !important;
	}
	.banner-menu .banner-lis li {line-height: 40px;
		margin: 0 3%;
		font-size: 12px;
	}
	.nymain .njia-list li{
		width: 50%;
	}
	.njia .njia-list .last-li-img{
		display: inline-block;
		width: 50%;
	}
}
@media screen and (min-width: 769px) and (max-width: 1024px) {
	.banner-menu .banner-lis li {
	    /*line-height: 40px;*/
		margin: 0 26px 0px 6px;
		/*font-size: 14px;*/
	}
	/*.banner-menu .banner-lis li a{*/
	/*	padding: 0px;*/
	/*}*/
	/*.banner-lis a:hover {*/
	/*	background: none !important;*/
	/*}*/
	/*.banner-menu .banner-lis{*/
	/*	display: block;*/
	/*}*/
}
.banner {
    position:relative;
     /* height: 20rem; */
}
.banner img {
    width:100%;
    height: 100%;
}
.banner-bg {
    position:absolute;
    left:0;
    top:0;
    width:100%;
    height:100%;
    background:rgba(0, 0, 0, 0.3);
}
.banner-menu {
   position:absolute;
   transform: translateY(-50%);
   left:0;
   top:62%;
   width:100%;
   text-align:center;
   z-index: 888;
   font-size:0;
}
.banner-menu .menu-tit {
    font-size:28px;
    color:#fff;
    line-height:1.4em;
    padding-bottom:3.8%;
    font-weight:normal;
    letter-spacing:1px;
}
.banner-lis {
    display:inline-block;
    padding:0 50px 0 30px;
    -moz-border-radius:30px;
    -webkit-border-radius:30px;
    -ms-border-radius:30px;
    -o-border-radius:30px;
    background:rgba(255, 255, 255, 0.9);
    border-radius:30px;
}
.banner-lis li {
    display:inline-block;
    vertical-align:top;
    font-size:16px;
    line-height:62px;
    margin:0 26px;
}
.banner-lis li a {
    padding-left:20px;
    display:block;
    color:#555;
}
.banner-lis li.on a {
    background:url("<?php echo $site['site_url'];?>common/static/image/arr.png") no-repeat left center;
}
.banner-lis a:hover {
    text-decoration:none;
    background:url("<?php echo $site['site_url'];?>common/static/image/arr.png") no-repeat left center;
}
.banner-img {
	position: relative;
	margin-top: 4rem;
	width: 100%;
}


.njia {
    padding:64px 0 130px;
}
.njia-list .li-con {
    background:#f7f7f7;
}
.njia-list li {
    display:inline-block;
    vertical-align: top;
    width:33.3%;
}
.njia-list {
    font-size:0;
}
.njia-list  li{
    margin-bottom:15px;
}
.njia-list li .li-con {
    font-size:0;
    padding-bottom:75%;
    position: relative;
}
.njia-list .li-inn {
    text-align: center;
    padding:0 11%;
    -moz-box-sizing:border-box;
    -webkit-box-sizing:border-box;
    -ms-box-sizing:border-box;
    -o-box-sizing:border-box;
    box-sizing:border-box;
    position: absolute;
    left:0;
    top:50%;
    width:100%;
    -moz-transform:translateY(-50%);
    -webkit-transform:translateY(-50%);
    -ms-transform:translateY(-50%);
    -o-transform:translateY(-50%);
    transform:translateY(-50%);
}
.njia-list .li-tit .icon{
    -moz-transition: all .5s;
    -ms-transition: all .5s;
    -webkit-transition: all .5s;
    -o-transition: all .5s;
    transition: all .5s;
    display: inline-block;
    vertical-align: middle;
    width:46px;
    -moz-border-radius:50%;
    -ms-border-radius:50%;
    -webkit-border-radius:50%;
    -o-border-radius:50%;
    border-radius:50%;
    background:#777;
    margin-right:12px;
}
.njia-list .li-tit p {
    display:inline-block;
    vertical-align: middle;
    font-size:20px;
    color:#555;
    line-height:1.5em;
}
.njia-list .li-tit {
    text-align:center;
    padding-bottom:16px;
    position: relative;
    left: -0.8rem;
}
.njia-list .li-txt {
    font-size:14px;
    color:#777;
    line-height:1.6em;
    padding-bottom:36px;
}
.njia-list .li-more:hover {
    color: #fff;
     border-color: #fdc064; 
     background-color: #fdc064; -webkit-transition: all 0.5s ease; -moz-transition: all 0.5s ease; 
     -ms-transition: all 0.5s ease; 
     -o-transition: all 0.5s ease; 
     transition: all 0.5s ease; 
}
.njia-list li {
    overflow: hidden;
}
.njia-list .li-img {
    overflow: hidden;
}
.njia-list .last-li-img{
	display: none;
}
.njia-list .li-img:hover,
.gnlis li:hover .li-img img{
    -webkit-transform: scale(1.05, 1.05); -moz-transform: scale(1.05, 1.05); -webkit-transition: all 0.5s ease; -moz-transition: all 0.5s ease; transition: all 0.5s ease; 
}
.njia-list .li-more {
    display: inline-block;
    font-size:14px;
    color:#555;
    line-height:2.5em;
    padding:0 18px;
    border:1px solid #ccc;
    -moz-border-radius:3px;
    -webkit-border-radius:3px;
    -ms-border-radius:3px;
    -o-border-radius:3px;
    border-radius:3px;
}

.li-img img{
    max-width: 100%;
	width: 100%;
}


</style>





<div class="banner">
        <div class="banner-img">
                            <img src="<?php echo $site['site_url'];?>common/static/image/2018072116514484.jpg">            <div class="banner-bg"></div>
        </div>
        <div class="banner-menu">
           <div class="container">
                <h3 class="menu-tit">加盟冠牛</h3>
                <ul class="banner-lis">
                                            <li>
                            <a href="<?php echo $site['site_url'];?>pinpaiyoushi">品牌优势</a>
                        </li>
                                                <li>
                            <a href="<?php echo $site['site_url'];?>xingyeyoushi">行业优势</a>
                        </li>
                                                <li>
                            <a href="<?php echo $site['site_url'];?>jiamengzhichi">加盟支持</a>
                        </li>
                                                <li>
                            <a href="<?php echo $site['site_url'];?>jiamengliucheng">加盟流程</a>
                        </li>
                                                <li>
                            <a href="<?php echo $site['site_url'];?>woyaojiameng">我要加盟</a>
                        </li>
                                        </ul>
            </div>
        </div>
    </div>


    <div class="nymain">
        <div class="njia">
            <div class="container">
                <ul class="njia-list">
                    <li>
                        <div class="li-con">
                            <div class="li-inn">
                                <div class="li-tit">
                                    <div class="icon"><img src="<?php echo $site['site_url'];?>common/static/image/p1.png" alt=""></div>
                                   <p>品牌优势</p>  
                                </div>
                                <div class="li-txt">
                                    <p>ABOUT CROWN COW</p>
                                </div>
                                <a href="<?php echo $site['site_url'];?>pinpaiyoushi" class="li-more">查看详情  &gt;</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="li-img">
                            <img src="<?php echo $site['site_url'];?>common/static/image/y2.jpg" alt="">
                        </div>
                    </li>
                    <li>
                        <div class="li-con">
                            <div class="li-inn">
                                <div class="li-tit">
                                    <div class="icon"><img src="<?php echo $site['site_url'];?>common/static/image/p1.png" alt=""></div>
                                   <p>行业优势</p>  
                                </div>
                                <div class="li-txt">
                                    <p>ABOUT CROWN COW</p>
                                </div>
                                <a href="<?php echo $site['site_url'];?>xingyeyoushi" class="li-more">查看详情  &gt;</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="li-img">
                            <img src="<?php echo $site['site_url'];?>common/static/image/y1.jpg" alt="">
                        </div>
                    </li>
                    <li>
                        <div class="li-con">
                            <div class="li-inn">
                                <div class="li-tit">
                                    <div class="icon"><img src="<?php echo $site['site_url'];?>common/static/image/p3.png" alt=""></div>
                                   <p>加盟支持</p>  
                                </div>
                                <div class="li-txt">
                                    <p>ABOUT CROWN COW</p>
                                </div>
                                <a href="<?php echo $site['site_url'];?>jiamengzhichi" class="li-more">查看详情  &gt;</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="li-img">
                            <img src="<?php echo $site['site_url'];?>common/static/image/y3.jpg" alt="">
                        </div>
                    </li>
                    <li>
                        <div class="li-con">
                            <div class="li-inn">
                                <div class="li-tit">
                                    <div class="icon"><img src="<?php echo $site['site_url'];?>common/static/image/p1.png" alt=""></div>
                                   <p>加盟流程</p>  
                                </div>
                                <div class="li-txt">
                                    <p>ABOUT CROWN COW</p>
                                </div>
                                <a href="<?php echo $site['site_url'];?>jiamengliucheng" class="li-more">查看详情  &gt;</a>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="li-img">
                            <img src="<?php echo $site['site_url'];?>common/static/image/y4.jpg" alt="">
                        </div>
                    </li>
                    <li>
                        <div class="li-con">
                            <div class="li-inn">
                                <div class="li-tit">
                                    <div class="icon"><img src="<?php echo $site['site_url'];?>common/static/image/p5.png" alt=""></div>
                                   <p>我要加盟</p>  
                                </div>
                                <div class="li-txt">
                                    <p>ABOUT CROWN COW</p>
                                </div>
                                <a href="<?php echo $site['site_url'];?>woyaojiameng" class="li-more">查看详情  &gt;</a>
                            </div>
                        </div>
                    </li>
					<li class="last-li-img">
					    <div class="li-img">
					        <img src="<?php echo $site['site_url'];?>common/static/image/y4.jpg" alt="">
					    </div>
					</li>
                </ul>
            </div>
        </div>
    </div>



   <?php include template("index","footer"); ?>



    <script type="text/javascript">

    

    </script>





