<?php defined('IN_YZMPHP') or exit('No permission resources.'); ?><?php include template("index","header"); ?>
<style>
body, h1, h2, h3, h4, h5, p, dl, dd, ul, ol, form, input, textarea, th, td, select {
    margin: 0;
    padding: 0;
}
	.wmxq-txt img {
		width: 100%;
	}

	.banner {
		position: relative;
		/* height: 20rem; */
	}

	.banner img {
		width: 100%;
		height: 100%;
	}

	.banner-bg {
		position: absolute;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
		background: rgba(0, 0, 0, 0.3);
	}

	.banner-menu {
		position: absolute;
		transform: translateY(-50%);
		left: 0;
		top: 62%;
		width: 100%;
		text-align: center;
		z-index: 888;
		font-size: 0;
	}

	.banner-menu .menu-tit {
		font-size: 28px;
		color: #fff;
		line-height: 1.4em;
		padding-bottom: 3.8%;
		font-weight: normal;
		letter-spacing: 1px;
	}

	.banner-lis {
		display: inline-block;
		padding: 0 50px 0 30px;
		-moz-border-radius: 30px;
		-webkit-border-radius: 30px;
		-ms-border-radius: 30px;
		-o-border-radius: 30px;
		background: rgba(255, 255, 255, 0.9);
		border-radius: 30px;
	}

	.banner-lis li {
		display: inline-block;
		vertical-align: top;
		font-size: 16px;
		line-height: 62px;
		margin: 0 26px;
	}

	.banner-lis li a {
		padding-left: 20px;
		display: block;
		color: #555;
	}

	.banner-lis li.on a {
		background:url("<?php echo $site['site_url'];?>common/static/image/arr.png") no-repeat left center;
	}

	.banner-lis a:hover {
		text-decoration: none;
		background:url("<?php echo $site['site_url'];?>common/static/image/arr.png") no-repeat left center;
	}

	.banner-img {
		position: relative;
		margin-top: 4rem;
		width: 100%;
	}

	.gntab {
		padding-top: 22px;
		text-align: center;
		padding-bottom: 30px;
	}

	.gntab li {
		display: inline-block;
		vertical-align: middle;
		margin: 0 4px;
	}

	.gntab li a {
		display: block;
		padding: 0 28px;
		line-height: 2.14em;
		font-size: 14px;
		color: #555;
		border-radius: 16px;
		-moz-transition: all .5s;
		-ms-transition: all .5s;
		-webkit-transition: all .5s;
		-o-transition: all .5s;
		transition: all .5s;
	}

	.gntab li.cur a,
	.gntab li a:hover {
		background: #eb9030;
		color: #fff;
		text-decoration: none;
	}

	.gntxt {
		text-align: center;
		font-size: 12px;
		color: #777;
		line-height: 2em;
		padding-top: 20px;
		padding-bottom: 46px;
	}

	.gnlis {
		font-size: 0;
	}

	.gnlis li {
		width: 32%;
		margin-right: 2%;
		display: inline-block;
		vertical-align: top;
		margin-bottom: 3.3%;
	}

	.gnlis li:nth-child(3n) {
		margin-right: 0;
	}

	.gnlis li p {
		font-size: 18px;
		color: #555;
		line-height: 1.6em;
		padding: 4.1% 0;
		background: #f1f1f1;
		text-align: center;
	}

	.ngnm {
		padding: 50px 0 70px;
	}

	.tit-box {
		text-align: center;
	}

	.tit-box .en {
		font-size: 26px;
		color: #ccc;
		line-height: 1.3em;
		font-weight: normal;
		font-family: font;
		text-transform: uppercase;
	}

	.tit-box .ch {
		font-size: 24px;
		color: #555;
		line-height: 1.75em;
		font-weight: normal;
	}

	.tit-box .ch b {
		font-weight: normal;
		color: #cca581;
	}

	.tit-box span {
		display: inline-block;
		padding: 0 40px;
		position: relative;
	}

	.tit-box span:before,
	.tit-box span:after {
		content: "";
		position: absolute;
		top: 50%;
		width: 15vw;
		height: 1px;
		background: #eaeaea;
	}

	.tit-box span:before {
		left: 100%;
	}

	.tit-box span:after {
		right: 100%;
	}

	.wmxq {
		padding: 60px 0 100px;
		background: #f1f1f1;
		width: 100%;
		position: relative;
		/*right: 1.5rem;*/
	}

	.wmxq-cont {
		/* margin-top: 36px; */
		background: #fff;
	}

	.wmxq-tp {
		padding: 25px;
		overflow: hidden;
	}

	.wmxq-big li.show {
		display: block;
		opacity: 1;
	}

	.wmxq-big li {
		display: none;
		opacity: 0;
	}

	.wmxq-tple {
		width: 70%;
		float: right;
	}

	.wmxq-min {
		padding: 20px 0 6px;
		overflow: hidden;
	}

	.wmxq-min li {
		float: left;
		margin-right: 20px;
		cursor: pointer;
	}

	.wmxq-min ul {
		width: 1200px;
	}

	.wmxq-tplr {
		width: 26.6%;
		float: left;
		padding-top: 26px;
	}

	.wmxq-tit {
		font-size: 24px;
		color: #555;
		line-height: 2em;
		font-weight: normal;
		padding-bottom: 16px;
	}

	.wmxq-tip {
		font-size: 16px;
		color: #555;
		line-height: 1.9em;
		padding-bottom: 10px;
	}

	.wmxq-txt {
		font-size: 12px;
		color: #777;
		line-height: 2em;
	}

	.wmxq-txt p {
		padding-bottom: 20px;
	}

	.wmxq-qh {
		font-size: 18px;
		color: #555;
		line-height: 2em;
		padding: 10px 25px;
		overflow: hidden;
	}

	.wmxq-qh .le,
	.wmxq-qh .lr {
		float: left;
		width: 50%;
	}

	.wmxq-qh a {
		color: #555;
	}

	.wmxq-qh a:hover {
		color: #cba580;
	}

	.wmxq-tp {
		border-bottom: 2px solid #f1f1f1;
	}

	.wmxq-min li {
		position: relative;
	}

	.wmxq-min li:before {
		content: "";
		position: absolute;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
		-moz-box-sizing: border-box;
		-webkit-box-sizing: border-box;
		-ms-box-sizing: border-box;
		-o-box-sizing: border-box;
		box-sizing: border-box;
		border: 3px solid #eb9030;
		display: none;
	}

	.wmxq-min li.cur::before {
		display: block;
	}

	.wmxq-min ul {
		position: relative;
	}
		   @media screen and (max-width:1280px) {
	.banner .banner-img {
		margin-top: 2.6rem;
	}
}
	/* 手机 */
	@media screen and (max-width: 769px) {
		.tit-box .ch {
		    font-size: 18px;
		}
		.tit-box .en {
		    font-size: 20px;
			margin-bottom: 0;
		}
		.tit-box .ch {
		    font-size: 18px;
			margin:0px;
			padding:0px;
		}
		.tit-box .en {
		    font-size: 20px;
			margin-bottom: 0;
		}
		.menu-tit {
			display: none;
		}
	
		.banner .banner-img {
			width: 100%;
			height: auto;
			margin-top: 2.5rem;
		}
	
		.banner {
			height: auto !important;
		}
	
		.banner .banner-menu {
			transform: translateY(0);
			border-bottom: 10px solid #f1f1f1;
			position: static;
			top: 0;
		}
	
		.nwod .nwod-box {
			padding: 7% 0 8%;
		}
	
		.banner-menu .banner-lis {
			width: 100%;
			text-align: center;
		}
	
		.banner-menu .banner-lis,
		.banner-menu .banner-lis li a {
			margin: 0px;
			padding: 0px;
		}
	
		.banner-menu .banner-lis li {
			line-height: 40px;
			margin: 0 3%;
			font-size: 12px;
		}
	
		.banner-menu .banner-lis li.on a {
			color: #eb9030;
			background: none;
		}
	
		.banner-lis a:hover {
			background: none !important;
		}
	
		.nymain .ngnm {
			padding-top: 0px;
		}
	
		.ngnm .gntab {
			padding: 0px;
		}
	
		.ngnm .gntab a {
			padding: 0 2px;
			font-size: 12px;
		}
	
		.gnlis .wmxq {
			width: 100vw;
		}
	
		.ngnm .gntab ul {
			padding: 0px;
		}
	}
	
	@media screen and (max-width: 992px) {
	
		.wmxq-tp .wmxq-tple,
		.wmxq-tp .wmxq-tplr {
			width: 100%;
		}
	}
			.zhong{
	    background: #eb9030;
		color: #fff;
		border-radius: 16px;
	}
	
</style>





<div class="banner">
	<div class="banner-img">
		<img src="<?php echo $site['site_url'];?>common/static/image/2018072116465178.jpg">
		<div class="banner-bg"></div>
	</div>
	<div class="banner-menu">
		<div class="container">
			<h3 class="menu-tit">我们的产品</h3>
			<ul class="banner-lis">
				<li>
					<a href="<?php echo $site['site_url'];?>guanniumumen">冠牛木门</a>
				</li>
				<li>
					<a href="<?php echo $site['site_url'];?>zhengtijiaji">整体家居</a>
				</li>
				<li class="on">
					<a href="<?php echo $site['site_url'];?>shijianli">实景案例</a>
				</li>
			</ul>
		</div>
	</div>
</div>


      <div class="nymain">
        <div class="ngnm">
            <div class="container">
                 <div class="tit-box">
                    <h3 class="en">REAL CASE<!-- <?php echo $entitle;?> --></h3>
                    <h3 class="ch"><span>实景案例<!-- <?php echo $catname;?> --></span></h3>
                </div>
               <div class="gntab">
    				<ul>
    					    <?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'nav')) {$data = $tag->nav(array('field'=>'catid,catname,arrchildid,pclink,type,catdir','where'=>"parentid=23",'limit'=>'5',));}?>
                            <?php if(is_array($data)) foreach($data as $v) { ?>
                                                    <li <?php if($catid == $v[catid]) { ?>class="zhong"<?php } ?>>
                                <a href="<?php echo $site['site_url'];?><?php echo $v['catdir'];?>"><?php echo $v['catname'];?></a>
                            </li>
                            <?php } ?>
    				</ul>
    			</div>

            <div class="gntxt">
                <div style="text-align:center;">
                    <?php echo $subtitle;?>
                </div>
            </div>
            <ul class="gnlis">
<!--                <div class="wmxq" style="padding:2vw  4% 0  4%;">
                    <div class="">
                        <div class="wmxq-cont">
                            <a href="http://www.szguanniu.com/product_showmore.php?id=183">
                                <div class="wmxq-tp">
                                    <div class="wmxq-tple">
                                        <div class="wmxq-big">
                                            <ul>
                                                <li class="show" style="width: 100%;">
                                                    <img src="<?php echo $site['site_url'];?>common/static/image/2019090315074741.jpg"
                                                        alt="" style="width: 100%;">
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="wmxq-tplr">
                                        <div class="wmxq-box">
                                            <h3 class="wmxq-tit">马到成功</h3>
                                            <div class="wmxq-wz">
                                                <p class="wmxq-tip">详情介绍:</p>
                                                <div class="wmxq-txt">
                                                    <span
                                                        style="color:#666666;font-size:14px;">运用纹理丰富的高档材质为基调，以写意的手法构画出大器天成的名门境界。它以简洁明快的风格布局平面元素，形成具有层次感的造型作品，并以精湛的工艺赋予它独特的文化内涵，表达了主人公自信有为、马到成功的优越心态。适合各种风格的家居环境选配。</span><span
                                                        style="color:#666666;font-size:14px;"></span><br>
                                                    <br>
                                                    <span style="color:#666666;font-size:14px;"></span><br>
                                                    <span style="color:#666666;font-size:24px;"></span><span
                                                        style="color:#666666;font-size:24px;">罗马假日<br>
                                                    </span><br>
                                                    <span style="color:#666666;font-size:14px;"></span><span
                                                        style="color:#666666;font-size:14px;">罗马世界古代史上最大的国家之一，丘比特爱神传说的根源，风光旖旎的传统文化，成就了“光荣属于埃及，伟大属于罗马”的美名。感受凯旋归来的洗礼，体验罗马式假日风情。</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div> -->
                 <?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'lists')) {$data = $tag->lists(array('field'=>'title,thumb,url,description,inputtime,click,catid,content','modelid'=>'1','catid'=>$catid,'limit'=>'6','page'=>'page',));$pages = $tag->pages();}?>
                    <?php if(is_array($data)) foreach($data as $v) { ?>
                <div class="wmxq" style="padding:2vw  4% 0  4%;">>
                    <div class="">
                        <div class="wmxq-cont">
                            <a href="<?php echo $v['url'];?>">
                                <div class="wmxq-tp">
                                    <div class="wmxq-tple">
                                        <div class="wmxq-big">
                                            <ul>
                                                <li class="show" style="width: 100%;">
                                                    <img src="<?php echo get_thumb($v['thumb']);?>" alt="<?php echo $v['title'];?>" style="width: 100%;">
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                    <div class="wmxq-tplr">
                                        <div class="wmxq-box">
                                            <h3 class="wmxq-tit"><?php echo $v['title'];?></h3>
                                            <div class="wmxq-wz">
                                                <!-- <p class="wmxq-tip">详情介绍:</p> -->
                                                <div class="wmxq-txt">
                                                        <?php echo $v['content'];?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <?php } ?>

                                    </ul>
            </div>
        </div>
    </div>



<?php include template("index","footer"); ?>



<script type="text/javascript">



</script>
