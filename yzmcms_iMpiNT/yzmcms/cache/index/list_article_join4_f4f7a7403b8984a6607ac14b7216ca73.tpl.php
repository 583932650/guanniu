<?php defined('IN_YZMPHP') or exit('No permission resources.'); ?><?php include template("index","header"); ?>
<style>
.banner {
    position:relative;
     /* height: 20rem; */
}
.banner img {
    width:100%;
    height: 100%;
}
.banner-bg {
    position:absolute;
    left:0;
    top:0;
    width:100%;
    height:100%;
    background:rgba(0, 0, 0, 0.3);
}
.banner-menu {
   position:absolute;
   transform: translateY(-50%);
   left:0;
   top:62%;
   width:100%;
   text-align:center;
   z-index: 888;
   font-size:0;
}
.banner-menu .menu-tit {
    font-size:28px;
    color:#fff;
    line-height:1.4em;
    padding-bottom:3.8%;
    font-weight:normal;
    letter-spacing:1px;
}
.banner-lis {
    display:inline-block;
    padding:0 50px 0 30px;
    -moz-border-radius:30px;
    -webkit-border-radius:30px;
    -ms-border-radius:30px;
    -o-border-radius:30px;
    background:rgba(255, 255, 255, 0.9);
    border-radius:30px;
}
.banner-lis li {
    display:inline-block;
    vertical-align:top;
    font-size:16px;
    line-height:62px;
    margin:0 26px;
}
.banner-lis li a {
    padding-left:20px;
    display:block;
    color:#555;
}
.banner-lis li.on a {
    background:url("<?php echo $site['site_url'];?>common/static/image/arr.png") no-repeat left center;
}
.banner-lis a:hover {
    text-decoration:none;
    background:url("<?php echo $site['site_url'];?>common/static/image/arr.png") no-repeat left center;
}
.banner-img {
	position: relative;
	margin-top: 4rem;
	width: 100%;
}


.tit-box {
    text-align:center;
}
.tit-box .en {
    font-size:26px;
    color:#ccc;
    line-height:1.3em;
    font-weight: normal;
    font-family: font;
    text-transform:uppercase;
}
.tit-box .ch {
    font-size:24px;
    color:#555;
    line-height:1.75em;
    font-weight: normal;
}
.tit-box .ch b {
    font-weight: normal;
    color:#cca581;
}
.tit-box span {
    display:inline-block;
    padding:0 40px;
    position:relative;
}
.tit-box span:before,
.tit-box span:after {
    content:"";
    position:absolute;
    top:50%;
    width:15vw;
    height:1px;
    background:#eaeaea;
}
.tit-box span:before {
    left:100%;
}
.tit-box span:after {
    right:100%;
}
.njia-xq {
    background:#f1f1f1;
    padding:70px 0;
}
.njia-xqli li{
    height:206px;
    margin-bottom:50px;
    display:inline-block;
    vertical-align: middle;
    background:#fff;
    width:31.6%;
    margin-right:2.6%;
    -moz-box-sizing:border-box;
    -ms-box-sizing:border-box;
    -webkit-box-sizing:border-box;
    -o-box-sizing:border-box;
    box-sizing:border-box;
    padding:0 24px 0;
    margin-top:50px;
}
.njia-xqli .li-img {
    width:100px;
    height:100px;
    margin-left:auto;
    margin-right:auto;
    margin-top:-50px;
    background:#fff;
    -moz-border-radius:50%;
    -ms-border-radius:50%;
    -webkit-border-radius:50%;
    -o-border-radius:50%;
    border-radius:50%;
    overflow:hidden;
}
.njia-xqli .li-tit {
    font-size:16px;
    color:#555;
    line-height:2em;
    text-align:center;
    font-weight: normal;
    padding-bottom:10px;
}
.njia-xqli{
	padding: 0px !important;
}
.njia-xqli .li-txt {
    font-size:14px;
    color:#777;
    line-height:2em;
    text-align:center;
}
.njia-xqli li:nth-child(3n) {
    margin-right:0;
}
.njia-xqli {
    font-size:0;
}
.njia-list li:last-child{
    display: none;
}
.njia-xqli li:hover .li-img {
    -moz-transition:all .5s;
    -webkit-transition:all .5s;
    -ms-transition:all .5s;
    -o-transition:all .5s;
    transition:all .5s;
    -moz-transform: rotate(360deg);
    -ms-transform: rotate(360deg);
    -webkit-transform: rotate(360deg);
    -o-transform: rotate(360deg);
    transform: rotate(360deg);
}
   @media screen and (max-width:1280px) {
	.banner .banner-img {
		margin-top: 2.6rem;
	}
}
/* 手机 */
@media screen and (max-width: 769px) { 
    .banner-menu .banner-lis li.on a {
        color: #eb9030;
        background: none;
    }
	.tit-box .ch {
	    font-size: 18px;
	}
	.tit-box .en {
	    font-size: 20px;
		margin-bottom: 0;
	}
	.tit-box .ch {
	    font-size: 18px;
		margin:0px;
		padding:0px;
	}
	.tit-box .en {
	    font-size: 20px;
		margin-bottom: 0;
	}
	.menu-tit {
		display: none;
	}
		
	.banner .banner-img {
		width: 100%;
		height: auto;
		margin-top: 2.5rem;
	}
		
	.banner {
		height: auto !important;
	}
		
	.banner .banner-menu { transform: translateY(0); border-bottom: 10px solid #f1f1f1;
		position: static;
		top: 0;
	}
		
	.nwod .nwod-box {
		padding: 7% 0 8%;
	}
		
	.banner-menu .banner-lis {
		width: 100%;
		text-align: left;
	}
	.banner-menu .banner-lis li {line-height: 40px;
		margin: 0 3%;
		font-size: 12px;
	}
		
	.banner-menu .banner-lis li a {
		padding: 0px;
	}
	.banner-lis a:hover {
		background: none !important;
	}
	.banner-menu .banner-lis,.banner-menu .banner-lis li a {
		margin: 0px;
		padding: 0px;
	}
	.njia-xq .njia-xqli li{
		width: 90%;
		margin: 35px 5%;
	}
	.njia-xq .njia-xqli .li-img,.njia-xq .njia-xqli .li-img img{
		width: 80px;
		height: 80px;
	}
	.njia-xq .njia-xqli .li-img{
		margin-top: -40px;
	}
}
@media screen and (min-width: 769px) and (max-width: 1024px) {
	.banner-menu .banner-lis li {
	    /*line-height: 40px;*/
		margin: 0 26px 0px 6px;
		/*font-size: 14px;*/
	}
		
	/*.banner-menu .banner-lis li a {*/
	/*	padding: 0px;*/
	/*}*/
	/*.banner-lis a:hover {*/
	/*	background: none !important;*/
	/*}*/
		
	/*.banner-menu .banner-lis {*/
	/*	display: block;*/
	/*}*/
	.njia-xq .njia-xqli li{
		width: 45%;
	}
	.njia-xq  .njia-xqli li:nth-child(3n){
		margin-right: 2.6%;
	}
}
</style>





<div class="banner">
        <div class="banner-img">
                            <img src="<?php echo $site['site_url'];?>common/static/image/2018072116514484.jpg">            <div class="banner-bg"></div>
        </div>
        <div class="banner-menu">
            <div class="container">
                <h3 class="menu-tit">加盟冠牛</h3>
                <ul class="banner-lis">
                                            <li>
                            <a href="<?php echo $site['site_url'];?>/pinpaiyoushi">品牌优势</a>
                        </li>
                                                <li>
                            <a href="<?php echo $site['site_url'];?>/xingyeyoushi">行业优势</a>
                        </li>
                                                <li>
                            <a href="<?php echo $site['site_url'];?>/jiamengzhichi">加盟支持</a>
                        </li>
                                                <li class="on">
                            <a href="<?php echo $site['site_url'];?>/jiamengliucheng">加盟流程</a>
                        </li>
                                                <li>
                            <a href="<?php echo $site['site_url'];?>/woyaojiameng">我要加盟</a>
                        </li>
                                        </ul>
            </div>
        </div>
    </div>


    <div class="nymain">
    <div class="njia-xq">
        <div class="container" style="padding: 0px;">
            <ul class="njia-xqli">
                <?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'pro_list')) {$data = $tag->pro_list(array('field'=>'title,url,description,pro_img','id'=>'234','limit'=>'1','page'=>'page',));$pages = $tag->pages();}?>
                    <?php if(is_array($data)) foreach($data as $v) { ?>
                <li>
                    <div class="li-img">
                        <img src="<?php echo $v['url'];?>" alt="">
                    </div>
                    <h3 class="li-tit"><?php echo $v['alt'];?></h3>
                    <div class="li-txt">
                        <p><?php echo $v['word'];?></p>
                    </div>
                </li>
              <?php } ?>
            </ul>
        </div>
    </div>
</div>  




   <?php include template("index","footer"); ?>



    <script type="text/javascript">

    

    </script>





