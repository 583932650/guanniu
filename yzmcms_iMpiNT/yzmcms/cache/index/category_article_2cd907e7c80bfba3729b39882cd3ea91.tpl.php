<?php defined('IN_YZMPHP') or exit('No permission resources.'); ?><?php include template("index","header"); ?>
<style>
body, h1, h2, h3, h4, h5, p, dl, dd, ul, ol, form, input, textarea, th, td, select {
    margin: 0;
    padding: 0;
}
.nwod-img img,.nwod-icon img{
    width: 100%;
}
.banner {
    position:relative;
    /*  /* height: 20rem; */ */
}
.banner img {
    width:100%;
    height: 100%;
}
.banner-bg {
    position:absolute;
    left:0;
    top:0;
    width:100%;
    height:100%;
    background:rgba(0, 0, 0, 0.3);
}
.banner-menu {
   position:absolute;
   transform: translateY(-50%);
   left:0;
   top:62%;
   width:100%;
   text-align:center;
   z-index: 888;
   font-size:0;
}
.banner-menu .menu-tit {
    font-size:28px;
    color:#fff;
    line-height:1.4em;
    padding-bottom:3.8%;
    font-weight:normal;
    letter-spacing:1px;
}
.banner-lis {
    display:inline-block;
    padding:0 50px 0 30px;
    -moz-border-radius:30px;
    -webkit-border-radius:30px;
    -ms-border-radius:30px;
    -o-border-radius:30px;
    background:rgba(255, 255, 255, 0.9);
    border-radius:30px;
}
.banner-lis li {
    display:inline-block;
    vertical-align:top;
    font-size:16px;
    line-height:62px;
    margin:0 26px;
}
.banner-lis li a {
    padding-left:20px;
    display:block;
    color:#555;
}
.banner-lis li.on a {
    background:url("<?php echo $site['site_url'];?>common/static/image/arr.png") no-repeat left center;
}
.banner-lis a:hover {
    text-decoration:none;
    background:url("<?php echo $site['site_url'];?>common/static/image/arr.png") no-repeat left center;
}
.banner-img {
	position: relative;
	margin-top: 4rem;
	width: 100%;
}

.nwod-box {
    padding:84px 0 96px;
    font-size:0;
}
.nwod-img {
    width:51.6%;
    display:inline-block;
    vertical-align:middle;
}
.nwod-cont {
    width:48.4%;
    display:inline-block;
    vertical-align: middle;
    -moz-box-sizing:border-box;
    -webkit-box-sizing:border-box;
    -ms-box-sizing:border-box;
    -o-box-sizing:border-box;
    box-sizing:border-box;
    text-align:center;
}
.nwod-box:nth-child(2n+1) .nwod-cont {
    padding-left:4.5%;
}
.nwod-box:nth-child(2n) .nwod-cont {
    padding-left:4.5%;
}
.nwod-icon {
    margin:0 auto;
    width:125px;
    height:125px;
    border:2px solid #ddd;
    -moz-border-radius:50%;
    -webkit-border-radius:50%;
    -ms-border-radius:50%;
    -o-border-radius:50%;
    border-radius:50%;
}
.nwod-tit {
    font-size:24px;
    color:#555;
    line-height:1.8em;
    font-weight:normal;
    padding:16px 0;
}
.nwod-txt {
    font-size:14px;
    color:#777;
    line-height:2em;
    padding-bottom:50px;
}
.nwod-xq {
    font-size:14px;
    color:#555;
    line-height:2.6em;
    display:inline-block;
    padding:0 36px;
    border:1px solid #ccc;
    -moz-border-radiud:3px;
    -webkit-border-radius:3px;
    -ms-border-radius:3px;
    -o-border-radius:3px;
    border-radius:3px;
    -moz-transition:all .5s;
    -ms-transition:all .5s;
    -webkit-transition:all .5s;
    -o-transition:all .5s;
    transition:all .5s;
}
.nwod-box a:hover .nwod-xq {
    color:#fff;
    background:#cca581;
    border:1px solid #cca581;
}
.nwod-img {
    -moz-border-top-left-radius: 26px;
    -webkit-border-top-left-radius: 26px;
    -ms-border-top-left-radius: 26px;
    -o-border-top-left-radius: 26px;
    border-top-left-radius: 26px;
    -moz-border-bottom-right-radius:26px;
    -ms-border-bottom-right-radius:26px;
    -webkit-border-bottom-right-radius:26px;
    -o-border-bottom-right-radius:26px;
    border-bottom-right-radius:26px;
    overflow: hidden;
}
.nwod-box {
    border-bottom:10px solid #f1f1f1;
}
.nwod-icon .img2 {
    display: none;
}
.nwod-box a:hover .nwod-icon .img2 {
    display: inline-block;
}
.nwod-box a:hover .nwod-icon .img1 {
    display: none;
}
.nwod-box a:hover .nwod-icon {
    border:2px solid #cca581;
}
@media screen and (max-width:1280px) {
	.banner .banner-img {
		margin-top: 2.6rem;
	}
	.nwod-icon {
        width: 100px;
        height: 100px;
    }
    .nwod-tit {
        font-size: 22px;
        padding: 12px 0;
    }
}
/* 手机 */
@media screen and (max-width: 769px){
	.nwod-box .nwod-img,.nwod-box .nwod-cont,.nwod-box .nwod-img img{
		width: 100%;
	}
	.nwod-cont .nwod-icon,.nwod-cont .nwod-icon img{
		width: 60px;
		height: 60px;
	}
	.nwod-cont{
		margin-top: 20px;
	}
	.nwod-cont .nwod-tit{
		margin: 0px;
		font-size: 18px;
		padding: 10px 0;
	}
	.nwod-cont .nwod-txt{
		padding-bottom: 10px;
		font-size: 12px;
	}
	.menu-tit{
		display: none;
	}
	.banner .banner-img{
		width: 100%;
		height: auto;
		margin-top: 2.5rem;
	}
	.banner{
		height: auto !important;
	}
	.banner .banner-menu { transform: translateY(0); border-bottom: 10px solid #f1f1f1;
		position: static;
		top: 0;
	}
	.nwod .nwod-box {
	    padding: 7% 0 8%;
	}
	.banner-menu .banner-lis,.banner-menu .banner-lis li a{
		margin: 0px;
		padding: 0px;
		text-align: left;
	}
	.banner-menu .banner-lis li{
		margin: 0 3%;
		font-size: 12px;
	}
	.banner .banner-menu .banner-lis li {
		font-size: 12px;
	}
}
@media screen and (max-width: 1024px) {
	.banner-menu .banner-lis li {
		margin: 0px 20px 0 0px; 
		/*font-size: 14px;*/
	}

	/*.banner-menu .banner-lis li a {*/
	/*	padding: 0px;*/
	/*}*/
	/*.banner-lis a:hover {*/
	/*	background: none !important;*/
	/*}*/

	/*.banner-menu .banner-lis {*/
	/*	display: block;*/
	/*}*/
	.nwmd-xqtp .nwmd-xqle,.nwmd-xqtp .nwmd-xqlr{
		width: 100%;
	}
	.nwmd-xqtp  .nwmd-xqlr{
		padding-top: 15px;
	}
}

</style>





 <div class="banner">
        <div class="banner-img">
                            <img src="<?php echo $site['site_url'];?>common/static/image/2018072116445485.jpg">            <div class="banner-bg"></div>
        </div>
        <div class="banner-menu">
            <div class="container">
                <h3 class="menu-tit">我们的冠牛</h3>
                <ul class="banner-lis">
                                        <li>
                        <a href="<?php echo $site['site_url'];?>guanyuguanniu">关于冠牛</a>
                    </li>
                                            <li>
                        <a href="<?php echo $site['site_url'];?>brand">冠牛品牌</a>
                    </li>
                                            <li>
                        <a href="<?php echo $site['site_url'];?>guanniurongyu">冠牛荣誉</a>
                    </li>
                                            <li>
                        <a href="<?php echo $site['site_url'];?>zhizao">4.0智造</a>
                    </li>
                                            <li>
                        <a href="<?php echo $site['site_url'];?>fazhanlicheng">发展历程</a>
                    </li>
                                            <li>
                        <a href="<?php echo $site['site_url'];?>dongshichangzhici">董事长致辞</a>
                    </li>
                                        </ul>
            </div>
        </div>
  </div> 
      <div class="nymain">
       <div class="nwod">   
       <?php $tag = yzm_base::load_sys_class('yzm_tag');if(method_exists($tag, 'pro_list')) {$data = $tag->pro_list(array('field'=>'title,url,description,pro_img','id'=>'240','limit'=>'1','page'=>'page',));$pages = $tag->pages();}?>
                    <?php if(is_array($data)) foreach($data as $v) { ?>
       <div class="nwod-box" id="wod1">
               <div class="container">
                   <a href="/<?php echo $v['text'];?>">
                                               <div class="nwod-img">
                            <img src="<?php echo $v['url'];?>" alt="">
                        </div>
                        <div class="nwod-cont">
                            <div class="nwod-icon">
                                <img src="<?php echo $site['site_url'];?>common/static/image/d1.png" alt="" class="img1">
                                <img src="<?php echo $site['site_url'];?>common/static/image/gg1.png" alt="" class="img2">
                            </div>
                            <h3 class="nwod-tit"><?php echo $v['alt'];?></h3>
                            <div class="nwod-txt">
                                <p><?php echo $v['word'];?></p>
                            </div>

                            <div href="/<?php echo $v['text'];?>" class="nwod-xq">查看详情  &gt;</div>
                        </div>
                                              </a>
               </div>
           </div>
  <?php } ?>
               
       </div>
    </div>


   <?php include template("index","footer"); ?>



    <script type="text/javascript">
	
    

    </script>





